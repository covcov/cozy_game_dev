#include <iostream>
#include <string>

#include "bomberman_game.hpp"

using namespace std;

std::string get_dir(int argc, char* argv[])
{
    if (argc == 2)
    {
        std::string dir = argv[1];
        if (dir.back() != '/')
        {
            dir.push_back('/');
        }

        return dir;
    }

    cout << "Error: set resources path\n";
    return{};
}

int main(int argc, char* argv[])
{
    std::string dir = get_dir(argc, argv);
    if (dir.empty()) return 1;

    bomberman_game(dir);

    return 0;
}