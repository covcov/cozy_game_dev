#pragma once

#include "scene.hpp"
#include "game_objects_pool.hpp"
#include "input_handler_factory.hpp"
#include "collider_factory.hpp"
#include "random_engine.hpp"
#include "quest.hpp"
#include "triggered_spikes.hpp"
#include "button.hpp"
#include "collectable.hpp"
#include "waypoint_enemy.hpp"
#include "shooting_enemy.hpp"
#include "candle.hpp"
#include "light_processor.hpp"
#include "developer_mode_toggler.hpp"
#include "periodic_spikes.hpp"

Scene::Pointer get_delayed_spiky_road_map(sf::RenderWindow& render_window, GameObjectsPool& game_objects_pool, float cell_size)
{
    int const W = 8;
    int const H = 5;
    int mask[H][W] = {
        { 1, 1, 1, 1, 1, 1, 1, 1 },
        { 0, 0, 2, 2, 2, 2, 3, 1 },
        { 4, 1, 1, 1, 1, 1, 1, 1 },
        { 4, 4, 4, 4, 4, 4, 5, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1 }
    };

    Scene::Pointer scene = Scene::create(W, H, cell_size, {});

    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            switch (mask[y][x])
            {
            case 0:
                scene->add(GameObjectsPool::get("ground")->clone(), "background", { x, y });
                break;
            case 1:
                scene->add(GameObjectsPool::get("wall")->clone(), "middleground", { x, y });
                break;
            case 2:
                scene->add(GameObjectsPool::get("delayed_spiky_floor")->clone(), "background", { x, y });
                break;
            case 3:
                scene->add(GameObjectsPool::get("speed_bonus")->clone(), "middleground", { x, y });
                scene->add(GameObjectsPool::get("delayed_spiky_floor")->clone(), "background", { x, y });
                break;
            case 4:
                scene->add(GameObjectsPool::get("periodic_spiky_floor")->clone(), "middleground", { x, y });
                break;
            case 5:
                scene->add(GameObjectsPool::get("portal")->clone(), "middleground", { x, y });
                break;
            }
        }
    }

    scene->add(GameObjectsPool::get("character")->clone(), "middleground", { 0, 1 });

    return scene;
}

Scene::Pointer get_mixed_spiky_road_map(sf::RenderWindow& render_window, GameObjectsPool& game_objects_pool, float cell_size)
{
    int const W = 17;
    int const H = 3;
    int mask[H][W] = {
        { 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5 },
        { 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1 }
    };

    Scene::Pointer scene = Scene::create(W, H, cell_size, {});

    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            switch (mask[y][x])
            {
            case 0:
                scene->add(GameObjectsPool::get("ground")->clone(), "background", { x, y });
                break;
            case 1:
                scene->add(GameObjectsPool::get("wall")->clone(), "middleground", { x, y });
                break;
            case 3:
                scene->add(GameObjectsPool::get("periodic_spiky_floor")->clone(), "middleground", { x, y });
                break;
            case 4:
                scene->add(GameObjectsPool::get("delayed_spiky_floor")->clone(), "background", { x, y });
                break;
            case 5:
                scene->add(GameObjectsPool::get("portal")->clone(), "background", { x, y });
                break;
            }
        }
    }

    scene->add(GameObjectsPool::get("character")->clone(), "middleground", { 0, 1 });

    return scene;
}

GameObject::Pointer get_wall(std::vector<std::vector<int>> const& mask, int w, int h, int x, int y, std::string const& prefix)
{
    auto get = [&](int x, int y)
    {
        return x >= 0 && x < w && y >= 0 && y < h ? mask[y][x] : 5;
    };

    auto is_wall = [&](int x, int y) { return get(x, y) == 1; };

    GameObject::Pointer game_object;
    // corners
    if (is_wall(x + 1, y) && is_wall(x, y + 1) && !is_wall(x - 1, y) && !is_wall(x, y - 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_top_left_corner");
    }
    else if (is_wall(x - 1, y) && is_wall(x, y + 1) && !is_wall(x + 1, y) && !is_wall(x, y - 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_top_right_corner");
    }
    else if (is_wall(x + 1, y) && is_wall(x, y - 1) && !is_wall(x - 1, y) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_bottom_left_corner");
    }
    else if (is_wall(x - 1, y) && is_wall(x, y - 1) && !is_wall(x + 1, y) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_bottom_right_corner");
    }
    // vertical/horizontal walls
    else if (is_wall(x + 1, y) && is_wall(x - 1, y) && !is_wall(x, y - 1) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_horizontal");
    }
    else if (is_wall(x, y - 1) && is_wall(x, y + 1) && !is_wall(x + 1, y) && !is_wall(x - 1, y))
    {
        game_object = GameObjectsPool::get(prefix + "wall_vertical");
    }
    // single
    else if (!is_wall(x, y - 1) && !is_wall(x, y + 1) && !is_wall(x + 1, y) && !is_wall(x - 1, y))
    {
        game_object = GameObjectsPool::get(prefix + "wall_single");
    }
    // crosswalls
    else if (is_wall(x - 1, y) && is_wall(x + 1, y) && is_wall(x, y - 1) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_top_intersection");
    }
    else if (is_wall(x - 1, y) && is_wall(x + 1, y) && !is_wall(x, y - 1) && is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_bottom_intersection");
    }
    else if (is_wall(x - 1, y) && !is_wall(x + 1, y) && is_wall(x, y - 1) && is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_left_intersection");
    }
    else if (!is_wall(x - 1, y) && is_wall(x + 1, y) && is_wall(x, y - 1) && is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_right_intersection");
    }
    // ends
    else if (!is_wall(x - 1, y) && !is_wall(x + 1, y) && !is_wall(x, y - 1) && is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_top_end");
    }
    else if (!is_wall(x - 1, y) && !is_wall(x + 1, y) && is_wall(x, y - 1) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_bottom_end");
    }
    else if (is_wall(x - 1, y) && !is_wall(x + 1, y) && !is_wall(x, y - 1) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_left_end");
    }
    else if (!is_wall(x - 1, y) && is_wall(x + 1, y) && !is_wall(x, y - 1) && !is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_right_end");
    }
    // cross
    else if (is_wall(x - 1, y) && is_wall(x + 1, y) && is_wall(x, y - 1) && is_wall(x, y + 1))
    {
        game_object = GameObjectsPool::get(prefix + "wall_intersection");
    }
    else
    {
        game_object = GameObject::Pointer(new GameObject("collider"));
    }

    game_object->tag = "wall";
    ColliderFactory::addBoxCollider(game_object, { 16, 16 });
    return game_object;
}

Scene::Pointer get_temple_map(sf::RenderWindow& render_window, float cell_size)
{
    int const W = 19;
    int const H = 16;
    std::vector<std::vector<int>> mask = {
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 1, 5, 5 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 4, 1, 0, 0, 0, 1, 1, 1 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 0, 0, 0, 0, 0, 0, 3, 1 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1, 1, 0, 0, 0, 1, 1, 1 },
        { 5, 5, 5, 1, 1, 1, 1, 5, 5, 5, 5, 5, 1, 0, 0, 0, 1, 5, 5 },
        { 1, 1, 1, 1, 0, 0, 1, 5, 5, 5, 5, 5, 1, 1, 0, 1, 1, 5, 5 },
        { 1, 0, 0, 2, 0, 0, 1, 5, 5, 5, 5, 5, 5, 1, 0, 1, 5, 5, 5 },
        { 1, 1, 1, 0, 0, 0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 5 },
        { 5, 5, 1, 1, 0, 0, 1, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 5 },
        { 5, 5, 5, 1, 0, 0, 1, 5, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 5 },
        { 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 5, 1, 0, 0, 0, 1, 5 },
        { 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 5, 1, 1, 1, 1, 1, 5 },
        { 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 5, 5, 5, 5, 5, 5, 5 },
        { 1, 0, 0, 0, 0, 1, 5, 5, 1, 1, 1, 1, 5, 5, 5, 5, 5, 5, 5 },
        { 1, 1, 1, 1, 1, 1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
    };

    Scene::Pointer scene = Scene::create(W, H, cell_size, {});

    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            switch (mask[y][x])
            {
            case 0:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                break;
            case 1:
            {
                auto obj = get_wall(mask, W, H, x, y, "blue_temple_");
                scene->add(obj->clone(), "background", { x, y });
                break;
            }
            case 2:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                scene->add(GameObjectsPool::get("box")->clone(), "movable", { x, y });
                break;
            case 3:
            {
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                auto key = GameObjectsPool::get("key")->clone();
                key->forceCast<Collectable>()->setMessage("key_collected");
                scene->add(key, "middleground", { x, y });
                break;
            }
            case 4:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                scene->add(GameObjectsPool::get("chest")->clone(), "middleground", { x, y });
                break;
             case 5:
                 if (y > 0 && mask[y - 1][x] == 1)
                 {
                     scene->add(GameObjectsPool::getRandom("blue_temple_wall_edge"), "background", { x, y });
                 }

                 break;
                break;
            }
        }
    }

    {
        auto triggered_spikes_3 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_3->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_1");
        scene->add(triggered_spikes_3, "middleground", { 3, 7 });

        auto triggered_spikes_4 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_4->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_1");
        scene->add(triggered_spikes_4, "middleground", { 4, 7 });

        auto triggered_spikes_5 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_5->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_1");
        scene->add(triggered_spikes_5, "middleground", { 5, 7 });

        auto blue_temple_button_1 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_1->forceCast<Button>()->setTriggerMessage("spikes_set_1");
        scene->add(blue_temple_button_1, "middleground", { 1, 6 });
    }

    {
        auto triggered_spikes_6 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_6->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_2");
        scene->add(triggered_spikes_6, "middleground", { 3, 11 });
    }

    {
        auto triggered_spikes_7 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_7->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_3");
        scene->add(triggered_spikes_7, "middleground", { 13, 4 });

        auto triggered_spikes_8 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_8->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_3");
        scene->add(triggered_spikes_8, "middleground", { 14, 4 });

        auto triggered_spikes_9 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_9->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_3");
        scene->add(triggered_spikes_9, "middleground", { 15, 4 });

        auto blue_temple_button_2 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_2->forceCast<Button>()->setTriggerMessage("spikes_set_3");
        scene->add(blue_temple_button_2, "middleground", { 14, 3 });
    }

    {
        auto triggered_spikes_10 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_10->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_4");
        scene->add(triggered_spikes_10, "middleground", { 12, 2 });

        auto blue_temple_button_3 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_3->forceCast<Button>()->setTriggerMessage("spikes_set_4");
        scene->add(blue_temple_button_3, "middleground", { 13, 1 });
    }

    {
        auto triggered_spikes_11 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_11->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_5");
        scene->add(triggered_spikes_11, "middleground", { 16, 2 });

        auto blue_temple_button_3 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_3->forceCast<Button>()->setTriggerMessage("spikes_set_5");
        scene->add(blue_temple_button_3, "middleground", { 15, 1 });
    }

    scene->add(GameObjectsPool::get("character")->clone(), "movable", { 4, 6 });
    scene->add(GameObjectsPool::get("pause_toggler")->clone(), "ui", { 0, 0 });
    scene->add(std::make_shared<DeveloperModeToggler>("dev_mode_toggler"), "ui", { 0, 0 });

    return scene;
}

Scene::Pointer get_temple_map_2(sf::RenderWindow& render_window, float cell_size)
{
    int const W = 9;
    int const H = 25;
    std::vector<std::vector<int>> mask = {
        { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
        { 1, 0, 1, 1, 1, 1, 1, 0, 1 },
        { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
        { 1, 1, 1, 0, 1, 0, 1, 1, 1 },
        { 1, 1, 1, 0, 1, 0, 1, 1, 1 },
        { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
        { 1, 0, 1, 1, 1, 1, 1, 0, 1 },
        { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
        { 1, 1, 1, 0, 1, 0, 1, 1, 1 },
        { 1, 0, 0, 0, 1, 0, 1, 5, 5 },
        { 1, 0, 1, 1, 1, 0, 1, 1, 1 },
        { 1, 0, 0, 0, 1, 0, 1, 3, 1 },
        { 1, 1, 1, 0, 1, 0, 1, 0, 1 },
        { 1, 0, 0, 0, 1, 0, 2, 0, 1 },
        { 1, 0, 1, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 1, 0, 1, 0, 1, 0, 1 },
        { 1, 0, 0, 0, 1, 0, 0, 0, 1 },
        { 1, 1, 0, 4, 1, 0, 0, 1, 1 },
        { 5, 1, 0, 1, 1, 1, 0, 1, 5 },
        { 5, 1, 0, 1, 5, 1, 0, 1, 5 },
        { 5, 1, 0, 1, 5, 1, 0, 1, 5 },
        { 5, 1, 0, 1, 5, 1, 6, 1, 5 },
        { 5, 1, 1, 1, 1, 1, 1, 1, 5 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5 }
    };

    Scene::Pointer scene = Scene::create(W, H, cell_size, {});

    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            switch (mask[y][x])
            {
            case 0:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                break;
            case 1:
            {
                auto obj = get_wall(mask, W, H, x, y, "blue_temple_");
                scene->add(obj->clone(), "background", { x, y });
                break;
            }
            case 2:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                scene->add(GameObjectsPool::get("box")->clone(), "movable", { x, y });
                break;
            case 3:
            {
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                auto key = GameObjectsPool::get("key")->clone();
                key->forceCast<Collectable>()->setMessage("key_collected");
                scene->add(key, "middleground", { x, y });
                break;
            }
            case 4:
            {
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                scene->add(GameObjectsPool::get("sniper")->clone(), "movable", { x, y });
                break;
            }
            case 5:
                if (y > 0 && mask[y - 1][x] == 1)
                {
                    scene->add(GameObjectsPool::getRandom("blue_temple_wall_edge"), "background", { x, y });
                }

                break;
            case 6:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                scene->add(GameObjectsPool::get("chest")->clone(), "middleground", { x, y });
                break;
            }
        }
    }

    {
        auto triggered_spikes_1 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_1->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_1");
        scene->add(triggered_spikes_1, "middleground", { 1, 2 });

        auto blue_temple_button_1 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_1->forceCast<Button>()->setTriggerMessage("spikes_set_1");
        scene->add(blue_temple_button_1, "middleground", { 7, 2 });
    }

    {
        auto triggered_spikes_2 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_2->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_2");
        scene->add(triggered_spikes_2, "middleground", { 3, 4 });

        auto blue_temple_button_2 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_2->forceCast<Button>()->setTriggerMessage("spikes_set_2");
        scene->add(blue_temple_button_2, "middleground", { 5, 4 });
    }

    {
        auto triggered_spikes_3 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_3->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_3");
        scene->add(triggered_spikes_3, "middleground", { 3, 5 });

        auto blue_temple_button_3 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_3->forceCast<Button>()->setTriggerMessage("spikes_set_3");
        scene->add(blue_temple_button_3, "middleground", { 5, 5 });
    }

    {
        auto triggered_spikes_4 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_4->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_4");
        scene->add(triggered_spikes_4, "middleground", { 4, 15 });
    }

    {
        auto triggered_spikes_5 = GameObjectsPool::get("triggered_spikes")->clone();
        triggered_spikes_5->forceCast<TriggeredSpikes>()->setTriggerMessage("spikes_set_5");
        scene->add(triggered_spikes_5, "middleground", { 6, 20 });

        auto blue_temple_button_5 = GameObjectsPool::get("blue_temple_button")->clone();
        blue_temple_button_5->forceCast<Button>()->setTriggerMessage("spikes_set_5");
        scene->add(blue_temple_button_5, "middleground", { 2, 20 });
    }

    auto spider = GameObjectsPool::get("spider")->clone()->forceCast<WaypointEnemy>();
    spider->setWaypoints({ sf::Vector2i(3, 1), sf::Vector2i(2, 22) });
    spider->registerAttribute("speed", 16);
    scene->add(spider, "movable", { 3, 1 });

    scene->add(GameObjectsPool::get("character")->clone(), "movable", { 5, 1 });
    scene->add(GameObjectsPool::get("pause_toggler")->clone(), "ui", { 0, 0 });
    scene->add(std::make_shared<DeveloperModeToggler>("dev_mode_toggler"), "ui", { 0, 0 });

    return scene;
}

Scene::Pointer get_temple_map_3(sf::RenderWindow& render_window, float cell_size)
{
    int const W = 10;
    int const H = 25;
    std::vector<std::vector<int>> mask = {
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 2, 0, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
        { 1, 0, 0, 1, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 3, 3, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 3, 3, 0, 0, 1 },
        { 1, 1, 0, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 7, 0, 0, 0, 0, 1, 7, 0, 1 },
        { 1, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
        { 1, 1, 0, 1, 0, 1, 1, 0, 1, 1 },
        { 1, 0, 0, 1, 0, 1, 0, 0, 1, 5 },
        { 1, 0, 0, 1, 0, 1, 0, 0, 1, 5 },
        { 1, 0, 0, 1, 0, 1, 0, 0, 1, 5 },
        { 1, 0, 0, 1, 0, 1, 0, 0, 1, 5 },
        { 1, 0, 0, 1, 1, 1, 0, 0, 1, 1 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
        { 1, 1, 0, 0, 1, 0, 0, 1, 0, 1 },
        { 5, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
    };

    Scene::Pointer scene = Scene::create(W, H, cell_size, {});

    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            switch (mask[y][x])
            {
            case 0:
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                break;
            case 1:
            {
                auto obj = get_wall(mask, W, H, x, y, "blue_temple_");
                scene->add(obj->clone(), "background", { x, y });
                break;
            }
            case 2:
            {
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                auto candle = GameObjectsPool::getAs<Candle>("candle");
                candle->setBurningTime(300);
                scene->add(candle->clone(), "foreground", { x, y });
                break;
            }
            case 3:
                scene->add(GameObjectsPool::get("periodic_spikes")->clone(), "middleground", { x, y });
                break;
            case 5:
                if (y > 0 && mask[y - 1][x] == 1)
                {
                    scene->add(GameObjectsPool::getRandom("blue_temple_wall_edge"), "middleground", { x, y });
                }

                break;
            case 7:
            {
                scene->add(GameObjectsPool::getRandom("blue_temple_floor"), "background", { x, y });
                auto pillar = GameObjectsPool::get("gray_temple_pillar");
                pillar->tag = "brick";
                scene->add(pillar->clone(), "movable", { x, y });
                scene->add(GameObjectsPool::get("gray_temple_torch"), "movable", { x, y });
                break;
            }
            }
        }
    }

    scene->add(GameObjectsPool::get("character")->clone(), "movable", { 2, 3 });
    scene->add(GameObjectsPool::get("pause_toggler")->clone(), "ui", { 0, 0 });
    scene->add(std::make_shared<LightProcessor>(), "posteffects", { 0, 0 });
    scene->add(std::make_shared<DeveloperModeToggler>("dev_mode_toggler"), "ui", { 0, 0 });

    return scene;
}