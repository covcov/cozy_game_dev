#pragma once

#include <unordered_map>
#include <array>

#include "game_object.hpp"
#include "grid_map.hpp"

class ExplosionManager
{
public:
    using Pointer = std::shared_ptr<ExplosionManager>;

    enum class ExplosionType
    {
        CENTER,
        LEFT_BODY,
        TOP_BODY,
        RIGHT_BODY,
        BOTTOM_BODY,
        LEFT_TAIL,
        TOP_TAIL,
        RIGHT_TAIL,
        BOTTOM_TAIL
    };

    static ExplosionManager::Pointer create()
    {
        return ExplosionManager::Pointer(new ExplosionManager());
    }

    std::vector<GameObject::Pointer> create(GridMap::Pointer grid_map, sf::Vector2i cell, int fire_power) const;
    void setPrototype(ExplosionType type, GameObject::Pointer game_object);

private:
    ExplosionManager() = default;
    GameObject::Pointer getFromPrototype(ExplosionType type) const;

private:
    std::unordered_map<ExplosionType, GameObject::Pointer> m_prototypes;

    static size_t const DIRECTIONS_N = 4;
    static std::array<sf::Vector2i, DIRECTIONS_N> const DIRECTIONS;
    static std::array<ExplosionType, DIRECTIONS_N> const BODY_TYPES;
    static std::array<ExplosionType, DIRECTIONS_N> const TAIL_TYPES;
};