#pragma once

#include <iostream>
#include <string>

#include <SFML/Config.hpp>

#include "scene.hpp"
#include "layer_map.hpp"
#include "scene_manager.hpp"
#include "game_resources_storage.hpp"
#include "fonts_storage.hpp"
#include "input_handler_factory.hpp"
#include "test_maps.hpp"
#include "engine.hpp"

void bomberman_game(std::string const& dir)
{
    GameObjectsPool::init(dir);
    LayerMap::instance().add({ "middleground", "movable" });

    sf::ContextSettings settings;

    auto const desctop_mode = sf::VideoMode::getDesktopMode();
    //sf::RenderWindow render_window(sf::VideoMode(desctop_mode.width, desctop_mode.height), "Bomberman", sf::Style::Fullscreen, settings);
    sf::RenderWindow render_window(sf::VideoMode(1024, 768), "Bomberman", sf::Style::Default, settings);
    render_window.setVerticalSyncEnabled(true);

    IInputHandler::Pointer input_handler = InputHandlerFactory::create(InputHandlerFactory::Type::KEYBOARD_MOUSE);
    auto camera = std::make_shared<Camera>();
    camera->setScale(0.25);

    engine::init(render_window, camera, input_handler);
    engine::get_fonts_storage().add("DEFAULT", dir + "gravity_regular.ttf", 5);
    engine::get_fonts_storage().add("GRAVITY_BOLD", dir + "gravity_bold.ttf", 8);
    engine::get_fonts_storage().add("GRAVITY_REGULAR", dir + "gravity_regular.ttf", 5);

    engine::get_sound_engine().add("BACKGROUND", dir, "temple.wav");
    engine::get_sound_engine().add("PLANT", dir, "plant.wav");
    engine::get_sound_engine().add("PROJECT", dir, "project.wav");
    engine::get_sound_engine().add("TELEPORT", dir, "teleport.wav");
    engine::get_sound_engine().add("BONUS", dir, "bonus.wav");
    engine::get_sound_engine().add("KILL", dir, "death.wav");
    engine::get_sound_engine().add("EXPLOSION", dir, "bomb.wav");
    engine::get_sound_engine().add("FANFARE", dir, "portal_fanfare.wav");
    engine::get_sound_engine().add("WIN", dir, "boss_kill.wav");
    engine::get_sound_engine().add("SPIKES", dir, "spikes_short.wav");

    auto test_temple_level = get_temple_map(render_window, 16);
    auto test_temple_level_2 = get_temple_map_2(render_window, 16);
    auto test_temple_level_3 = get_temple_map_3(render_window, 16);

    SceneManager::instance().registerScene("test_temple_level", test_temple_level->clone(), "test_temple_level_2");
    SceneManager::instance().registerScene("test_temple_level_2", test_temple_level_2->clone(), "test_temple_level");
    SceneManager::instance().registerScene("test_temple_level_3", test_temple_level_3->clone(), "test_temple_level_3");
    engine::get_sound_engine().play("BACKGROUND", 5, true);
    SceneManager::instance().setActive("test_temple_level");
    SceneManager::instance().runMainLoop();
}