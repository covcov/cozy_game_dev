#include "game_object.hpp"

#include "direction.hpp"
#include "movement_handler.hpp"

class Pushable : public Creature
{
public:
    Pushable(std::string const& name) : Creature(m_name) {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    bool isPossibleToPush(utl::DirectionType directionType);
    void move(float speed, utl::DirectionType directionType);

private:
    MovementHandler::Pointer m_movement_handler;
    sf::Vector2i m_target_cell = {};
    float m_speed = 0.f;
    bool m_is_moving = false;
};