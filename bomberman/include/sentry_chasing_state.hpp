#pragma once

#include "i_enemy_state.hpp"

class SentryChasingState : public IEnemyState
{
public:
    SentryChasingState(
        std::shared_ptr<IEnemy> enemy,
        Scene::Pointer scene,
        sf::Vector2i const& local_target_cell,
        int threshold_distance_to_target);

    virtual void getInformed(int message) override;
    virtual void update(float time_delta) override;

private:
    int m_threshold_distance_to_target;
    std::vector<sf::Vector2i> m_path;
    bool m_is_map_changed;
};