#pragma once

#include "game_object.hpp"

class TriggeredSpikes : public GameObject
{
public:
    TriggeredSpikes(std::string const& name);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    void setTriggerMessage(std::string const& trigger_message) { m_trigger_message = trigger_message; }

private:
    std::string m_trigger_message;
    bool m_is_activated;
    float m_elapsed_time;
    GameObject::Pointer m_character;

    static float constexpr TIME_PERIOD = 0.3f;
    static int constexpr MAX_VOLUME = 10;
    static float constexpr MAX_DISTANCE = 15.f;
};