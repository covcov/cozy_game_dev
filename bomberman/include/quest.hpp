#pragma once

#include <functional>
#include "text_object.hpp"

class Quest : public GameObject
{
public:
    Quest(std::string const& name);

    virtual void update(float time_delta);
    virtual GameObject::Pointer clone() const override;
    virtual void awake() override;
private:
    void meetPlayer();
    void greet();
    void suggestQuest();
    void controlQuest();
    void finishQuest();

    bool collideWithPlayer();
    void handleTextVisualization(std::string const& text);
    bool hasProceeded();
    void cleanUp();

private:
    bool m_connected_to_player = false;
    std::shared_ptr<TextObject> m_current_text_object;

    std::function<void()> m_state_processor;
    std::function<void()> m_last_state;
    GameObject::Pointer m_target;

    static int constexpr SPIDERS_TO_KILL = 1;
};