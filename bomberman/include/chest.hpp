#pragma once

#include "game_object.hpp"

class Chest : public GameObject
{
public:
    Chest(std::string const& name);

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    bool m_is_opening;
    float m_elapsed_time;
    static float constexpr WAIT_TIME = 2.5f;
};