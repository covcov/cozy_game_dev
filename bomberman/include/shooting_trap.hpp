#pragma once

#include "creature.hpp"

class ShootingTrap : public Creature
{
public:
    ShootingTrap(std::string const& name, GameObject::Pointer projectile_prototype);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    float m_elapsed_time;
    bool m_is_shooting;

    GameObject::Pointer m_projectile_prototype;
};