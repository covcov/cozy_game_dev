#include "game_object.hpp"
#include "follower.hpp"

class Candle : public GameObject
{
public:
    Candle(std::string const& name) : GameObject(name) {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
    virtual void draw(sf::RenderTarget& render_target) override;

    void setBurningTime(float burning_time);

private:
    static sf::Vector2f calculatePixelPosition(sf::Vector2f const& candle_size);

private:
    GameObject::Pointer m_light_processor;
    GameObject::Pointer m_pushable;
    std::shared_ptr<Follower> m_light_zone;
    std::shared_ptr<Follower> m_fire;
    sf::Sprite m_sprite;
    float m_burning_time = 0.f;
    float m_elaplsed_time = 0.f;
    float m_multiplier = 1.f;
    sf::Vector2f m_candle_position;
    sf::Vector2f m_candle_size;

    static sf::Vector2i const TEXTURE_SIZE;
    static float constexpr PIXEL_WIDTH = 4.f;
    static float constexpr PIXEL_LENGTH = 5.f;
    static float constexpr LIGHT_RADIUS = 48.f;
};