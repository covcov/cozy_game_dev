#pragma once

#include "creature.hpp"
#include "movement_handler.hpp"

class SnakeHead : public Creature
{
public:
    SnakeHead(std::string const& name);

    virtual void getInformed(int message) override;
    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const;

    virtual void kill() override;

    bool isMoving() const { return m_is_moving; }
    void applyDamage(float damage);
    void applyHeal(float amount);
    void addBodyPart(std::shared_ptr<Creature> body_part) { m_body_parts.push_back(body_part); }
private:
    void updatePath();
    void makeAnimationTransitions();

private:
    bool m_is_map_changed;
    bool m_reached_cell_center;
    bool m_is_moving;

    GameObject::Pointer m_target;
    std::vector<sf::Vector2i> m_path;
    std::unordered_set<std::string> walking_collision_mask;
    std::unique_ptr<MovementHandler> m_movement_handler;
    sf::Vector2i m_target_cell;
    std::vector<std::shared_ptr<Creature>> m_body_parts;
};