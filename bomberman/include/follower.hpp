#pragma once

#include "game_object.hpp"

class Follower : public GameObject
{
public:
    Follower(std::string const& name)
        : GameObject(name)
        , m_shift(0.f, 0.f)
    {}

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    void setTarget(GameObject::Pointer target);
    void applyShift(sf::Vector2f const& shift);
private:
    GameObject::Pointer m_target;
    sf::Vector2f m_shift;
};