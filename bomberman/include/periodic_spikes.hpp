#pragma once

#include "game_object.hpp"

class PeriodicSpikes : public GameObject
{
public:
    PeriodicSpikes(std::string const& name, float period, float delay);
    void reset(float period, float delay);
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
private:
    float m_period;
    float m_delay;
    float m_elapsed_time;
    bool m_is_waiting;
    bool m_is_activated;
    bool m_spikes_out;

    static float const AUDIBLE_DISTANCE_MAX;
};