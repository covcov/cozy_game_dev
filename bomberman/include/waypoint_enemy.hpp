#pragma once

#include "creature.hpp"
#include "movement_handler.hpp"

class WaypointEnemy : public Creature
{
public:
    WaypointEnemy(std::string const& name);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
    virtual void getInformed(int message) override;
    virtual void kill() override;
    virtual void applyDamage(float) override { kill(); }

    void setWaypoints(std::vector<sf::Vector2i> const& waypoints) { m_waypoints = waypoints; }

private:
    void updateTargetWaypoint();
    void updatePathIfNecessary();
    void moveToWaypoint(float time_delta);

private:
    std::vector<sf::Vector2i> m_waypoints;
    std::vector<sf::Vector2i> m_path;
    bool m_is_path_outdated;
    bool m_is_dead;

    sf::Vector2i m_target_waypoint;
    sf::Vector2i m_target_cell;
    size_t m_cell_index;

    MovementHandler::Pointer m_movement_handler;
};