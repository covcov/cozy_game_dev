#pragma once

#include "scene.hpp"
#include "path_search_engine.hpp"
#include "movement_handler.hpp"

class IEnemy;
class IEnemyState
{
public:
    IEnemyState(std::shared_ptr<IEnemy> enemy, Scene::Pointer scene, sf::Vector2i const& local_target_cell);

    virtual ~IEnemyState() = default;

    virtual void getInformed(int) {};
    virtual void update(float) = 0;

protected:
    static bool isNearTheTarget(
        GridMap::Pointer grid_map,
        sf::Vector2i cell,
        GameObject::Pointer target,
        int threshold_distance_to_target);

    sf::Vector2i chooseRandomDirection(std::shared_ptr<IEnemy> enemy, GridMap::Pointer grid_map);

protected:
    std::shared_ptr<IEnemy> m_enemy;
    Scene::Pointer m_scene;
    sf::Vector2i m_local_target_cell;
    MovementHandler m_movement_handler;


    static size_t const DIRECTIONS_N = 4;
    static std::array<sf::Vector2i, DIRECTIONS_N> const DIRECTIONS;
};