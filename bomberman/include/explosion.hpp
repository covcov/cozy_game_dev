#pragma once

#include "game_object.hpp"

class Explosion : public GameObject
{
public:
    Explosion(std::string const& name, float duration)
        : GameObject(name)
        , m_duration(duration)
        , m_elapsed_time(0.f)
    {
        collision_mask.insert("explosive");
    }

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    float m_duration;
    float m_elapsed_time;
};