#pragma once

#include <unordered_map>

#include "game_object.hpp"
#include "explosion_manager.hpp"

class GameObjectsPool
{
public:
    static void init(std::string const& dir);
    static GameObject::Pointer get(std::string const& name);
    static GameObject::Pointer getRandom(std::string const& name);

    template <typename T>
    static std::shared_ptr<T> getAs(std::string const& name);
    template <typename T>
    static std::shared_ptr<T> getRandomAs(std::string const& name);

private:
    static void initExplosion(std::string const& dir);
    static void initCharacters(std::string const& dir);
    static void initBonuses(std::string const& dir);
    static void initTemple(std::string const& dir);
    static void initOther(std::string const& dir);

    static GameObject::Pointer addDefaultObject(
        std::string const& name,
        std::string const& tag = GameObject::DEFAULT_TAG,
        std::string const& dir = "",
        size_t number = 0,
        sf::Vector2f const& animation_shift = { 0.f, 0.f });

    template <typename T>
    static GameObject::Pointer addDefaultObjectAs(
        std::string const& name,
        std::string const& tag = GameObject::DEFAULT_TAG,
        std::string const& dir = "",
        size_t number = 0,
        sf::Vector2f const& animation_shift = { 0.f, 0.f });

private:
    static std::unordered_map<std::string, std::vector<GameObject::Pointer>> game_objects;
    static ExplosionManager::Pointer explosion_manager;
    static bool is_initialized;
};

template <typename T>
inline std::shared_ptr<T> GameObjectsPool::getAs(std::string const& name)
{
    auto object = get(name);
    return object->cast<T>();
}

template <typename T>
inline std::shared_ptr<T> GameObjectsPool::getRandomAs(std::string const& name)
{
    auto object = getRandom(name);
    return object->cast<T>();
}
