#pragma once

#include "game_object.hpp"

bool resolve_direction(GameObject::Pointer target, sf::Vector2i const& direction, std::string const& prefix = "");