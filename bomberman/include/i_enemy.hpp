#pragma once

#include "i_enemy_state.hpp"
#include "creature.hpp"

class IEnemy : public Creature
{
public:
    enum class StateType
    {
        CHASING,
        PATROLLING,
        ATTACK,
        SUICIDE
    };

    IEnemy(std::string const& name, IEnemyState* state)
        : Creature(name)
        , m_state(state)
    {}

    virtual ~IEnemy();
    virtual void setState(StateType type) = 0;

    virtual void awake() override;
    void setTarget(GameObject::Pointer target) { m_target = target; }
    GameObject::Pointer getTarget() const { return m_target; }

protected:
    GameObject::Pointer m_target;
    IEnemyState* m_state;
};