#pragma once

#include "text_object.hpp"

struct TextObjectsFactory
{
    static std::shared_ptr<TextObject> getPauseText();
    static std::shared_ptr<TextObject> getQuestText(std::string const& text);
};