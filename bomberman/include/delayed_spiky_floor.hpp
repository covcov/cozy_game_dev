#pragma once

#include "game_object.hpp"

class DelayedSpikyFloor : public GameObject
{
public:
    DelayedSpikyFloor(std::string const& name, float reaction_time);

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    float m_reaction_time;
    float m_elapsed_time;
    bool m_is_activated;
    bool m_spikes_out;
};