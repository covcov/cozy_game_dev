#pragma once

#include <functional>

#include "creature.hpp"
#include "movement_handler.hpp"
#include "direction.hpp"

class Character : public Creature
{
public:
    Character(std::string const& name) : Creature(name) {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
    virtual void kill() override;
private:
    enum class StateType
    {
        MOVE,
        IDLE,
        PUSH
    };

    void move(float time_delta);
    void idle(float time_delta);
    void push(float time_delta);

    void switchState(StateType state_type);

    utl::DirectionType getPressedDirection() const;
    void handleProjecting();
private:
    MovementHandler::Pointer m_movement_handler;
    std::function<void(float)> m_state_processor;
    bool m_is_alive = true;
    bool m_is_switched_state = true;
    sf::Vector2f m_target_position;
    GameObject::Pointer m_projection_prototype;
    bool m_is_projected = false;
};