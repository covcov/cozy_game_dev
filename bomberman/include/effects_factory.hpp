#pragma once

#include <unordered_map>

#include "effect.hpp"
#include "game_object.hpp"

struct EffectsFactory
{
    enum class Type
    {
        DAMAGE_EFFECT,
        HEALING_EFFECT,
        PROJECTION_EFFECT,
        BLUR_EFFECT
    };

    static Effect::Pointer create(EffectsFactory::Type type);
private:
    static void init();

private:
    static std::unordered_map<Type, Effect::Pointer> effects;
    static bool is_init;
};