#pragma once

#include "game_object.hpp"
#include "explosion_manager.hpp"

struct Bomb : GameObject
{
    Bomb(std::string const& name,
        float time_to_explode,
        int fire_power,
        ExplosionManager::Pointer explosion_manager)
        : GameObject(name)
        , m_time_to_explode(time_to_explode)
        , m_elapsed_time(0.f)
        , m_fire_power(fire_power)
        , m_explosion_manager(explosion_manager)
    {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
    virtual void onDestroy() override;
    void setFirePower(int fire_power) { m_fire_power = fire_power; }

private:
    float m_time_to_explode;
    float m_elapsed_time;
    int m_fire_power;
    ExplosionManager::Pointer m_explosion_manager;
};