#pragma once

#include "creature.hpp"
#include "direction.hpp"

class Projectile : public Creature
{
public:
    Projectile(std::string const& name);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    void setDirection(utl::DirectionType direction);
    sf::Vector2i getDirection() const;

private:
    utl::DirectionType m_direction;
};