#pragma once

#include "i_enemy_state.hpp"

class SentryAttackState : public IEnemyState
{
public:
    SentryAttackState(std::shared_ptr<IEnemy> enemy, Scene::Pointer scene, sf::Vector2i const& local_target_cell);
    virtual ~SentryAttackState() = default;

    virtual void update(float time_delta) override;

private:
    float m_elapsed_time;
};