#pragma once

#include "snake_head.hpp"
#include "movement_handler.hpp"

class SnakeBody : public Creature
{
public:
    SnakeBody(std::string const& name, size_t index);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    virtual void kill() override;

private:
    void makeAnimationTransitions();

private:
    GameObject::Pointer m_target;
    std::shared_ptr<SnakeHead> m_head;
    std::unique_ptr<MovementHandler> m_movement_handler;
    sf::Vector2i m_target_cell;
    size_t m_index;
};