#pragma once

#include "game_object.hpp"

class PauseToggler : public GameObject
{
public:
    PauseToggler(std::string const& name)
        : GameObject(name)
        , m_is_paused(false)
        , m_time_scale(1.f)
        , m_pause_text_object(nullptr)
    {}

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    bool m_is_paused;
    float m_time_scale;
    GameObject::Pointer m_pause_text_object;
};