#pragma once

#include "game_object.hpp"

class Button : public GameObject
{
public:
    Button(std::string const& name);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    void setTriggerMessage(std::string const& trigger_message) { m_trigger_message = trigger_message; }

private:
    std::string m_trigger_message;
    bool m_is_activated;
};