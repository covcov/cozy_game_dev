#pragma once

#include "creature.hpp"
#include "direction.hpp"

class ShootingEnemy : public Creature
{
public:
    ShootingEnemy(std::string const& name, GameObject::Pointer projectile_prototype);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    void shoot(float time_delta);

private:
    bool m_is_shooting;
    float m_elapsed_time;
    utl::DirectionType m_direction_type;

    GameObject::Pointer m_projectile_prototype;
};