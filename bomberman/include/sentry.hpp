#pragma once

#include <functional>
#include <unordered_set>

#include "i_enemy.hpp"
#include "i_enemy_state.hpp"

class Sentry : public IEnemy
{
public:
    Sentry(std::string const& name, int threshold_distance_to_target);

    virtual void getInformed(int message) override;
    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    virtual void setState(StateType type) override;

    virtual void kill() override;

protected:
    bool m_is_dead;
    int m_threshold_distance_to_target;
};