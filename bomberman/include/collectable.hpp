#pragma once

#include "game_object.hpp"

class Collectable : public GameObject
{
public:
    Collectable(std::string const& name);

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

    virtual void setMessage(std::string const& message) { m_message = message; }
private:
    std::string m_message;
};