#pragma once

#include "i_input_handler.hpp"

struct InputHandlerFactory
{
    enum class Type
    {
        KEYBOARD_MOUSE,
        JOYSTIC
    };

    static IInputHandler::Pointer create(Type type);

private:
    static IInputHandler::Pointer createKeyboardMouseInputHandler();
};