#pragma once

enum Message
{
    MAP_CHANGED,
    ENEMY_SPAWNED,
    ENEMY_DIED,
    BOMB_PLANTED,
    BOMB_EXPLODED,
};