#pragma once

#include "i_enemy_state.hpp"

class SentryPatrollingState : public IEnemyState
{
public:
    SentryPatrollingState(
        std::shared_ptr<IEnemy> enemy,
        Scene::Pointer scene,
        sf::Vector2i const& local_target_cell,
        int threshold_distance_to_target)
        : IEnemyState(enemy, scene, local_target_cell)
        , m_threshold_distance_to_target(threshold_distance_to_target)
    {}

    virtual void update(float time_delta) override;

private:
    int m_threshold_distance_to_target;
};