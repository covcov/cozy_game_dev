#pragma once

#include "game_object.hpp"

class Brick : public GameObject
{
public:
    Brick(std::string const& name);

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    void processDestruction();

private:
    bool m_is_burning;
};