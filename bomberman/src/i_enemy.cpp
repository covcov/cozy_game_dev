#include "i_enemy.hpp"

#include "scene.hpp"

IEnemy::~IEnemy()
{
    if (m_state)
    {
        delete m_state;
    }
}

void IEnemy::awake()
{
    auto scene = engine::get_active_scene();
    if (scene)
    {
        auto targets = scene->findObjectsByTag("player");
        if (!targets.empty())
        {
            setTarget(targets.front());
        }
    }
}
