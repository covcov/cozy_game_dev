#include "snake_body.hpp"

#include "scene.hpp"
#include "game_objects_pool.hpp"
#include "effects_factory.hpp"

SnakeBody::SnakeBody(std::string const& name, size_t index)
    : Creature(name)
    , m_index(index)
{
    collision_mask.insert("explosion");
    registerAttribute("speed", 48);
}

void SnakeBody::awake()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    m_movement_handler = std::make_unique<MovementHandler>(scene, forceCast<Creature>());
    auto grid_map = scene->getGridMap();

    m_head = scene->findObjectByName("snake_head")->cast<SnakeHead>();
    if (m_head)
    {
        m_head->addBodyPart(cast<Creature>());
    }

    if (m_index == 0)
    {
        m_target = m_head;
    }
    else
    {
        auto objects = scene->findObjectsByName(m_name);
        for (auto object : objects)
        {
            auto object_casted = object->cast<SnakeBody>();
            if (object_casted && object_casted->m_index == m_index - 1)
            {
                m_target = object_casted;
            }
        }
    }

    if (m_target)
    {
        m_target_cell = grid_map->worldToCellPosition(m_target->position);
    }
}

void SnakeBody::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;
    if (!m_head->isMoving()) return;

    auto grid_map = scene->getGridMap();

    animation_graph.update(time_delta);
    makeAnimationTransitions();
    if (m_movement_handler->moveTowardsGridCell(time_delta, m_head->getAttributeCasted<float>("speed"), m_target_cell))
    {
        auto grid_map = scene->getGridMap();
        m_target_cell = grid_map->worldToCellPosition(m_target->position);
    }

    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    for (auto obj : collided_objects)
    {
        if (obj->tag == "explosion" && !effects_storage.contains("damage"))
        {
            effects_storage.add("damage", EffectsFactory::create(EffectsFactory::Type::DAMAGE_EFFECT));
            m_head->applyDamage(m_head->getAttribute("damage_per_explosion"));
        }
    }
}

GameObject::Pointer SnakeBody::clone() const
{
    auto instance = std::make_shared<SnakeBody>(m_name, m_index);
    cloneComponents(instance);
    return instance;
}

void SnakeBody::kill()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    destroy();
    auto effect = GameObjectsPool::get("explosion_effect");
    effect->position = position;
    scene->add(effect, "foreground");
}

void SnakeBody::makeAnimationTransitions()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto grid_map = scene->getGridMap();
    sf::Vector2i cell = grid_map->worldToCellPosition(position);
    sf::Vector2i direction = m_target_cell - cell;
    if (direction.x || direction.y)
    {
        if (direction.x == 0)
        {
            animation_graph.makeTransition(direction.y > 0 ? "DOWN" : "UP");
        }
        else if (direction.y == 0)
        {
            animation_graph.makeTransition(direction.x > 0 ? "RIGHT" : "LEFT");
        }
    }
}
