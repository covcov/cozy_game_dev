#include "snake_head.hpp"

#include "scene.hpp"
#include "message.hpp"
#include "health_bar.hpp"
#include "effects_factory.hpp"
#include "game_objects_pool.hpp"
#include "experience_object.hpp"

SnakeHead::SnakeHead(std::string const& name)
    : Creature(name)
    , m_is_map_changed(true)
    , m_reached_cell_center(false)
    , m_is_moving(false)
{
    collision_mask.insert("explosion");
    collision_mask.insert("bomb");
    collision_mask.insert("player");

    registerAttribute("speed", 48);
    registerAttribute("health", 100);
    registerAttribute("health_max", 100);
    registerAttribute("damage_per_explosion", 10);
    registerAttribute("health_per_bomb", 5);
}

void SnakeHead::getInformed(int message)
{
    if (message == Message::MAP_CHANGED)
    {
        m_is_map_changed = true;
    }
}

void SnakeHead::awake()
{
    walking_collision_mask = { "wall", "brick", "enemy" };

    auto scene = engine::get_active_scene();
    m_movement_handler = std::make_unique<MovementHandler>(scene, forceCast<Creature>());
    auto grid_map = scene->getGridMap();
    auto targets = scene->findObjectsByTag("player");
    if (!targets.empty())
    {
        m_target = targets.front();
        m_is_moving = true;
        updatePath();
        if (!m_path.empty())
        {
            m_target_cell = m_path.back();
            m_path.pop_back();
        }
    }

    auto health_bar = GameObject::Pointer(new HealthBar("snake_health_bar", cast<Creature>(), { 24, 2 }));
    scene->add(health_bar, "foreground");
}

void SnakeHead::update(float time_delta)
{
    if (!m_is_moving) return;

    auto grid_map = engine::get_grid_map();

    animation_graph.update(time_delta);
    makeAnimationTransitions();

    if (m_movement_handler->moveTowardsGridCell(time_delta, m_target_cell))
    {
        updatePath();
        if (!m_path.empty())
        {
            m_target_cell = m_path.back();
            m_path.pop_back();
        }
        else
        {
            m_is_moving = false;
        }
    }

    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    for (auto obj : collided_objects)
    {
        if (obj->tag == "explosion" && !effects_storage.contains("damage"))
        {
            effects_storage.add("damage", EffectsFactory::create(EffectsFactory::Type::DAMAGE_EFFECT));
            applyDamage(getAttribute("damage_per_explosion"));
        }
        else if (obj->tag == "bomb")
        {
            engine::get_sound_engine().play("BONUS", 100, false, 0.75);
            effects_storage.add("healing", EffectsFactory::create(EffectsFactory::Type::HEALING_EFFECT));

            for (auto obj : m_body_parts)
            {
                obj->effects_storage.add("healing", EffectsFactory::create(EffectsFactory::Type::HEALING_EFFECT));
            }

            applyHeal(getAttribute("health_per_bomb"));
            obj->destroy();
        }
        else if (obj->tag == "player")
        {
            obj->forceCast<Creature>()->kill();
        }
    }
}

GameObject::Pointer SnakeHead::clone() const
{
    auto instance = std::make_shared<SnakeHead>(m_name);
    cloneComponents(instance);
    return instance;
}

void SnakeHead::kill()
{
    auto scene = engine::get_active_scene();
    destroy();
    auto effect = GameObjectsPool::get("explosion_effect");
    effect->position = position;
    scene->add(effect, "foreground");
    engine::get_sound_engine().play("WIN", 100, false, 1.25);
    auto experience = GameObject::Pointer(new ExperienceObject("xp", position, 500, 2.f));
    scene->add(experience, "foreground");
}

void SnakeHead::applyDamage(float damage)
{
    float health = getAttribute("health");
    damage = std::min(damage, health);
    setAttribute("health", health - damage);
    if (utl::almost_zero(std::fabs(health - damage)))
    {
        kill();
        for (auto body_part : m_body_parts)
        {
            body_part->kill();
        }
    }
}

void SnakeHead::applyHeal(float amount)
{
    float health = getAttribute("health");
    float health_max = getAttribute("health_max");
    setAttribute("health", std::min(health_max, health + amount));
}

void SnakeHead::updatePath()
{
    if (m_is_map_changed && m_target)
    {
        auto const grid_map = engine::get_grid_map();
        auto const path_search_engine = engine::get_path_search_engine();
        auto const cell_from = grid_map->worldToCellPosition(position);
        auto const cell_to = grid_map->worldToCellPosition(m_target->position);
        m_path = path_search_engine->findPath(cell_from, cell_to, walking_collision_mask);
        m_is_map_changed = false;
    }
}

void SnakeHead::makeAnimationTransitions()
{
    auto grid_map = engine::get_grid_map();
    sf::Vector2i const cell = grid_map->worldToCellPosition(position);
    sf::Vector2i const direction = m_target_cell - cell;
    if (direction.x || direction.y)
    {
        if (direction.x == 0)
        {
            animation_graph.makeTransition(direction.y > 0 ? "DOWN" : "UP");
        }
        else if (direction.y == 0)
        {
            animation_graph.makeTransition(direction.x > 0 ? "RIGHT" : "LEFT");
        }
    }
}