#include "explosion.hpp"
#include "scene.hpp"

void Explosion::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;
    auto grid_map = scene->getGridMap();

    animation_graph.update(time_delta);
    m_elapsed_time += time_delta;
    if (m_elapsed_time > m_duration)
    {
        destroy();
    }

    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    for (auto obj : collided_objects)
    {
        obj->destroy();
    }
}

GameObject::Pointer Explosion::clone() const
{
    GameObject::Pointer instance = std::make_shared<Explosion>(m_name, m_duration);
    cloneComponents(instance);
    return instance;
}
