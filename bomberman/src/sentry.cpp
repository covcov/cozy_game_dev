#include "sentry.hpp"

#include <queue>
#include <array>

#include "scene.hpp"
#include "message.hpp"

#include "sentry_chasing_state.hpp"
#include "sentry_patrolling_state.hpp"
#include "sentry_attack_state.hpp"

#include "experience_object.hpp"
#include "game_objects_pool.hpp"

Sentry::Sentry(std::string const& name, int threshold_distance_to_target)
    : IEnemy(name, nullptr)
    , m_is_dead(false)
    , m_threshold_distance_to_target(threshold_distance_to_target)
{
    registerAttribute("speed", 16);
    registerAttribute("damage_deal", 16);
    registerAttribute("time_between_attacks", 0.4f);
    collision_mask = { "brick", "wall", "bomb", "explosion", "player" };
}

void Sentry::getInformed(int message)
{
    m_state->getInformed(message);
}

void Sentry::awake()
{
    auto scene = engine::get_active_scene();
    if (scene)
    {
        IEnemy::awake();
        auto grid_map = scene->getGridMap();
        scene->inform("player", Message::ENEMY_SPAWNED);
        auto const& cell = grid_map->worldToCellPosition(position);
        m_state = new SentryPatrollingState(forceCast<IEnemy>(), scene, cell, m_threshold_distance_to_target);
    }
}

void Sentry::update(float time_delta)
{
    animation_graph.update(time_delta);
    auto scene = engine::get_active_scene();
    if (!m_is_dead)
    {
        auto grid_map = scene->getGridMap();
        auto const cell = grid_map->worldToCellPosition(position);

        if (grid_map->cellContainsTag(cell, "explosion"))
        {
            kill();
        }
        else
        {
            m_state->update(time_delta);
        }
    }

    if (m_is_dead)
    {
        destroy();
        scene->inform("player", Message::ENEMY_DIED);
        auto effect = GameObjectsPool::get("explosion_effect");
        effect->position = position;
        scene->add(effect, "foreground");

        auto output = scene->blackboard.get<int>(m_name);
        scene->blackboard.set(m_name, output.isValid() ? output.getValue() + 1 : 1);
    }
}

GameObject::Pointer Sentry::clone() const
{
    GameObject::Pointer enemy(new Sentry(m_name, m_threshold_distance_to_target));
    cloneComponents(enemy);
    return enemy;
}

void Sentry::setState(StateType type)
{
    auto scene = engine::get_active_scene();

    if (scene)
    {
        auto grid_map = scene->getGridMap();
        auto const cell = grid_map->worldToCellPosition(position);

        if (m_state)
        {
            delete m_state;
        }

        switch (type)
        {
        case IEnemy::StateType::CHASING:
            m_state = new SentryChasingState(forceCast<IEnemy>(), scene, cell, m_threshold_distance_to_target);
            break;
        case IEnemy::StateType::PATROLLING:
            m_state = new SentryPatrollingState(forceCast<IEnemy>(), scene, cell, m_threshold_distance_to_target);
            break;
        case IEnemy::StateType::ATTACK:
            m_state = new SentryAttackState(forceCast<IEnemy>(), scene, cell);
            break;
        }
    }
}

void Sentry::kill()
{
    if (!m_is_dead)
    {
        auto experience = GameObject::Pointer(new ExperienceObject("xp", position, 100, 2.f));
        engine::get_active_scene()->add(experience, "foreground");
    }

    m_is_dead = true;
    animation_graph.makeTransition("KILL");
}