#include "sentry_chasing_state.hpp"

#include "i_enemy.hpp"
#include "message.hpp"
#include "animation_helpers.hpp"
#include "algorithm_extra.hpp"

SentryChasingState::SentryChasingState(
    std::shared_ptr<IEnemy> enemy,
    Scene::Pointer scene,
    sf::Vector2i const& local_target_cell,
    int threshold_distance_to_target)
    : IEnemyState(enemy, scene, local_target_cell)
    , m_threshold_distance_to_target(threshold_distance_to_target)
    , m_is_map_changed(true)
{
    m_enemy->animation_graph.makeTransition("MOVE");
}

void SentryChasingState::getInformed(int message)
{
    if (!m_is_map_changed && message == Message::MAP_CHANGED)
    {
        m_is_map_changed = true;
    }
}

void SentryChasingState::update(float time_delta)
{
    auto target = m_enemy->getTarget();
    auto grid_map = m_scene->getGridMap();
    auto cell = grid_map->worldToCellPosition(m_enemy->position);

    if (m_movement_handler.moveTowardsGridCell(time_delta, m_local_target_cell))
    {
        auto collided_objects = grid_map->getCollidedObjects(m_enemy);
        if (utl::contains_linear(collided_objects, [](GameObject::Pointer x) { return x->tag == "player"; }))
        {
            resolve_direction(m_enemy, grid_map->worldToCellPosition(target->position) - grid_map->worldToCellPosition(m_enemy->position));
            return m_enemy->setState(IEnemy::StateType::ATTACK);
        }

        if (m_is_map_changed)
        {
            m_path = m_scene->getPathSearchEngine()->findPathToObjectWithTag(
                cell,
                { "brick", "wall", "bomb", "pushable" },
                target->tag,
                m_threshold_distance_to_target);

            m_is_map_changed = false;
        }

        if (m_path.empty())
        {
            return m_enemy->setState(IEnemy::StateType::PATROLLING);
        }
        else
        {
            m_local_target_cell = m_path.back();
            m_path.pop_back();
            resolve_direction(m_enemy, m_local_target_cell - cell);
        }
    }
}