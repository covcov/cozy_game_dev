#include "game_objects_pool.hpp"

#include "animation_graph_factory.hpp"
#include "collider_factory.hpp"
#include "attribute_bonus_factory.hpp"
#include "character.hpp"
#include "sentry.hpp"
#include "waypoint_enemy.hpp"
#include "shooting_enemy.hpp"
#include "triggered_spikes.hpp"
#include "periodic_spikes.hpp"
#include "button.hpp"
#include "bomb.hpp"
#include "brick.hpp"
#include "effects_factory.hpp"
#include "explosion.hpp"
#include "animated_effect.hpp"
#include "pause_toggler.hpp"
#include "chest.hpp"
#include "snake_head.hpp"
#include "snake_body.hpp"
#include "game_object_with_tooltip.hpp"
#include "quest.hpp"
#include "pushable.hpp"
#include "shooting_trap.hpp"
#include "projectile.hpp"
#include "follower.hpp"
#include "collectable.hpp"
#include "random_engine.hpp"
#include "candle.hpp"
#include "light_source.hpp"

std::unordered_map<std::string, std::vector<GameObject::Pointer>> GameObjectsPool::game_objects = {};
ExplosionManager::Pointer GameObjectsPool::explosion_manager = nullptr;
bool GameObjectsPool::is_initialized = false;

void GameObjectsPool::init(std::string const& dir)
{
    if (!is_initialized)
    {
        initExplosion(dir);
        initCharacters(dir);
        initBonuses(dir);
        initTemple(dir);
        initOther(dir);

        is_initialized = true;
    }
    else
    {
        std::cout << "WARNING: Game objects pool has been initialized already\n";
    }
}

GameObject::Pointer GameObjectsPool::get(std::string const& name)
{
    auto it = game_objects.find(name);
    if (it != game_objects.end())
    {
        return it->second.front()->clone();
    }
    else
    {
        std::cout << "WARNING: Object " + name + " is not found\n";
    }

    return nullptr;
}

GameObject::Pointer GameObjectsPool::getRandom(std::string const& name)
{
    auto it = game_objects.find(name);
    if (it != game_objects.end())
    {
        return it->second[RandomEngine::randomRange(0, it->second.size() - 1)]->clone();
    }
    else
    {
        std::cout << "WARNING: Object " + name + " is not found\n";
    }

    return nullptr;
}

void GameObjectsPool::initExplosion(std::string const& dir)
{
    auto explosion_center = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_center->animation_graph = AnimationGraphFactory::create(dir, "explosion_center.anim");
    explosion_center->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_center, { 16, 16 });
    game_objects["explosion_center"].push_back(explosion_center);

    auto explosion_left = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_left->animation_graph = AnimationGraphFactory::create(dir, "explosion_left.anim");
    explosion_left->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_left, { 16, 16 });
    game_objects["explosion_left"].push_back(explosion_left);

    auto explosion_top = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_top->animation_graph = AnimationGraphFactory::create(dir, "explosion_top.anim");
    explosion_top->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_top, { 16, 16 });
    game_objects["explosion_top"].push_back(explosion_top);

    auto explosion_right = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_right->animation_graph = AnimationGraphFactory::create(dir, "explosion_right.anim");
    explosion_right->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_right, { 16, 16 });
    game_objects["explosion_right"].push_back(explosion_right);

    auto explosion_bottom = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_bottom->animation_graph = AnimationGraphFactory::create(dir, "explosion_bottom.anim");
    explosion_bottom->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_bottom, { 16, 16 });
    game_objects["explosion_bottom"].push_back(explosion_bottom);

    auto explosion_left_tail = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_left_tail->animation_graph = AnimationGraphFactory::create(dir, "explosion_left_tail.anim");
    explosion_left_tail->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_left_tail, { 16, 16 });
    game_objects["explosion_left_tail"].push_back(explosion_left_tail);

    auto explosion_top_tail = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_top_tail->animation_graph = AnimationGraphFactory::create(dir, "explosion_top_tail.anim");
    explosion_top_tail->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_top_tail, { 16, 16 });
    game_objects["explosion_top_tail"].push_back(explosion_top_tail);

    auto explosion_right_tail = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_right_tail->animation_graph = AnimationGraphFactory::create(dir, "explosion_right_tail.anim");
    explosion_right_tail->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_right_tail, { 16, 16 });
    game_objects["explosion_right_tail"].push_back(explosion_right_tail);

    auto explosion_bottom_tail = std::make_shared<Explosion>("explosion", 0.9f);
    explosion_bottom_tail->animation_graph = AnimationGraphFactory::create(dir, "explosion_bottom_tail.anim");
    explosion_bottom_tail->tag = "explosion";
    ColliderFactory::addBoxCollider(explosion_bottom_tail, { 16, 16 });
    game_objects["explosion_bottom_tail"].push_back(explosion_bottom_tail);

    explosion_manager = ExplosionManager::create();
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::BOTTOM_TAIL, explosion_bottom_tail);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::RIGHT_TAIL, explosion_right_tail);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::TOP_TAIL, explosion_top_tail);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::LEFT_TAIL, explosion_left_tail);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::BOTTOM_BODY, explosion_bottom);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::RIGHT_BODY, explosion_right);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::TOP_BODY, explosion_top);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::LEFT_BODY, explosion_left);
    explosion_manager->setPrototype(ExplosionManager::ExplosionType::CENTER, explosion_center);

    auto bomb = std::make_shared<Bomb>("bomb", 2.f, 3, explosion_manager);
    bomb->animation_graph = AnimationGraphFactory::create(dir, "bomb.anim");
    bomb->tag = "bomb";
    ColliderFactory::addBoxCollider(bomb, { 16, 16 });
    game_objects["bomb"].push_back(bomb);

    auto projectile = std::make_shared<Projectile>("projectile");
    projectile->animation_graph = AnimationGraphFactory::create(dir, "projectile.anim");
    projectile->tag = "projectile";
    projectile->destroy_on_collision_mask = { "wall", "brick" };
    projectile->collision_mask = { "player", "enemy", "pushable" };
    ColliderFactory::addCircleCollider(projectile, 5);

    auto shooting_trap = std::make_shared<ShootingTrap>("shooting_trap", projectile);
    shooting_trap->animation_graph = AnimationGraphFactory::create(dir, "trap.anim");
    shooting_trap->tag = "bomb";
    ColliderFactory::addBoxCollider(shooting_trap, { 16, 16 });
    game_objects["shooting_trap"].push_back(shooting_trap);
}

void GameObjectsPool::initCharacters(std::string const& dir)
{
    auto character = std::make_shared<Character>("character");
    character->animation_graph = AnimationGraphFactory::create(dir, "character.anim", { 0.f, -6.f });
    character->tag = "player";
    ColliderFactory::addCircleCollider(character, 5);
    game_objects["character"].push_back(character);

    auto projection = std::make_shared<GameObject>("projection");
    projection->animation_graph = AnimationGraphFactory::create(dir, "projection.anim", { 0.f, -6.f });
    projection->tag = "projection";
    ColliderFactory::addCircleCollider(projection, 5);
    game_objects["projection"].push_back(projection);

    auto quest_giver = std::make_shared<Quest>("quest");
    quest_giver->animation_graph = AnimationGraphFactory::create(dir, "quest_giver.anim", { 0.f, -6.f });
    ColliderFactory::addCircleCollider(quest_giver, 16);
    game_objects["quest"].push_back(quest_giver);

    auto sentry = std::make_shared<Sentry>("spider", 8);
    sentry->animation_graph = AnimationGraphFactory::create(dir, "spider.anim", { 0.f, -1.f });
    sentry->tag = "enemy";
    ColliderFactory::addCircleCollider(sentry, 10, { 0, 0 });
    game_objects["sentry"].push_back(sentry);

    auto spider = std::make_shared<WaypointEnemy>("spider");
    spider->animation_graph = AnimationGraphFactory::create(dir, "spider.anim", { 0.f, -1.f });
    spider->tag = "enemy";
    ColliderFactory::addCircleCollider(spider, 6);
    game_objects["spider"].push_back(spider);

    {
        auto arrow = std::make_shared<Projectile>("arrow");
        arrow->animation_graph = AnimationGraphFactory::create(dir, "arrow.anim");
        arrow->tag = "arrow";
        arrow->destroy_on_collision_mask = { "pushable", "wall", "brick" };
        arrow->collision_mask = { "player", "enemy" };
        ColliderFactory::addCircleCollider(arrow, 5);

        auto sniper = std::make_shared<ShootingEnemy>("sniper", arrow);
        sniper->animation_graph = AnimationGraphFactory::create(dir, "goblin_sniper.anim", { 0.f, -1.f });
        sniper->tag = "enemy";
        ColliderFactory::addCircleCollider(sniper, 8);
        game_objects["sniper"].push_back(sniper);
    }

    {
        auto snake_head = std::make_shared<SnakeHead>("snake_head");
        snake_head->animation_graph = AnimationGraphFactory::create(dir, "snake_head.anim", { 0.f, -1.5f });
        snake_head->tag = "enemy";
        ColliderFactory::addCircleCollider(snake_head, 6, { 0, 0 });
        game_objects["snake_head"].push_back(snake_head);

        auto snake_neck = std::make_shared<SnakeBody>("snake_body", 0);
        snake_neck->animation_graph = AnimationGraphFactory::create(dir, "snake_body.anim");
        snake_neck->tag = "enemy";
        ColliderFactory::addCircleCollider(snake_neck, 6, { 0, 0 });
        game_objects["snake_neck"].push_back(snake_neck);

        auto snake_body = std::make_shared<SnakeBody>("snake_body", 1);
        snake_body->animation_graph = AnimationGraphFactory::create(dir, "snake_body.anim");
        snake_body->tag = "enemy";
        ColliderFactory::addCircleCollider(snake_body, 6, { 0, 0 });
        game_objects["snake_body"].push_back(snake_body);

        auto snake_tail = std::make_shared<SnakeBody>("snake_body", 2);
        snake_tail->animation_graph = AnimationGraphFactory::create(dir, "snake_tail.anim");
        snake_tail->tag = "enemy";
        ColliderFactory::addCircleCollider(snake_tail, 6, { 0, 0 });
        game_objects["snake_tail"].push_back(snake_tail);
    }
}

void GameObjectsPool::initBonuses(std::string const& dir)
{
    auto speed_bonus = AttributeBonusFactory::create(AttributeBonusFactory::Type::SPEED);
    speed_bonus->animation_graph = AnimationGraphFactory::create(dir, "speed_bonus.anim");
    speed_bonus->tag = "speed_bonus";
    ColliderFactory::addBoxCollider(speed_bonus, { 16, 16 });
    game_objects["speed_bonus"].push_back(speed_bonus);

    auto power_bonus = AttributeBonusFactory::create(AttributeBonusFactory::Type::POWER);
    power_bonus->animation_graph = AnimationGraphFactory::create(dir, "power_bonus.anim");
    power_bonus->tag = "power_bonus";
    ColliderFactory::addBoxCollider(power_bonus, { 16, 16 });
    game_objects["power_bonus"].push_back(power_bonus);

    auto charge_bonus = AttributeBonusFactory::create(AttributeBonusFactory::Type::CHARGE);
    charge_bonus->animation_graph = AnimationGraphFactory::create(dir, "charge_bonus.anim");
    charge_bonus->tag = "charge_bonus";
    ColliderFactory::addBoxCollider(charge_bonus, { 16, 16 });
    game_objects["charge_bonus"].push_back(charge_bonus);
}

void GameObjectsPool::initTemple(std::string const& dir)
{
    auto triggered_spikes = std::make_shared<TriggeredSpikes>("triggered_spikes");
    triggered_spikes->animation_graph = AnimationGraphFactory::create(dir, "triggered_spikes.anim");
    triggered_spikes->tag = "spike";
    ColliderFactory::addBoxCollider(triggered_spikes, { 6, 6 });
    game_objects["triggered_spikes"].push_back(triggered_spikes);

    auto periodic_spikes = std::make_shared<PeriodicSpikes>("periodic_spikes", 0.8f, 0.f);
    periodic_spikes->animation_graph = AnimationGraphFactory::create(dir, "triggered_spikes.anim");
    periodic_spikes->tag = "spike";
    ColliderFactory::addBoxCollider(periodic_spikes, { 6, 6 });
    game_objects["periodic_spikes"].push_back(periodic_spikes);

    auto chest = std::make_shared<Chest>("chest");
    chest->animation_graph = AnimationGraphFactory::create(dir, "chest.anim");
    ColliderFactory::addBoxCollider(chest, { 16, 16 });
    game_objects["chest"].push_back(chest);

    auto key = std::make_shared<Collectable>("key");
    key->tag = "key";
    key->animation_graph = AnimationGraphFactory::create(dir, "key.anim");
    ColliderFactory::addBoxCollider(key, { 6, 6 });
    game_objects["key"].push_back(key);

    auto gray_temple_torch = std::make_shared<LightSource>("gray_temple_torchrch", 72.f);
    gray_temple_torch->animation_graph = AnimationGraphFactory::create(dir, "gray_temple_torch.anim", { 0.f, -8.f });
    game_objects["gray_temple_torch"].push_back(gray_temple_torch);

    addDefaultObjectAs<Candle>("candle");
    addDefaultObjectAs<Follower>("light_zone");
    addDefaultObjectAs<Follower>("fire", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_floor", GameObject::DEFAULT_TAG, dir, 1);
    addDefaultObject("blue_temple_floor", GameObject::DEFAULT_TAG, dir, 2);
    addDefaultObject("blue_temple_floor", GameObject::DEFAULT_TAG, dir, 3);
    addDefaultObject("blue_temple_floor", GameObject::DEFAULT_TAG, dir, 4);
    addDefaultObject("blue_temple_wall_bottom_end", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_bottom_intersection", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_bottom_left_corner", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_bottom_right_corner", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_horizontal", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_intersection", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_left_end", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_left_intersection", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_right_end", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_right_intersection", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_single", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_top_end", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_top_intersection", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_top_left_corner", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_top_right_corner", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_vertical", GameObject::DEFAULT_TAG, dir);
    addDefaultObject("blue_temple_wall_edge", GameObject::DEFAULT_TAG, dir, 1);
    addDefaultObject("blue_temple_wall_edge", GameObject::DEFAULT_TAG, dir, 2);
    addDefaultObject("blue_temple_wall_edge", GameObject::DEFAULT_TAG, dir, 3);
    addDefaultObject("blue_temple_wall_edge", GameObject::DEFAULT_TAG, dir, 4);

    ColliderFactory::addBoxCollider(addDefaultObject("gray_temple_pillar", GameObject::DEFAULT_TAG, dir, 0, { 0.f, -8.f }), { 16, 16 });

    ColliderFactory::addBoxCollider(addDefaultObject("blue_temple_miscellaneous", GameObject::DEFAULT_TAG, dir, 1), { 16, 16 });
    ColliderFactory::addBoxCollider(addDefaultObject("blue_temple_miscellaneous", GameObject::DEFAULT_TAG, dir, 2), { 16, 16 });

    ColliderFactory::addBoxCollider(addDefaultObjectAs<Button>("blue_temple_button", GameObject::DEFAULT_TAG, dir), { 12, 12 });
    ColliderFactory::addBoxCollider(addDefaultObjectAs<Pushable>("box", "pushable", dir, 1, { 0.f, -8.f }), { 16, 16 });
    ColliderFactory::addBoxCollider(addDefaultObjectAs<Pushable>("box", "pushable", dir, 2, { 0.f, -8.f }), { 16, 16 });
}

void GameObjectsPool::initOther(std::string const& dir)
{
    addDefaultObjectAs<PauseToggler>("pause_toggler", "ui_object");
    addDefaultObjectAs<AnimatedEffect>("explosion_effect", "explosion_effect", dir, 1);
    addDefaultObjectAs<AnimatedEffect>("explosion_effect", "explosion_effect", dir, 2);
}

GameObject::Pointer GameObjectsPool::addDefaultObject(
    std::string const& name,
    std::string const& tag,
    std::string const& dir,
    size_t index,
    sf::Vector2f const& animation_shift)
{
    return addDefaultObjectAs<GameObject>(name, tag, dir, index, animation_shift);
}

template <typename T>
GameObject::Pointer GameObjectsPool::addDefaultObjectAs(
    std::string const& name,
    std::string const& tag,
    std::string const& dir,
    size_t index,
    sf::Vector2f const& animation_shift)
{
    auto instance = std::make_shared<T>(name);
    instance->tag = tag;

    if (!dir.empty())
    {
        std::string const prefix = index == 0 ? "" : "_" + std::to_string(index);
        instance->animation_graph = AnimationGraphFactory::create(dir, name + prefix + ".anim", animation_shift);
    }

    game_objects[name].push_back(instance);
    return game_objects[name].back();
}
