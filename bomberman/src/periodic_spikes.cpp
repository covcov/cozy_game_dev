#include "periodic_spikes.hpp"
#include "scene.hpp"
#include "creature.hpp"
#include "game_resources_storage.hpp"

float const PeriodicSpikes::AUDIBLE_DISTANCE_MAX = 64.f;

PeriodicSpikes::PeriodicSpikes(std::string const& name, float period, float delay)
    : GameObject(name)
    , m_period(period)
    , m_delay(delay)
    , m_elapsed_time(-0.f)
    , m_is_waiting(true)
    , m_is_activated(false)
    , m_spikes_out(false)
{
    collision_mask = { "player" };
}

void PeriodicSpikes::reset(float period, float delay)
{
    m_period = period;
    m_delay = delay;
}

void PeriodicSpikes::update(float time_delta)
{
    animation_graph.update(time_delta);
    m_elapsed_time += time_delta;

    auto scene = engine::get_active_scene();
    if (scene)
    {
        auto grid_map = scene->getGridMap();
        auto const cell = grid_map->worldToCellPosition(position);

        bool const activation_condition = m_is_activated && animation_graph.getActive()->isFinished();
        bool const deactivation_condition = !m_is_activated && m_elapsed_time > m_period;

        if (activation_condition || deactivation_condition)
        {
            m_is_activated = !m_is_activated;
            m_elapsed_time = 0.f;
            if (m_is_activated)
            {
                animation_graph.makeTransition("ACTIVATE");
                auto character = scene->findObjectByName("character");
                float const dist_to_spikes = std::min(utl::distance(character->position.get(), position.get()), AUDIBLE_DISTANCE_MAX);
                int const volume = static_cast<int>(100. * (1. - dist_to_spikes / AUDIBLE_DISTANCE_MAX));
                engine::get_sound_engine().play("SPIKES", volume, false);
            }
            else
            {
                animation_graph.makeTransition("DEACTIVATE");
            }
        }

        if (m_is_activated)
        {
            auto const collided_objects = grid_map->getCollidedObjects(shared_from_this());
            for (auto const& obj : collided_objects)
            {
                auto creature = std::dynamic_pointer_cast<Creature>(obj);
                if (creature)
                {
                    creature->kill();
                }
            }
        }
    }
}

GameObject::Pointer PeriodicSpikes::clone() const
{
    auto instance = std::make_shared<PeriodicSpikes>(m_name, m_period, m_delay);
    cloneComponents(instance);
    return instance;
}
