#include "pause_toggler.hpp"

#include "text_objects_factory.hpp"
#include "game_resources_storage.hpp"
#include "scene_manager.hpp"

void PauseToggler::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    auto input_handler = engine::get_input_handler();
    if (input_handler->isPressedInUpdate("PAUSE"))
    {
        if (m_is_paused) 
        {
            m_pause_text_object->destroy();
            scene->setTimeScale(m_time_scale);
        }
        else
        {
            m_pause_text_object = TextObjectsFactory::getPauseText();
            scene->add(m_pause_text_object, "ui");
            m_time_scale = scene->getTimeScale();
            scene->setTimeScale(0.f);
        }

        m_is_paused = !m_is_paused;
    }
    else if (input_handler->isPressedInUpdate("RESET"))
    {
        SceneManager::instance().reset();
    }
    else if (input_handler->isPressedInUpdate("MUTE"))
    {
        engine::get_sound_engine().toggleMute();
    }
}

GameObject::Pointer PauseToggler::clone() const
{
    GameObject::Pointer instance = std::make_shared<PauseToggler>(m_name);
    cloneComponents(instance);
    return instance;
}
