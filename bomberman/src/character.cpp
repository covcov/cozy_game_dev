#include "character.hpp"
#include "scene_manager.hpp"
#include "engine.hpp"
#include "message.hpp"
#include "pushable.hpp"
#include "animation_helpers.hpp"
#include "game_objects_pool.hpp"
#include "light_processor.hpp"
#include "effects_factory.hpp"
#include "ray_caster.hpp"

void Character::awake()
{
    registerAttribute("speed", 72);
    registerAttribute("push_speed", 48);

    collision_mask = { "brick", "bomb", "wall" };
    m_movement_handler.reset(new MovementHandler(engine::get_active_scene(), forceCast<Creature>()));

    switchState(StateType::IDLE);
    auto camera = engine::get_camera();
    camera->setTarget(shared_from_this());

    m_projection_prototype = GameObjectsPool::get("projection");
}

void Character::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    auto time_scale = scene->getTimeScale();
    if (utl::almost_zero(time_scale)) return;
    animation_graph.update(time_delta);
    auto input_handler = engine::get_input_handler();

    if (m_is_alive)
    {
        if (input_handler->isPressedInUpdate("PLANT"))
        {
            handleProjecting();
        }

        m_state_processor(time_delta);
    }

    if (!m_is_alive && animation_graph.getActive()->isFinished())
    {
        if (m_is_projected)
        {
            m_is_alive = true;
            handleProjecting();
        }
        else
        {
            destroy();
            SceneManager::instance().reset();
        }
    }

    //auto light_processor = scene->findObjectByName("light_processor");
    //if (light_processor)
    //{
    //    light_processor->forceCast<LightProcessor>()->addSource(shared_from_this(), 6.f);
    //}
}

void Character::kill()
{
    if (m_is_alive)
    {
        animation_graph.makeTransition("KILL");
        engine::get_sound_engine().play("KILL", 100, false);
        m_is_alive = false;
    }
}

GameObject::Pointer Character::clone() const
{
    auto instance = std::make_shared<Character>(m_name);
    cloneComponents(instance);
    return instance;
}

void Character::move(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (m_is_switched_state || m_movement_handler->moveTowardsPosition(time_delta, m_target_position))
    {
        scene->inform("enemy", Message::MAP_CHANGED);
        auto const directionType = getPressedDirection();
        auto grid_map = engine::get_grid_map();
        auto const direction = utl::get_direction(directionType);
        auto const current_cell = grid_map->worldToCellPosition(position);
        auto const target_cell = current_cell + direction;

        bool const is_direction_set = directionType != utl::DirectionType::NONE;
        bool const is_in_map = grid_map->contains(target_cell);
        bool const is_wall = is_in_map && grid_map->cellContainsAnyOfTags(target_cell, collision_mask);

        if (!is_direction_set ||
            !is_in_map ||
            is_wall)
        {
            return switchState(StateType::IDLE);
        }

        m_target_position = grid_map->cellToWorldPosition(target_cell);
        if (grid_map->cellContainsTag(target_cell, "pushable"))
        {
            return switchState(StateType::PUSH);
        }

        resolve_direction(shared_from_this(), direction);
    }

    m_is_switched_state = false;
}

void Character::idle(float time_delta)
{
    animation_graph.makeTransition("IDLE");
    auto const directionType = getPressedDirection();
    if (directionType != utl::DirectionType::NONE)
    {
        auto grid_map = engine::get_grid_map();
        auto const direction = utl::get_direction(directionType);
        auto const current_cell = grid_map->worldToCellPosition(position);
        auto const target_cell = current_cell + direction;
        if (grid_map->contains(target_cell))
        {
            if (grid_map->cellContainsTag(target_cell, "pushable"))
            {
                return switchState(StateType::PUSH);
            }

            if (!grid_map->cellContainsAnyOfTags(target_cell, collision_mask))
            {
                return switchState(StateType::MOVE);
            }
        }
    }

    m_is_switched_state = false;
}

void Character::push(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (m_is_switched_state || m_movement_handler->moveTowardsPosition(time_delta, getAttribute("push_speed"), m_target_position))
    {
        scene->inform("enemy", Message::MAP_CHANGED);
        auto const directionType = getPressedDirection();
        auto grid_map = engine::get_grid_map();
        auto const direction = utl::get_direction(directionType);
        auto const current_cell = grid_map->worldToCellPosition(position);
        auto const target_cell = current_cell + direction;

        bool const is_direction_set = directionType != utl::DirectionType::NONE;
        bool const is_in_map = grid_map->contains(target_cell);
        bool const is_wall = is_in_map && grid_map->cellContainsAnyOfTags(target_cell, collision_mask);
        auto pushable = grid_map->getCellObjectByTag(target_cell, "pushable");
        bool const is_possible_to_push = pushable && pushable->forceCast<Pushable>()->isPossibleToPush(directionType);

        if (!is_possible_to_push ||
            !is_direction_set ||
            !is_in_map ||
            is_wall)
        {
            return switchState(StateType::IDLE);
        }

        m_target_position = grid_map->cellToWorldPosition(target_cell);
        if (!pushable)
        {
            return switchState(StateType::MOVE);
        }

        resolve_direction(shared_from_this(), direction, "PUSH_");
        pushable->forceCast<Pushable>()->move(getAttribute("push_speed"), directionType);
    }

    m_is_switched_state = false;
}

void Character::switchState(StateType state_type)
{
    m_is_switched_state = true;
    switch (state_type)
    {
    case Character::StateType::MOVE:
        m_state_processor = [&](float time_delta) { move(time_delta); };
        break;
    case Character::StateType::IDLE:
        m_state_processor = [&](float time_delta) { idle(time_delta); };
        break;
    case Character::StateType::PUSH:
        m_state_processor = [&](float time_delta) { push(time_delta); };
        break;
    }
}

utl::DirectionType Character::getPressedDirection() const
{
    static std::unordered_map<std::string, utl::DirectionType> const COMMANDS_TO_DIRECTIONS =
    {
        { "UP", utl::DirectionType::TOP },
        { "RIGHT", utl::DirectionType::RIGHT },
        { "DOWN", utl::DirectionType::BOTTOM },
        { "LEFT", utl::DirectionType::LEFT }
    };

    auto input_handler = engine::get_input_handler();
    for (auto const& p : COMMANDS_TO_DIRECTIONS)
    {
        if (input_handler->isPressed(p.first))
        {
            return COMMANDS_TO_DIRECTIONS.at(p.first);
        }
    }

    return utl::DirectionType::NONE;
}

void Character::handleProjecting()
{
    auto scene = engine::get_active_scene();
    auto time_scale = scene->getTimeScale();
    if (utl::almost_zero(time_scale)) return;

    auto grid_map = scene->getGridMap();
    auto const current_cell = grid_map->worldToCellPosition(position);
    if (!m_is_projected)
    {
        auto projection = m_projection_prototype->clone();
        projection->effects_storage.add("projection", EffectsFactory::create(EffectsFactory::Type::PROJECTION_EFFECT));
        scene->add(projection, "movable", current_cell);
        scene->inform("enemy", Message::MAP_CHANGED);
        engine::get_sound_engine().play("PROJECT", 75, false);
    }
    else
    {
        animation_graph.setActive("IDLE_DOWN");
        switchState(StateType::IDLE);
        auto projection = scene->findObjectByName("projection");
        position = projection->position;
        projection->destroy();
        engine::get_sound_engine().play("TELEPORT", 75, false);
    }

    m_is_projected = !m_is_projected;
}
