#include "i_enemy_state.hpp"

#include "i_enemy.hpp"
#include "random_engine.hpp"

std::array<sf::Vector2i, IEnemyState::DIRECTIONS_N> const IEnemyState::DIRECTIONS = {{
    { -1,  0 },
    {  0, -1 },
    {  1,  0 },
    {  0,  1 }}};

IEnemyState::IEnemyState(std::shared_ptr<IEnemy> enemy, Scene::Pointer scene, sf::Vector2i const& local_target_cell)
    : m_enemy(enemy)
    , m_scene(scene)
    , m_local_target_cell(local_target_cell)
    , m_movement_handler(scene, enemy)
{}

bool IEnemyState::isNearTheTarget(
    GridMap::Pointer grid_map,
    sf::Vector2i cell,
    GameObject::Pointer target,
    int threshold_distance_to_target)
{
    if (target)
    {
        auto const target_cell = grid_map->worldToCellPosition(target->position);
        return utl::norm_l1(cell, target_cell) < threshold_distance_to_target;
    }

    return false;
}

sf::Vector2i IEnemyState::chooseRandomDirection(std::shared_ptr<IEnemy> enemy, GridMap::Pointer grid_map)
{
    auto const cell = grid_map->worldToCellPosition(enemy->position);

    std::vector<size_t> direction_indices;
    for (size_t i = 0; i < DIRECTIONS_N; ++i)
    {
        auto const shifted_cell = cell + DIRECTIONS[i];
        if (grid_map->contains(shifted_cell) &&
            !grid_map->cellContainsAnyOfTags(shifted_cell, enemy->collision_mask))
        {
            direction_indices.push_back(i);
        }
    }

    if (!direction_indices.empty())
    {
        size_t idx = RandomEngine::randomRange(0, direction_indices.size() - 1);
        return DIRECTIONS[direction_indices[idx]];
    }

    return{};
}
