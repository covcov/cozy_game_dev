#include "animation_helpers.hpp"

bool resolve_direction(GameObject::Pointer target, sf::Vector2i const& direction, std::string const& prefix)
{
    if (direction.x || direction.y)
    {
        if (direction.x == 0)
        {
            return target->animation_graph.makeTransition(prefix + (direction.y > 0 ? "DOWN" : "UP"));
        }
        else if (direction.y == 0)
        {
            return target->animation_graph.makeTransition(prefix + (direction.x > 0 ? "RIGHT" : "LEFT"));
        }
    }
}
