#include "effects_factory.hpp"

#include "color_flash_effect.hpp"
#include "blur_effect.hpp"
#include "animated_effect.hpp"
#include "random_engine.hpp"

bool EffectsFactory::is_init = false;
std::unordered_map<EffectsFactory::Type, Effect::Pointer> EffectsFactory::effects = {};

Effect::Pointer EffectsFactory::create(EffectsFactory::Type type)
{
    if (!is_init)
    {
        init();
    }

    return effects[type]->clone();
}

void EffectsFactory::init()
{
    is_init = true;
    effects[Type::DAMAGE_EFFECT] = Effect::Pointer(new ColorFlashEffect(0.5f, sf::Color(255, 50, 50, 125)));
    effects[Type::HEALING_EFFECT] = Effect::Pointer(new ColorFlashEffect(0.5f, sf::Color(125, 255, 125, 190)));
    effects[Type::PROJECTION_EFFECT] = Effect::Pointer(new ColorFlashEffect(-1.f, sf::Color(255, 255, 255, 125)));
    effects[Type::BLUR_EFFECT] = Effect::Pointer(new BlurEffect(3.f));
}
