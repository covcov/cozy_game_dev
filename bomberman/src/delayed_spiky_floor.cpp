#include "delayed_spiky_floor.hpp"
#include "scene.hpp"
#include "creature.hpp"
#include "game_resources_storage.hpp"

DelayedSpikyFloor::DelayedSpikyFloor(std::string const& name, float reaction_time)
    : GameObject(name)
    , m_reaction_time(reaction_time)
    , m_elapsed_time(0.f)
    , m_is_activated(false)
    , m_spikes_out(false)
{
    collision_mask = { "player" };
}

void DelayedSpikyFloor::update(float time_delta)
{
    animation_graph.update(time_delta);
    auto scene = engine::get_active_scene();
    if (scene)
    {
        auto grid_map = scene->getGridMap();
        auto const cell = grid_map->worldToCellPosition(position);
        if (!m_is_activated)
        {
            auto const collided_objects = grid_map->getCollidedObjects(shared_from_this());
            for (auto const& obj : collided_objects)
            {
                if (collision_mask.find(obj->tag) != collision_mask.end())
                {
                    m_is_activated = true;
                    m_elapsed_time = 0.f;
                    break;
                }
            }
        }
        else
        {
            m_elapsed_time += time_delta;
            if (!m_spikes_out && m_elapsed_time > m_reaction_time)
            {
                animation_graph.makeTransition("ACTIVATE");
                engine::get_sound_engine().play("SPIKES", 100, false);
                m_spikes_out = true;
            }

            if (m_spikes_out)
            {
                if (animation_graph.getActive()->isFinished())
                {
                    m_spikes_out = false;
                    m_is_activated = false;
                    animation_graph.makeTransition("DEACTIVATE");
                }
                else
                {
                    auto const collided_objects = grid_map->getCollidedObjects(shared_from_this());
                    for (auto const& obj : collided_objects)
                    {
                        auto creature = std::dynamic_pointer_cast<Creature>(obj);
                        if (creature)
                        {
                            creature->kill();
                        }
                    }
                }
            }
        }
    }
}

GameObject::Pointer DelayedSpikyFloor::clone() const
{
    GameObject::Pointer instance = std::make_shared<DelayedSpikyFloor>(m_name, m_reaction_time);
    cloneComponents(instance);
    return instance;
}
