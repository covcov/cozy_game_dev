#include "follower.hpp"

#include "animation_graph_factory.hpp"

void Follower::update(float time_delta)
{
    auto animation = animation_graph.getActive();
    if (animation)
    {
        animation->update(time_delta);
    }

    if (m_target->is_active)
    {
        position = m_target->position + m_shift;
    }
    else
    {
        destroy();
    }
}

GameObject::Pointer Follower::clone() const
{
    auto instance = std::make_shared<Follower>(m_name);
    instance->setTarget(m_target);
    instance->applyShift(m_shift);
    cloneComponents(instance);
    return instance;
}

void Follower::setTarget(GameObject::Pointer target)
{
    m_target = target;
    if (m_target)
    {
        position = m_target->position;
    }
}

void Follower::applyShift(sf::Vector2f const& shift)
{
    m_shift += shift;
}
