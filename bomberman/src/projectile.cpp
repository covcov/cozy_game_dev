#include "projectile.hpp"

#include "scene.hpp"
#include "pushable.hpp"
#include "animation_helpers.hpp"

Projectile::Projectile(std::string const& name)
    : Creature(name)
    , m_direction(utl::DirectionType::BOTTOM)
{
    registerAttribute("damage", 25.f);
    registerAttribute("speed", 128.f);
}

void Projectile::awake()
{
    resolve_direction(shared_from_this(), utl::get_direction<int>(m_direction));
}

void Projectile::update(float time_delta)
{
    resolve_direction(shared_from_this(), utl::get_direction<int>(m_direction));
    animation_graph.update(time_delta);

    auto speed = utl::get_direction<float>(m_direction) * getAttribute("speed");
    position += speed * time_delta;

    auto grid_map = engine::get_grid_map();
    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());

    if (!collided_objects.empty())
    {
        for (auto obj : collided_objects)
        {
            if (obj->tag == "player")
            {
                obj->forceCast<Creature>()->applyDamage(getAttribute("damage"));
            }
            else if (obj->tag == "enemy")
            {
                obj->forceCast<Creature>()->kill();
            }
            else if (obj->tag == "pushable")
            {
                obj->forceCast<Pushable>()->move(getAttribute("speed"), m_direction);
            }
        }

        destroy();
    }
}

GameObject::Pointer Projectile::clone() const
{
    auto instance = std::make_shared<Projectile>(m_name);
    cloneComponents(instance);
    return instance;
}

void Projectile::setDirection(utl::DirectionType direction)
{
    switch (direction)
    {
    case utl::DirectionType::TOP:
        animation_graph.makeTransition("TOP");
        break;
    case utl::DirectionType::RIGHT:
        animation_graph.makeTransition("RIGHT");
        break;
    case utl::DirectionType::BOTTOM:
        animation_graph.makeTransition("BOTTOM");
        break;
    case utl::DirectionType::LEFT:
        animation_graph.makeTransition("LEFT");
        break;
    }

    m_direction = direction;
}

sf::Vector2i Projectile::getDirection() const
{
    return utl::get_direction<int>(m_direction);
}
