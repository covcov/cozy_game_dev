#include "sentry_patrolling_state.hpp"

#include "random_engine.hpp"
#include "i_enemy.hpp"
#include "animation_helpers.hpp"
#include "algorithm_extra.hpp"

void SentryPatrollingState::update(float time_delta)
{
    auto target = m_enemy->getTarget();
    auto grid_map = m_scene->getGridMap();
    auto cell = grid_map->worldToCellPosition(m_enemy->position);

    if (m_movement_handler.moveTowardsGridCell(time_delta, m_local_target_cell))
    {
        auto collided_objects = grid_map->getCollidedObjects(m_enemy);
        if (utl::contains_linear(collided_objects, [](GameObject::Pointer x) { return x->tag == "player"; }))
        {
            resolve_direction(m_enemy, grid_map->worldToCellPosition(target->position) - grid_map->worldToCellPosition(m_enemy->position));
            return m_enemy->setState(IEnemy::StateType::ATTACK);
        }

        auto path = m_scene->getPathSearchEngine()->findPathToObjectWithTag(
            cell,
            { "brick", "wall", "bomb", "pushable" },
            target->tag,
            m_threshold_distance_to_target);

        if (!path.empty())
        {
            return m_enemy->setState(IEnemy::StateType::CHASING);
        }
        else
        {
            m_local_target_cell = cell + chooseRandomDirection(m_enemy, grid_map);
            resolve_direction(m_enemy, m_local_target_cell - cell);
        }
    }
}