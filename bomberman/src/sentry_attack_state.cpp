#include "sentry_attack_state.hpp"

#include "i_enemy.hpp"
#include "algorithm_extra.hpp"
#include "animation_helpers.hpp"

SentryAttackState::SentryAttackState(std::shared_ptr<IEnemy> enemy, Scene::Pointer scene, sf::Vector2i const& local_target_cell)
    : IEnemyState(enemy, scene, local_target_cell)
    , m_elapsed_time(m_enemy->getAttribute("time_between_attacks") * 0.5f)
{}

void SentryAttackState::update(float time_delta)
{
    m_elapsed_time += time_delta;

    auto target = m_enemy->getTarget()->cast<Creature>();
    auto grid_map = m_scene->getGridMap();
    auto target_cell = grid_map->worldToCellPosition(target->position);
    auto enemy_cell = grid_map->worldToCellPosition(m_enemy->position);

    m_enemy->animation_graph.makeTransition("ATTACK");

    if (m_elapsed_time >= m_enemy->getAttribute("time_between_attacks"))
    {
        m_elapsed_time = 0.f;
        if (target)
        {
            auto collided_objects = grid_map->getCollidedObjects(m_enemy);
            if (utl::contains_linear(collided_objects, [&](GameObject::Pointer x) { return x->getId() == target->getId(); }))
            {
                target->applyDamage(m_enemy->getAttribute("damage_deal"));
            }
        }
    }

    if (m_enemy->animation_graph.getActive()->isFinished())
    {
        return m_enemy->setState(IEnemy::StateType::CHASING);
    }
}
