#include "input_handler_factory.hpp"
#include "keyboard_mouse_input_handler.hpp"

IInputHandler::Pointer InputHandlerFactory::create(Type type)
{
    switch (type)
    {
    case InputHandlerFactory::Type::KEYBOARD_MOUSE:
        return createKeyboardMouseInputHandler();
    case InputHandlerFactory::Type::JOYSTIC:
        return nullptr;
    }

    return nullptr;
}

IInputHandler::Pointer InputHandlerFactory::createKeyboardMouseInputHandler()
{
    auto input_handler = std::make_shared<KeyboardMouseInputHandler>();
    input_handler->bindKeyboardKey("DOWN", sf::Keyboard::Key::S);
    input_handler->bindKeyboardKey("UP", sf::Keyboard::Key::W);
    input_handler->bindKeyboardKey("RIGHT", sf::Keyboard::Key::D);
    input_handler->bindKeyboardKey("LEFT", sf::Keyboard::Key::A);

    input_handler->bindKeyboardKey("DOWN", sf::Keyboard::Key::Down);
    input_handler->bindKeyboardKey("UP", sf::Keyboard::Key::Up);
    input_handler->bindKeyboardKey("RIGHT", sf::Keyboard::Key::Right);
    input_handler->bindKeyboardKey("LEFT", sf::Keyboard::Key::Left);

    input_handler->bindKeyboardKey("PAUSE", sf::Keyboard::Key::P);
    input_handler->bindKeyboardKey("MUTE", sf::Keyboard::Key::M);
    input_handler->bindKeyboardKey("PLANT", sf::Keyboard::Key::Space);
    input_handler->bindKeyboardKey("USE", sf::Keyboard::Key::Enter);
    input_handler->bindKeyboardKey("RESET", sf::Keyboard::Key::R);
    input_handler->bindKeyboardKey("EXIT", sf::Keyboard::Key::Escape);

    input_handler->bindKeyboardKey("DEVELOPER_MODE", sf::Keyboard::Key::Tilde);
    return input_handler;
}