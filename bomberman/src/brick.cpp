#include "brick.hpp"
#include "scene.hpp"
#include "algorithm_extra.hpp"
#include "message.hpp"
#include "game_objects_pool.hpp"

Brick::Brick(std::string const& name)
    : GameObject(name)
    , m_is_burning(false)
{
    collision_mask.insert("explosion");
}

void Brick::update(float time_delta)
{
    animation_graph.update(time_delta);

    auto scene = engine::get_active_scene();
    if (scene)
    {
        auto grid_map = scene->getGridMap();
        auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
        if (!collided_objects.empty() && !m_is_burning)
        {
            m_is_burning = true;
            bool is_transition_made = animation_graph.makeTransition("KILL");
            if (!is_transition_made)
            {
                processDestruction();
                auto effect = GameObjectsPool::get("explosion_effect");
                if (effect)
                {
                    effect->position = 0.5f * (collider->getAABB().getTopLeft() + collider->getAABB().getBottomRight());
                    scene->add(effect, "foreground");
                }
            }
        }
    }

    if (m_is_burning && animation_graph.getActive()->isFinished())
    {
        processDestruction();
    }
}

GameObject::Pointer Brick::clone() const
{
    GameObject::Pointer instance = std::make_shared<Brick>(m_name);
    cloneComponents(instance);
    return instance;
}

void Brick::processDestruction()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    destroy();
    scene->inform("enemy", Message::MAP_CHANGED);
}
