#include "bomb.hpp"
#include "scene.hpp"
#include "game_resources_storage.hpp"
#include "camera_shaker.hpp"
#include "random_engine.hpp"
#include "message.hpp"

void Bomb::awake()
{
    auto scene = engine::get_active_scene();
    {
        if (scene)
        {
            scene->inform("enemy", Message::MAP_CHANGED);
            scene->inform("player", Message::BOMB_PLANTED);
        }
    }
}

void Bomb::update(float time_delta)
{
    animation_graph.update(time_delta);
    auto scene = engine::get_active_scene();
    if (scene)
    {
        auto grid_map = scene->getGridMap();
        auto const& cell = grid_map->worldToCellPosition(position);
        m_elapsed_time += time_delta;

        if (m_elapsed_time > m_time_to_explode || grid_map->cellContainsTag(cell, "explosion"))
        {
            destroy();
            engine::get_sound_engine().play("EXPLOSION", 50, false, 2.);
            auto camera_shaker = CameraShaker::Pointer(new CameraShaker("camera_shaker", 1.f, { 2.f, 2.f }));
            scene->add(camera_shaker, "explosion");

            auto explosions = m_explosion_manager->create(grid_map, cell, m_fire_power);
            for (auto explosion : explosions)
            {
                scene->add(explosion, "explosion");
            }
        }
    }
}

GameObject::Pointer Bomb::clone() const
{
    GameObject::Pointer instance = std::make_shared<Bomb>(
        m_name,
        m_time_to_explode,
        m_fire_power,
        m_explosion_manager);

    cloneComponents(instance);
    return instance;
}

void Bomb::onDestroy()
{
    auto scene = engine::get_active_scene();
    if (scene)
    {
        scene->inform("enemy", Message::MAP_CHANGED);
        scene->inform("player", Message::BOMB_EXPLODED);
    }
}
