#include "explosion_manager.hpp"
#include "algorithm_extra.hpp"
#include "debug.hpp"

std::array<sf::Vector2i, ExplosionManager::DIRECTIONS_N> const ExplosionManager::DIRECTIONS = {
    sf::Vector2i{ -1,  0 },
    sf::Vector2i{  0, -1 },
    sf::Vector2i{  1,  0 },
    sf::Vector2i{  0,  1 } };

std::array<ExplosionManager::ExplosionType, ExplosionManager::DIRECTIONS_N> const ExplosionManager::BODY_TYPES = {
    ExplosionManager::ExplosionType::LEFT_BODY,
    ExplosionManager::ExplosionType::TOP_BODY,
    ExplosionManager::ExplosionType::RIGHT_BODY,
    ExplosionManager::ExplosionType::BOTTOM_BODY };

std::array<ExplosionManager::ExplosionType, ExplosionManager::DIRECTIONS_N> const ExplosionManager::TAIL_TYPES = {
    ExplosionManager::ExplosionType::LEFT_TAIL,
    ExplosionManager::ExplosionType::TOP_TAIL,
    ExplosionManager::ExplosionType::RIGHT_TAIL,
    ExplosionManager::ExplosionType::BOTTOM_TAIL };

std::vector<GameObject::Pointer> ExplosionManager::create(GridMap::Pointer grid_map, sf::Vector2i cell, int fire_power) const
{
    std::vector<GameObject::Pointer> explosions;
    auto explosion_center = getFromPrototype(ExplosionType::CENTER);
    explosion_center->position = grid_map->cellToWorldPosition(cell);
    explosions.push_back(explosion_center);

    for (size_t direction_idx = 0; direction_idx < DIRECTIONS_N; ++direction_idx)
    {
        for (int power = 1; power <= fire_power; ++power)
        {
            auto const current_cell = cell + power * DIRECTIONS[direction_idx];
            if (grid_map->contains(current_cell))
            {
                auto const& cell_objects = grid_map->getCellObjects(current_cell);

                bool is_brick = utl::contains_linear(
                    cell_objects,
                    [](GameObject::Pointer obj) { return obj->tag == "brick" || obj->tag == "wall"; });
                bool is_last = power == fire_power;

                if (is_brick || is_last)
                {
                    GameObject::Pointer explosion = getFromPrototype(TAIL_TYPES[direction_idx]);
                    explosion->position = grid_map->cellToWorldPosition(current_cell);
                    explosions.push_back(explosion);
                    break;
                }
                else
                {
                    GameObject::Pointer explosion = getFromPrototype(BODY_TYPES[direction_idx]);
                    explosion->position = grid_map->cellToWorldPosition(current_cell);
                    explosions.push_back(explosion);
                }
            }
        }
    }

    return explosions;
}

void ExplosionManager::setPrototype(ExplosionType type, GameObject::Pointer game_object)
{
    m_prototypes[type] = game_object;
}

GameObject::Pointer ExplosionManager::getFromPrototype(ExplosionType type) const
{
    auto it = m_prototypes.find(type);
    if (it != m_prototypes.end())
    {
        return it->second->clone();
    }

    return nullptr;
}
