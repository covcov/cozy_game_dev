#include "button.hpp"

#include "scene.hpp"

Button::Button(std::string const& name)
    : GameObject(name)
    , m_is_activated(false)
{
    collision_mask.insert("player");
    collision_mask.insert("enemy");
    collision_mask.insert("pushable");
    collision_mask.insert("bomb");
    collision_mask.insert("projection");
}

void Button::awake()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    scene->blackboard.set<bool>(m_trigger_message, true);
}

void Button::update(float time_delta)
{
    animation_graph.update(time_delta);

    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto grid_map = scene->getGridMap();
    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    if (m_is_activated && collided_objects.empty())
    {
        animation_graph.makeTransition("DEACTIVATE");
        scene->blackboard.set<bool>(m_trigger_message, true);
        m_is_activated = false;
    }
    else if (!m_is_activated && !collided_objects.empty())
    {
        animation_graph.makeTransition("ACTIVATE");
        scene->blackboard.set<bool>(m_trigger_message, false);
        m_is_activated = true;
    }
}

GameObject::Pointer Button::clone() const
{
    auto instance = std::make_shared<Button>(m_name);
    instance->m_trigger_message = m_trigger_message;
    cloneComponents(instance);
    return instance;
}
