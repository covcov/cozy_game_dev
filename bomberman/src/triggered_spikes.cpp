#include "triggered_spikes.hpp"

#include "scene.hpp"
#include "creature.hpp"
#include "math.hpp"
#include "game_resources_storage.hpp"

TriggeredSpikes::TriggeredSpikes(std::string const& name)
    : GameObject(name)
    , m_is_activated(true)
    , m_elapsed_time(0.f)
    , m_character(nullptr)
{
    collision_mask = { "player", "enemy" };
}

void TriggeredSpikes::awake()
{
    auto scene = engine::get_active_scene();
    m_character = scene->findObjectByName("character");
}

void TriggeredSpikes::update(float time_delta)
{
    animation_graph.update(time_delta);
    auto grid_map = engine::get_grid_map();

    m_elapsed_time += time_delta;
    if (m_elapsed_time >= TIME_PERIOD && m_is_activated)
    {
        m_elapsed_time = 0.f;
        auto const cell = grid_map->worldToCellPosition(position);
        auto const character_cell = grid_map->worldToCellPosition(m_character->position);
        auto distance = static_cast<float>(utl::norm_l1(cell, character_cell));
        distance = std::min(distance, MAX_DISTANCE);
        auto const k = (MAX_DISTANCE - distance) / MAX_DISTANCE;
        if (!utl::almost_zero(k))
        {
            engine::get_sound_engine().play("SPIKES", MAX_VOLUME * k, false);
        }
    }

    auto const cell = grid_map->worldToCellPosition(position);
    if (m_is_activated)
    {
        auto const collided_objects = grid_map->getCollidedObjects(shared_from_this());
        for (auto const& obj : collided_objects)
        {
            auto creature = obj->cast<Creature>();
            if (creature)
            {
                creature->kill();
            }
        }
    }

    auto scene = engine::get_active_scene();
    if (scene->blackboard.get<bool>(m_trigger_message).isValid())
    {
        auto const is_bottom_pressed = scene->blackboard.get<bool>(m_trigger_message).getValue();
        if (m_is_activated && !is_bottom_pressed)
        {
            animation_graph.makeTransition("DEACTIVATE");
            m_is_activated = false;
        }
        else if (!m_is_activated && is_bottom_pressed)
        {
            animation_graph.makeTransition("ACTIVATE");
            m_is_activated = true;
        }
    }
}

GameObject::Pointer TriggeredSpikes::clone() const
{
    auto instance = std::make_shared<TriggeredSpikes>(m_name);
    instance->m_trigger_message = m_trigger_message;
    cloneComponents(instance);
    return instance;
}
