#include "chest.hpp"

#include "scene.hpp"
#include "scene_manager.hpp"

Chest::Chest(std::string const& name)
    : GameObject(name)
    , m_elapsed_time(0.f)
    , m_is_opening(false)
{
    collision_mask = { "player" };
}

void Chest::update(float time_delta)
{
    animation_graph.update(time_delta);

    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto grid_map = scene->getGridMap();
    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());

    if (scene->blackboard.get<bool>("key_collected").isValid())
    {
        auto key_collected = scene->blackboard.get<bool>("key_collected").getValue();
        if (!m_is_opening && !collided_objects.empty() && key_collected)
        {
            animation_graph.makeTransition("OPEN");
            engine::get_sound_engine().play("FANFARE", 100, false);
            m_is_opening = true;
        }
    }

    if (m_is_opening)
    {
        m_elapsed_time += time_delta;
    }

    if (m_elapsed_time >= WAIT_TIME)
    {
        SceneManager::instance().playNext();
    }
}

GameObject::Pointer Chest::clone() const
{
    auto instance = std::make_shared<Chest>(m_name);
    cloneComponents(instance);
    return instance;
}
