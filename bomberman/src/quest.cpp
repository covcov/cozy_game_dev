#include "quest.hpp"

#include "scene.hpp"
#include "game_resources_storage.hpp"
#include "text_objects_factory.hpp"
#include "experience_object.hpp"

Quest::Quest(std::string const& name)
    : GameObject(name)
{
    collision_mask.insert("player");
    m_state_processor = [&]() { meetPlayer(); };
    m_last_state = [&]() { greet(); };
}

void Quest::awake()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    m_target = scene->findObjectByName("character");
}

void Quest::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    animation_graph.update(time_delta);
    auto camera = engine::get_camera();

    if (m_state_processor)
    {
        m_state_processor();
    }
}

GameObject::Pointer Quest::clone() const
{
    auto instance = std::make_shared<Quest>(m_name);
    cloneComponents(instance);
    return instance;
}

void Quest::meetPlayer()
{
    handleTextVisualization("[ENTER] to speak");

    if (hasProceeded())
    {
        cleanUp();
        m_state_processor = m_last_state;
    }
}

void Quest::greet()
{
    handleTextVisualization(
        "Hello, Stranger! Are you trying to get\n"
        "into the temple?\n"
        "[ENTER]");

    if (hasProceeded())
    {
        cleanUp();
        m_state_processor = [&]() { suggestQuest(); };
    }
}

void Quest::suggestQuest()
{
    m_last_state = [&]() { suggestQuest(); };
    handleTextVisualization(
        "Well, lets say I know how to help!\n\n"
        "Have you seen a spider near the graveyard?\n"
        "There are two more in the northern woods.\n"
        "Kill them and get back to me!\n"
        "\nKill 3 spiders and return to the ghost\n"
        "[ENTER]");

    if (hasProceeded())
    {
        cleanUp();
        m_state_processor = [&]() { controlQuest(); };
    }
}

void Quest::controlQuest()
{
    m_last_state = [&]() { controlQuest(); };

    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto killed_spiders_n = scene->blackboard.get<int>("spider").getValue();
    if (killed_spiders_n == SPIDERS_TO_KILL)
    {
        cleanUp();
        m_state_processor = [&]() { finishQuest(); };
    }
}

void Quest::finishQuest()
{
    m_last_state = [&]() { finishQuest(); };
    handleTextVisualization(
        "Great job!\n"
        "Here's your key!\n\n"
        "[ENTER]");

    if (hasProceeded())
    {
        auto experience = GameObject::Pointer(new ExperienceObject("xp", position, 500, 2.f));
        engine::get_active_scene()->add(experience, "foreground");

        cleanUp();
        m_state_processor = nullptr;
    }
}

bool Quest::collideWithPlayer()
{
    auto grid_map = engine::get_grid_map();
    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    return !collided_objects.empty();
}

void Quest::handleTextVisualization(std::string const& text)
{
    auto scene = engine::get_active_scene();
    if (collideWithPlayer())
    {
        if (!m_connected_to_player)
        {
            m_connected_to_player = true;
            m_current_text_object = TextObjectsFactory::getQuestText(text);
            m_current_text_object->bindToTarget(shared_from_this());
            scene->add(m_current_text_object, "ui");
        }
    }
    else if (m_current_text_object)
    {
        cleanUp();
        m_state_processor = [&]() { meetPlayer(); };
    }
}

bool Quest::hasProceeded()
{
    return engine::get_input_handler()->isPressedInUpdate("USE") && m_connected_to_player;
}

void Quest::cleanUp()
{
    m_current_text_object->destroy();
    m_connected_to_player = false;
}
