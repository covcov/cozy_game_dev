#include "shooting_trap.hpp"

#include "scene.hpp"
#include "projectile.hpp"
#include "message.hpp"

ShootingTrap::ShootingTrap(std::string const& name, GameObject::Pointer projectile_prototype)
    : Creature(name)
    , m_elapsed_time(0.f)
    , m_is_shooting(false)
    , m_projectile_prototype(projectile_prototype)
{
    registerAttribute("shift", 2.f);
    registerAttribute("duration", 3.5f);
}

void ShootingTrap::awake()
{
    auto scene = engine::get_active_scene();
    {
        if (scene)
        {
            scene->inform("enemy", Message::MAP_CHANGED);
            scene->inform("player", Message::BOMB_PLANTED);
        }
    }
}

void ShootingTrap::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    animation_graph.update(time_delta);

    m_elapsed_time += time_delta;
    if (!m_is_shooting && m_elapsed_time >= getAttribute("duration"))
    {
        animation_graph.makeTransition("SHOOT");
        m_is_shooting = true;

        for (size_t i = 0; i < utl::DIRECTINS_N; ++i)
        {
            auto direction_type = static_cast<utl::DirectionType>(i);
            auto projectile = m_projectile_prototype->clone();
            projectile->forceCast<Projectile>()->setDirection(direction_type);
            projectile->position = position + getAttribute("shift") * utl::get_direction<float>(direction_type);
            scene->add(projectile, "foreground");
        }
    }

    if (m_is_shooting && animation_graph.getActive()->isFinished())
    {
        scene->inform("enemy", Message::MAP_CHANGED);
        scene->inform("player", Message::BOMB_EXPLODED);
        destroy();
    }
}

GameObject::Pointer ShootingTrap::clone() const
{
    auto instance = std::make_shared<ShootingTrap>(m_name, m_projectile_prototype);
    cloneComponents(instance);
    return instance;
}
