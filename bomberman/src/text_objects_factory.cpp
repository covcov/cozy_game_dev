#include "text_objects_factory.hpp"

#include <string>

#include "game_resources_storage.hpp"

std::shared_ptr<TextObject> TextObjectsFactory::getPauseText()
{
    TextObject::Params params;
    params.font_type = "GRAVITY_BOLD";
    params.text_color = sf::Color::Red;
    params.text_color.a = 200;
    params.outline_color = params.text_color;
    params.outline_thickness = 0;
    params.scale = 12;

    auto text_object = std::make_shared<TextObject>("pause_text", "PAUSE", params);
    auto& render_window = engine::get_render_window();
    text_object->position = render_window.getDefaultView().getCenter();
    return text_object;
}

std::shared_ptr<TextObject> TextObjectsFactory::getQuestText(std::string const& text)
{
    TextObject::Params params;
    params.background_color = sf::Color::Black;
    params.background_color.a = 125;
    params.text_color = sf::Color::Yellow;
    params.font_type = "GRAVITY_REGULAR";
    params.scale = 2;
    return std::make_shared<TextObject>("phrase", text, params);
}
