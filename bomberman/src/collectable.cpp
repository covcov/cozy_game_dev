#include "collectable.hpp"

#include "scene.hpp"

Collectable::Collectable(std::string const& name)
    : GameObject(name)
{
    collision_mask.insert("player");
}

void Collectable::awake()
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    scene->blackboard.set<bool>(m_message, false);
}

void Collectable::update(float time_delta)
{
    animation_graph.update(time_delta);

    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto grid_map = scene->getGridMap();
    auto collided_objects = grid_map->getCollidedObjects(shared_from_this());
    if (!collided_objects.empty())
    {
        engine::get_sound_engine().play("BONUS", 100, false);
        destroy();
        scene->blackboard.set<bool>(m_message, true);
    }
}

GameObject::Pointer Collectable::clone() const
{
    auto instance = std::make_shared<Collectable>(m_name);
    instance->m_message = m_message;
    cloneComponents(instance);
    return instance;
}
