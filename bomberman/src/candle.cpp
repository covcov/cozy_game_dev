#include "candle.hpp"

#include "game_objects_pool.hpp"
#include "pushable.hpp"
#include "light_processor.hpp"

sf::Vector2i const Candle::TEXTURE_SIZE = { 4, 32 };

void Candle::awake()
{
    auto scene = engine::get_active_scene();

    m_pushable = GameObjectsPool::getRandomAs<Pushable>("box");
    m_pushable->position = position;
    scene->add(m_pushable, "movable");

    m_fire = GameObjectsPool::getAs<Follower>("fire");
    m_fire->setTarget(shared_from_this());
    m_fire->applyShift(sf::Vector2f(0.f, -2.f));
    scene->add(m_fire, "foreground");

    m_candle_size = { PIXEL_WIDTH, PIXEL_LENGTH };
    m_candle_position = calculatePixelPosition(m_candle_size);

    m_light_processor = scene->findObjectByName("light_processor");
}

void Candle::update(float time_delta)
{
    position = m_pushable->position;

    m_elaplsed_time += time_delta;
    if (m_elaplsed_time >= m_burning_time)
    {
        destroy();
    }
    else
    {
        auto render_texture = engine::get_render_texture_storage().get(TEXTURE_SIZE);
        render_texture->clear(sf::Color::Transparent);
        sf::RectangleShape rectangle_shape;
        rectangle_shape.setSize(m_candle_size);
        rectangle_shape.setPosition(m_candle_position);
        rectangle_shape.setFillColor(sf::Color::White);
        render_texture->draw(rectangle_shape);
        render_texture->display();
        m_sprite = sf::Sprite(render_texture->getTexture());

        if (m_elaplsed_time >= m_multiplier * m_burning_time / PIXEL_LENGTH)
        {
            m_multiplier += 1.f;
            m_candle_size.y -= 1.f;
            m_candle_position = calculatePixelPosition(m_candle_size);
            m_fire->applyShift({ 0.f, 1.f });
        }
    }

    m_light_processor->forceCast<LightProcessor>()->addSource(shared_from_this(), LIGHT_RADIUS);
}

GameObject::Pointer Candle::clone() const
{
    auto instance = std::make_shared<Candle>(m_name);
    instance->setBurningTime(m_burning_time);
    cloneComponents(instance);
    return instance;
}

void Candle::setBurningTime(float burning_time)
{
    m_burning_time = burning_time;
}

sf::Vector2f Candle::calculatePixelPosition(sf::Vector2f const& candle_size)
{
    float const x = 0;
    float const y = TEXTURE_SIZE.y / 2 - 3 - candle_size.y;
    return { x, y };
}

void Candle::draw(sf::RenderTarget& render_target)
{
    drawSprite(render_target, m_sprite);
}
