#include "waypoint_enemy.hpp"

#include "scene.hpp"
#include "message.hpp"
#include "animation_helpers.hpp"
#include "game_objects_pool.hpp"

WaypointEnemy::WaypointEnemy(std::string const& name)
    : Creature(name)
    , m_cell_index()
    , m_is_path_outdated(false)
    , m_is_dead(false)
{
    collision_mask = { "brick", "wall", "bomb", "pushable", "blocked_pushable" };
}

void WaypointEnemy::awake()
{
    auto scene = engine::get_active_scene();
    auto grid_map = scene->getGridMap();
    m_movement_handler = MovementHandler::Pointer(new MovementHandler(scene, forceCast<Creature>()));
    m_target_cell = grid_map->worldToCellPosition(position);
    updateTargetWaypoint();
}

void WaypointEnemy::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    animation_graph.update(time_delta);
    moveToWaypoint(time_delta);

    if (m_is_dead)
    {
        destroy();
        scene->inform("player", Message::ENEMY_DIED);
        auto effect = GameObjectsPool::get("explosion_effect");
        effect->position = position;
        scene->add(effect, "foreground");
    }
}

GameObject::Pointer WaypointEnemy::clone() const
{
    auto instance = std::make_shared<WaypointEnemy>(m_name);
    instance->setWaypoints(m_waypoints);
    for (auto attribute : m_attributes)
    {
        instance->registerAttribute(attribute.first, attribute.second);
    }

    cloneComponents(instance);
    return instance;
}

void WaypointEnemy::getInformed(int message)
{
    if (message == Message::MAP_CHANGED)
    {
        m_is_path_outdated = true;
    }
}

void WaypointEnemy::kill()
{
    if (!m_is_dead)
    {
        m_is_dead = true;
        animation_graph.makeTransition("KILL");
    }
}

void WaypointEnemy::updateTargetWaypoint()
{
    if (!m_waypoints.empty())
    {
        m_cell_index = (m_cell_index + 1) % m_waypoints.size();
        m_target_waypoint = m_waypoints[m_cell_index];
        m_is_path_outdated = true;
    }
}

void WaypointEnemy::updatePathIfNecessary()
{
    if (m_is_path_outdated)
    {
        m_is_path_outdated = false;

        auto scene = engine::get_active_scene();
        if (!scene) return;
        auto grid_map = scene->getGridMap();

        auto current_cell = grid_map->worldToCellPosition(position);
        m_path = scene->getPathSearchEngine()->findPath(current_cell, m_target_waypoint, collision_mask);
    }
}

void WaypointEnemy::moveToWaypoint(float time_delta)
{
    if (m_is_dead)
    {
        return;
    }

    if (m_movement_handler->moveTowardsGridCell(time_delta, m_target_cell))
    {
        if (m_target_cell == m_target_waypoint)
        {
            m_is_path_outdated = true;
            updateTargetWaypoint();
        }

        updatePathIfNecessary();

        if (!m_path.empty())
        {
            auto scene = engine::get_active_scene();
            if (!scene) return;

            auto grid_map = scene->getGridMap();
            m_target_cell = m_path.back();
            m_path.pop_back();
            resolve_direction(forceCast<Creature>(), m_target_cell - grid_map->worldToCellPosition(position));
        }
    }
}
