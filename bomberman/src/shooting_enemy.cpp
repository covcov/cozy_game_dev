#include "shooting_enemy.hpp"

#include "scene.hpp"
#include "projectile.hpp"

ShootingEnemy::ShootingEnemy(std::string const& name, GameObject::Pointer projectile_prototype)
    : Creature(name)
    , m_elapsed_time(0)
    , m_is_shooting(false)
    , m_direction_type(utl::DirectionType::TOP)
    , m_projectile_prototype(projectile_prototype)
{
    registerAttribute("speed", 16);
    registerAttribute("reload", 2);
    registerAttribute("shift", 16);
}

void ShootingEnemy::awake()
{
}

void ShootingEnemy::update(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    auto animation = animation_graph.getActive();
    animation->update(time_delta);
    shoot(time_delta);
}

GameObject::Pointer ShootingEnemy::clone() const
{
    auto instance = std::make_shared<ShootingEnemy>(m_name, m_projectile_prototype);
    cloneComponents(instance);
    return instance;
}

void ShootingEnemy::shoot(float time_delta)
{
    auto scene = engine::get_active_scene();
    if (!scene) return;

    m_elapsed_time += time_delta;
    if (m_elapsed_time >= getAttribute("reload"))
    {
        m_elapsed_time = 0.f;
        m_is_shooting = true;

        switch (m_direction_type)
        {
        case utl::DirectionType::TOP:
            animation_graph.makeTransition("ATTACK_UP");
            break;
        case utl::DirectionType::RIGHT:
            animation_graph.makeTransition("ATTACK_RIGHT");
            break;
        case utl::DirectionType::BOTTOM:
            animation_graph.makeTransition("ATTACK_DOWN");
            break;
        case utl::DirectionType::LEFT:
            animation_graph.makeTransition("ATTACK_LEFT");
            break;
        }

        auto projectile = m_projectile_prototype->clone();
        projectile->forceCast<Projectile>()->setDirection(m_direction_type);
        projectile->position = position + getAttribute("shift") * utl::get_direction<float>(m_direction_type);
        scene->add(projectile, "foreground");
    }

    if (m_is_shooting && animation_graph.getActive()->isFinished())
    {
        m_is_shooting = false;
        animation_graph.makeTransition("STOP");
    }
}
