#include "pushable.hpp"

#include "engine.hpp"

void Pushable::awake()
{
    collision_mask = { "pushable", "wall", "brick", "projection" };
    m_movement_handler.reset(new MovementHandler(engine::get_active_scene(), forceCast<Creature>()));
}

void Pushable::update(float time_delta)
{
    animation_graph.update(time_delta);
    if (m_is_moving && m_movement_handler->moveTowardsGridCell(time_delta, m_speed, m_target_cell))
    {
        m_is_moving = false;
    }
}

GameObject::Pointer Pushable::clone() const
{
    auto instance = std::make_shared<Pushable>(m_name);
    cloneComponents(instance);
    return instance;
}

bool Pushable::isPossibleToPush(utl::DirectionType directionType)
{
    auto grid_map = engine::get_grid_map();
    auto const current_cell = grid_map->worldToCellPosition(position);
    auto const target_cell = current_cell + utl::get_direction(directionType);
    return grid_map->contains(target_cell) && !grid_map->cellContainsAnyOfTags(target_cell, collision_mask);
}

void Pushable::move(float speed, utl::DirectionType directionType)
{
    if (isPossibleToPush(directionType))
    {
        m_is_moving = true;
        m_speed = speed;
        auto grid_map = engine::get_grid_map();
        auto const current_cell = grid_map->worldToCellPosition(position);
        m_target_cell = current_cell + utl::get_direction(directionType);
    }
}
