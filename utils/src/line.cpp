#include "line.hpp"

namespace utl
{

Intersection find_intersection(Line const& l_1, Line const& l_2)
{
    float const w = l_1.a * l_2.b - l_2.a * l_1.b;
    if (utl::almost_zero(w))
    {
        return { false,{} };
    }

    float const x = l_1.b * l_2.c - l_2.b * l_1.c;
    float const y = l_1.c * l_2.a - l_2.c * l_1.a;
    return { true,{ x / w, y / w } };
}

}
