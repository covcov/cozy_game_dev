#include "blackboard.hpp"

namespace utl
{

void utl::Blackboard::erase(std::string const& key)
{
    m_anymap.erase(key);
}

void utl::Blackboard::clear()
{
    m_anymap.clear();
}

}