#include <algorithm>
#include <functional>
#include <unordered_map>

namespace utl
{

template <typename ...Args>
class Signal
{
public:
    struct Connection
    {
        Connection(Signal& signal, size_t id)
            : signal(signal)
            , id(id)
        {}

        void disconnect() { signal.slots.erase(id); }
        ~Connection() { disconnect(); }
    private:
        Signal& signal;
        size_t id;
    };

    Signal() : id(0) {}

    std::shared_ptr<Connection> connect(std::function<void(Args...)> const& slot)
    {
        slots.insert({ id++, slot });
        return std::shared_ptr<Connection>(new Connection(*this, id - 1));
    }

    template <typename T>
    std::shared_ptr<Connection> connectMember(T * inst, void(T:: * slot)(Args...) const)
    {
        return connect([=](Args... args) { (inst->*slot)(args...); });
    }

    template <typename T>
    std::shared_ptr<Connection> connectMember(T * inst, void(T:: * slot)(Args...))
    {
        return connect([=](Args... args) { (inst->*slot)(args...); });
    }

    void emit(Args... args)
    {
        for (auto const& it : slots)
        {
            it.second.operator()(args...);
        }
    }

private:
    std::unordered_map<size_t, std::function<void(Args...)>> slots;
    size_t id;
};

}