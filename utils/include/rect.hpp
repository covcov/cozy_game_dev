#pragma once

#include "math.hpp"

namespace utl
{

template <typename T>
struct Rect
{
    T left;
    T top;
    T width;
    T height;

    bool contains(sf::Vector2<T> const& pt) const;
    bool intersects(Rect<T> const& other) const;
    sf::Vector2<T> getTopLeft() const { return { left, top }; }
    sf::Vector2<T> getBottomRight() const { return { left + width, top + height }; }
    sf::Vector2<T> getTopRight() const { return { left + width, top }; }
    sf::Vector2<T> getBottomLeft() const { return { left, top + height }; }
    Rect<T> shift(sf::Vector2<T> const& v) const;
};

template <typename T>
bool Rect<T>::contains(sf::Vector2<T> const& pt) const
{
    return pt.x >= left && pt.x <= left + width && pt.y >= top && pt.y <= top + height;
}

template <typename T>
bool Rect<T>::intersects(Rect<T> const& other) const
{
    float const l1 = left;
    float const r1 = left + width;
    float const l2 = other.left;
    float const r2 = other.left + other.width;

    if (l1 <= r2 && l2 <= r1)
    {
        float const t1 = top;
        float const b1 = top + height;
        float const t2 = other.top;
        float const b2 = other.top + other.height;

        return t1 <= b2 && t2 <= b1;
    }

    return false;
}

template <typename T>
Rect<T> Rect<T>::shift(sf::Vector2<T> const& v) const
{
    Rect<T> shifted = *this;
    shifted.left += v.x;
    shifted.top += v.y;
    return shifted;
}

}