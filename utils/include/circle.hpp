#pragma once

#include "math.hpp"

namespace utl
{

template <typename T>
struct Circle
{
    Circle() = default;

    Circle(sf::Vector2<T> const& position, T radius)
        : position(position)
        , radius(radius)
    {}


    sf::Vector2<T> position;
    T radius;

    bool contains(sf::Vector2<T> const& pt) const;
    bool intersects(Circle<T> const& other) const;
};

template <typename T>
inline bool Circle<T>::contains(sf::Vector2<T> const& pt) const
{
    return distance(pt, position) < radius;
}

template<typename T>
inline bool Circle<T>::intersects(Circle<T> const& other) const
{
    T const d = distance(other.position, position);
    return d < radius + other.radius;
}

}