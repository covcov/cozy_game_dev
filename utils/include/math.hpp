#pragma once

#include <cmath>

#include <SFML/System.hpp>

namespace utl
{

float constexpr PI = 3.141592f;
float constexpr DISTANCE_EPS = 0.1f;

template <typename T>
bool almost_zero(T value, T eps = 10 * std::numeric_limits<T>::epsilon())
{
    return std::fabs(value) < eps;
}

template <typename T>
T fmod(T numerator, T denominator)
{
    int const d = numerator / denominator;
    return numerator - static_cast<T>(d) * denominator;
}

template <typename T>
T sqr(T val)
{
    return val * val;
}

template <typename T>
sf::Vector2<T> round(sf::Vector2<T> const& pt)
{
    int x = std::round(pt.x);
    int y = std::round(pt.y);
    return { T(x), T(y) };
}

template <typename T>
bool in_range_inclusive(T value, T low, T high)
{
    return value >= low && value <= high;
}

template <typename T>
T distance(sf::Vector2<T> const& pt1, sf::Vector2<T> const& pt2)
{
    return std::sqrt(sqr(pt1.x - pt2.x) + sqr(pt1.y - pt2.y));
}

template <typename T>
T norm_l1(sf::Vector2<T> const& pt1, sf::Vector2<T> const& pt2)
{
    return static_cast<T>(std::fabs(pt1.x - pt2.x) + std::fabs(pt1.y - pt2.y));
}

template <typename T>
T norm(sf::Vector2<T> const& pt)
{
    return std::sqrt(sqr(pt.x) + sqr(pt.y));
}

template <typename T>
sf::Vector2<T> direction(sf::Vector2<T> const& pt)
{
    return pt == sf::Vector2<T>() ? sf::Vector2<T>() : pt / norm(pt);
}

inline sf::Vector2f rotate(sf::Vector2f const& from, sf::Vector2f const& to, float angle_rad)
{
    auto const p = to - from;
    auto const cosa = std::cos(angle_rad);
    auto const sina = std::sin(angle_rad);
    auto const x = cosa * p.x - sina * p.y;
    auto const y = sina * p.x + cosa * p.y;
    return from + sf::Vector2f(x, y);
}

inline float deg_to_rad(float angle)
{
    return angle / 180.f * PI;
}

inline float rad_to_deg(float angle)
{
    return angle * 180.f / PI;
}

inline float pseudo_angle(float dy, float dx)
{
    auto const angle = std::atan2(dy, dx);
    return angle > 0.f ? angle : (2.f * PI + angle);
}

inline float pseudo_angle(sf::Vector2f const& v)
{
    return pseudo_angle(v.y, v.x);
}

inline bool belongs_to_segment(
    sf::Vector2f const& p,
    sf::Vector2f const& p_1,
    sf::Vector2f const& p_2,
    float eps = 10 * std::numeric_limits<float>::epsilon())
{
    auto const d_1 = distance(p, p_1);
    auto const d_2 = distance(p, p_2);
    auto const d = distance(p_1, p_2);
    return d_1 + d_2 - d < eps;
}

} // namespace utl
