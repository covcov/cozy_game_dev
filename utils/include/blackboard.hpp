#pragma once

#include <unordered_map>
#include <string>
#include <memory>

namespace utl
{

class Blackboard
{
public:
    template <typename T>
    struct Output
    {
        Output(bool is_valid, T const& value) : m_is_valid(is_valid), m_value(value) {}

        bool isValid() const { return m_is_valid; }
        T const& getValue() const { return m_value; }

    private:
        bool m_is_valid;
        T m_value;
    };

    template <typename T>
    void set(std::string const& key, T const& value);

    template <typename T>
    Blackboard::Output<T> get(std::string const& key);

    void erase(std::string const& key);
    void clear();
private:
    struct AnyBase
    {
        virtual ~AnyBase() = 0;
    };

    template <class T>
    struct Any : public AnyBase
    {
        using Type = T;
        explicit Any(const Type& data) : data(data) {}
        Any() = default;

        virtual ~Any() override = default;

        Type data;
    };

    std::unordered_map<std::string, std::unique_ptr<AnyBase>> m_anymap;
};

inline Blackboard::AnyBase::~AnyBase() {}

template <typename T>
void Blackboard::set(std::string const& key, T const& value)
{
    m_anymap[key].reset(new Any<T>(value));
}

template <typename T>
Blackboard::Output<T> Blackboard::get(std::string const& key)
{
    if (m_anymap.find(key) != m_anymap.end())
    {
        try
        {
            auto value = dynamic_cast<Any<T>&>(*m_anymap[key]);
            return{ true, value.data };
        }
        catch (...)
        {
            return{ false, T() };
        }
    }

    return{ false, T() };
}

}