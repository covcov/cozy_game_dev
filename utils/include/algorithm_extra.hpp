#include <algorithm>
#include <vector>
#include <iterator>
#include <set>
#include <unordered_map>

namespace utl
{

template <typename ContainerType, typename PredicateType>
bool contains_linear(ContainerType& container, PredicateType const& predicate)
{
    return std::find_if(std::begin(container), std::end(container), predicate) != std::end(container);
}

template <typename T, typename CompT>
std::vector<T> merge(std::vector<T> const& a, std::vector<T> const& b, CompT comp = std::less<T>)
{
    size_t l = 0;
    size_t r = 0;
    std::vector<T> merged(a.size() + b.size());
    int i = 0;
    while (l < a.size() || r < b.size())
    {
        if (l == a.size())
        {
            merged[i++] = b[r++];
        }
        else if (r == b.size())
        {
            merged[i++] = a[l++];
        }
        else if (comp(a[l], b[r]))
        {
            merged[i++] = a[l++];
        }
        else
        {
            merged[i++] = b[r++];
        }
    }

    return merged;
}

namespace inplace
{

template <typename ContainerType, typename PredicateType>
void erase_if(ContainerType& container, PredicateType const& predicate)
{
    if (!container.empty())
    {
        auto it = std::remove_if(std::begin(container), std::end(container), predicate);
        container.erase(it, std::end(container));
    }
}

template <typename T, typename OrderedContainerType>
void erase_ordered_indices_unstable(std::vector<T>& container, OrderedContainerType const& indices)
{
    for (auto it = std::rbegin(indices); it != std::rend(indices); ++it)
    {
        std::swap(container.back(), container[*it]);
        container.pop_back();
    }
}

template <typename T>
void erase_index_ustable(std::vector<T>& container, size_t index)
{
    std::swap(container[index], container.back());
    container.pop_back();
}

template <typename T>
void erase_indices_unstable(std::vector<T>& container, std::vector<size_t> const& indices)
{
    if (!std::is_sorted(indices.begin(), indices.end()))
    {
        erase_ordered_indices_unstable (container, indices);
    }
    else
    {
        std::set<size_t> indices_tree;
        for (auto idx : indices)
        {
            indices_tree.insert(idx);
        }

        erase_ordered_indices_unstable(container, indices_tree);
    }
}

template <typename T>
void erase_common_unstable(std::vector<T>& a, std::vector<T>& b)
{
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    size_t i = 0;
    size_t j = 0;

    std::vector<size_t> common_indices_a;
    std::vector<size_t> common_indices_b;
    while (i < a.size() && j < b.size())
    {
        if (a[i] < b[j])
        {
            ++i;
        }
        else if (b[j] < a[i])
        {
            ++j;
        }
        else
        {
            common_indices_a.push_back(i++);
            common_indices_b.push_back(j++);
        }
    }

    erase_ordered_indices_unstable(a, common_indices_a);
    erase_ordered_indices_unstable(b, common_indices_b);
}

}

}