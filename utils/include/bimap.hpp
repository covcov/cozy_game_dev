#pragma once

#include <unordered_map>

namespace utl
{

template <typename T, typename U>
class Bimap
{
public:
    bool empty() const;
    size_t size() const;
    void insert(T const& first, U const& second);
    bool containsFirst(T const& first) const;
    bool containsSecond(U const& second) const;
    T const& firstBySecond(U const& second) const;
    U const& secondByFirst(T const& first) const;
private:
    std::unordered_map<T, U> m_first_to_second;
    std::unordered_map<U, T> m_second_to_first;
};

template<typename T, typename U>
inline bool Bimap<T, U>::empty() const
{
    return m_first_to_second.empty();
}

template<typename T, typename U>
inline size_t Bimap<T, U>::size() const
{
    return m_first_to_second.size();
}

template<typename T, typename U>
inline void Bimap<T, U>::insert(T const& first, U const& second)
{
    m_first_to_second.insert(std::make_pair(first, second));
    m_second_to_first.insert(std::make_pair(second, first));
}

template<typename T, typename U>
inline bool Bimap<T, U>::containsFirst(T const& first) const
{
    return m_first_to_second.find(first) != m_first_to_second.end();
}

template<typename T, typename U>
inline bool Bimap<T, U>::containsSecond(U const& second) const
{
    return m_second_to_first.find(second) != m_second_to_first.end();
}

template<typename T, typename U>
inline T const& Bimap<T, U>::firstBySecond(U const& second) const
{
    auto it = m_second_to_first.find(second);
    return it->second;
}

template<typename T, typename U>
inline U const& Bimap<T, U>::secondByFirst(T const& first) const
{
    auto it = m_first_to_second.find(first);
    return it->second;
}

}