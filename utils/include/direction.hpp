#pragma once

#include <array>

#include <SFML/System.hpp>

namespace utl
{

size_t constexpr DIRECTINS_N = 4;

static std::array<sf::Vector2i, DIRECTINS_N + 1> const DIRECTIONS = {
    sf::Vector2i{  0, -1  },
    sf::Vector2i{  1,  0  },
    sf::Vector2i{  0,  1  },
    sf::Vector2i{ -1,  0  },
    sf::Vector2i{  0,  0  }
};

enum class DirectionType
{
    TOP,
    RIGHT,
    BOTTOM,
    LEFT,
    NONE,
};

inline size_t to_index(utl::DirectionType direction_type)
{
    return static_cast<size_t>(direction_type);
}

template <typename T = int>
inline sf::Vector2<T> get_direction(DirectionType direction_type)
{
    size_t const idx = static_cast<size_t>(direction_type);
    return static_cast<sf::Vector2<T>>(DIRECTIONS[idx]);
}

}