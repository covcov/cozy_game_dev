#pragma once

#include <vector>

namespace utl
{

template <typename T>
class Matrix
{
public:
    Matrix(size_t width, size_t height)
        : m_width(width)
        , m_height(height)
        , m_data(m_width * m_height)
    {}

    T& operator()(size_t x, size_t y) { return m_data[y * m_width + x]; }
    T const& operator()(size_t x, size_t y) const { return m_data[y * m_width + x]; }
    void setAll(T const& value);

    size_t getWidth() const { return m_width; }
    size_t getHeight() const { return m_height; }
    bool contains(int x, int y) const
    {
        return static_cast<size_t>(x) < m_width && static_cast<size_t>(y) < m_height;
    }

private:
    size_t m_width;
    size_t m_height;
    std::vector<T> m_data;
};

template <typename T>
void Matrix<T>::setAll(T const& value)
{
    std::fill(m_data.begin(), m_data.end(), value);
}

} // namespace utl
