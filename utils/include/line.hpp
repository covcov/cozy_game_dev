#pragma once

#include <SFML/System.hpp>

#include "math.hpp"

namespace utl
{

struct Line
{
    Line() = default;
    Line(sf::Vector2f const& from, sf::Vector2f const& to)
        : a(from.y - to.y)
        , b(to.x - from.x)
        , c(from.x* to.y - to.x * from.y)
    {}

    float a;
    float b;
    float c;
};

struct Intersection
{
    Intersection() = default;
    Intersection(bool doIntersect, sf::Vector2f const& intersectionPoint)
        : valid(doIntersect)
        , intersectionPoint(intersectionPoint)
    {}

    bool valid = false;
    sf::Vector2f intersectionPoint;
};

Intersection find_intersection(Line const& l_1, Line const& l_2);

}