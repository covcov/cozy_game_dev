#pragma once

#include <memory>

#include <SFML/System.hpp>

class GameObject;

class Position
{
public:
    Position() = default;
    Position const& operator=(sf::Vector2f const& position);
    Position(sf::Vector2f const& position);

    void setTarget(std::shared_ptr<GameObject> game_object);

    sf::Vector2f const& get() const { return m_position; }

    sf::Vector2f operator+(sf::Vector2f const& value);
    sf::Vector2f operator-(sf::Vector2f const& value);
    sf::Vector2f operator*(float value);
    sf::Vector2f operator/(float value);
    friend sf::Vector2f operator+(sf::Vector2f const& value, Position const& rhs) { return rhs.get() + value; }
    friend sf::Vector2f operator-(sf::Vector2f const& value, Position const& rhs) { return value - rhs.get(); }
    friend sf::Vector2f operator*(float value, Position const& rhs) { return rhs.get() * value; }

    Position& operator+=(sf::Vector2f const& value);
    Position& operator-=(sf::Vector2f const& value);
    Position& operator*=(float value);
    Position& operator/=(float value);

    bool operator==(Position const& rhs) const { return m_position == rhs.m_position; }
    bool operator!=(Position const& rhs) const { return m_position != rhs.m_position; }

    operator sf::Vector2f() const { return m_position; }

private:
    void applyChange(sf::Vector2f const& position);

    std::weak_ptr<GameObject> m_game_object;
    sf::Vector2f m_position;
};