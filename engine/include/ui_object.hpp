#pragma once

#include "game_object.hpp"

class UIObject : public GameObject
{
public:
    UIObject(std::string const& name) : GameObject(name) {}

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

protected:
    sf::Vector2f m_awake_postion;
};