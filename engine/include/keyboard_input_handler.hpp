#pragma once

#include <array>
#include <unordered_map>

#include "i_input_handler.hpp"

class KeyboardInputHandler : public IInputHandler
{
public:
    KeyboardInputHandler() = default;

    virtual void update() override;
    virtual bool isPressed(std::string const& name) override;
    virtual bool isReleased(std::string const&) override;
    virtual bool isPressedInUpdate(std::string const&) override;
    virtual bool isReleasedInUpdate(std::string const&) override;
    virtual sf::Vector2f getPointerPosition(sf::RenderWindow&, PointerPositionType type = PointerPositionType::WORLD) { return{}; };

    void bind(std::string const& name, sf::Keyboard::Key key);

private:
    std::unordered_multimap<std::string, sf::Keyboard::Key> m_key_binds;
    std::array<bool, sf::Keyboard::Key::KeyCount> m_is_key_pressed;
    std::array<bool, sf::Keyboard::Key::KeyCount> m_is_key_pressed_in_update;
    std::array<bool, sf::Keyboard::Key::KeyCount> m_is_key_released_in_update;
};