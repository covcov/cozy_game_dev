#pragma once

#include "effect.hpp"

class BlinkEffect : public Effect
{
public:
    BlinkEffect(float time_period, float transparency_min, float transparency_max);

    virtual void update(float time_delta) override;
    virtual void apply(sf::Sprite& sprite) override;
    virtual bool isFinished() const override;
    virtual Effect::Pointer clone() const override;

private:
    float m_transparency;
    float m_time_period;
    float m_time_elapsed;
    float m_transparency_min;
    float m_transparency_max;
};
