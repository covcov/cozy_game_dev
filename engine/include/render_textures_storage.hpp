#pragma once

#include <memory>
#include <unordered_map>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class RenderTexturesStorage
{
public:
    RenderTexturesStorage() = default;

    std::shared_ptr<sf::RenderTexture> get(sf::Vector2i const& size);
    void reset();

private:
    using RenderTexturesT = std::vector<std::pair<bool, std::shared_ptr<sf::RenderTexture>>>;
    void add(RenderTexturesT& render_textures, sf::Vector2i const& size);

private:
    struct HashFunctor
    {
        size_t operator()(sf::Vector2i const& size) const
        {
            return std::hash<int>()(size.x) ^ std::hash<int>()(size.y);
        }
    };

    std::unordered_map<sf::Vector2i, RenderTexturesT, HashFunctor> m_render_textures_storage;
};