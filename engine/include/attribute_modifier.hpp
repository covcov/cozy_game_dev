#pragma once

#include <memory>
#include <string>

#include "game_object.hpp"

class AttributeModifier : public GameObject
{
public:
    AttributeModifier(
        std::string const& attribute_name,
        int value,
        float duration,
        GameObject::Pointer target)
        : GameObject(attribute_name + "_modifier")
        , m_attribute_name(attribute_name)
        , m_duration(duration)
        , m_value(value)
        , m_elapsed_time(0.f)
        , m_target(target)
    {}

    virtual void awake() override;
    virtual void update(float time_delta) override;

private:
    std::string m_attribute_name;
    float m_duration;
    int m_value;
    float m_elapsed_time;
    GameObject::Pointer m_target;
};