#pragma once

#include <string>
#include <memory>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class IInputHandler
{
public:
    using Pointer = std::shared_ptr<IInputHandler>;

    enum class PointerPositionType
    {
        WORLD,
        WINDOW
    };

    virtual void update() = 0;
    virtual bool isPressed(std::string const&) = 0;
    virtual bool isReleased(std::string const&) = 0;
    virtual bool isPressedInUpdate(std::string const&) = 0;
    virtual bool isReleasedInUpdate(std::string const&) = 0;
    virtual sf::Vector2f getPointerPosition(sf::RenderWindow&, PointerPositionType) = 0;
};