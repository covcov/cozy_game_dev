#pragma once

#include "creature.hpp"

class HealthBar : public GameObject
{
public:
    HealthBar(std::string const& name, std::shared_ptr<Creature> creature, sf::Vector2i const& size, sf::Vector2f shift = sf::Vector2f(0.f, -10.f));

    virtual void update(float time_delta) override;
    virtual void draw(sf::RenderTarget& render_target) override;

private:
    std::shared_ptr<Creature> m_creature;
    sf::Vector2i m_size;
    sf::Vector2f m_shift;
    sf::Sprite m_sprite;
};