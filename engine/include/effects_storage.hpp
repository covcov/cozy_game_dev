#pragma once

#include <unordered_map>
#include <string>

#include "effect.hpp"

class EffectsStorage
{
public:
    void update(float time_delta);
    void apply(sf::Sprite& sprite) const;
    void add(std::string const& name, Effect::Pointer effect);
    void remove(std::string const& name);
    bool contains(std::string const& name);
    bool empty() const { return m_effects.empty(); }

private:
    std::unordered_map<std::string, Effect::Pointer> m_effects;
};
