#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "i_input_handler.hpp"
#include "camera.hpp"
#include "render_textures_storage.hpp"
#include "sound_engine.hpp"
#include "fonts_storage.hpp"

class GameResourcesStorage
{
public:
    GameResourcesStorage(GameResourcesStorage const&) = delete;
    void operator=(GameResourcesStorage const&) = delete;

    static std::shared_ptr<GameResourcesStorage> instance();
    static void init(sf::RenderWindow& render_window, Camera::Pointer camera, IInputHandler::Pointer input_handler);

    sf::RenderWindow& getRenderWindow() { return m_render_window; };
    sf::RenderTexture& getRenderTexture() { return m_render_texture; };
    RenderTexturesStorage& getRenderTexturesStorage() { return m_render_textures_storage; }
    SoundEngine& getSoundEngine() { return m_sound_engine; }
    FontsStorage& getFontsStorage() { return m_fonts_storage; }
    Camera::Pointer getCamera() { return m_camera; }
    IInputHandler::Pointer getInputHandler() { return m_input_handler; }

private:
    GameResourcesStorage(sf::RenderWindow& render_window, Camera::Pointer camera, IInputHandler::Pointer input_handler);

private:
    sf::RenderWindow& m_render_window;
    sf::RenderTexture m_render_texture;
    RenderTexturesStorage m_render_textures_storage;
    SoundEngine m_sound_engine;
    FontsStorage m_fonts_storage;
    Camera::Pointer m_camera;
    IInputHandler::Pointer m_input_handler;

    static bool is_initialized;
    static std::shared_ptr<GameResourcesStorage> single;
};
