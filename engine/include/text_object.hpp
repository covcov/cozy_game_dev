#pragma once

#include "game_object.hpp"

class TextObject : public GameObject
{
public:
    struct Params
    {
        Params()
            : font_type("DEFAULT")
            , text_color(sf::Color::White)
            , outline_color(sf::Color::White)
            , background_color(sf::Color::Transparent)
            , scale(1.f)
            , outline_thickness(0)
            , padding(4)
        {}

        std::string font_type;
        sf::Color text_color;
        sf::Color outline_color;
        sf::Color background_color;
        float scale;
        float outline_thickness;
        int padding;
    };

    TextObject(std::string const& name, std::string const& text, Params params);
    virtual void draw(sf::RenderTarget& render_target) override;
    virtual void update(float) override;

    void bindToTarget(GameObject::Pointer target)
    {
        m_draw_position_initialized = false;
        m_target = target;
    }

private:
    void init();

private:
    std::string m_text;
    Params m_params;
    sf::Sprite m_sprite;

    GameObject::Pointer m_target;
    bool m_draw_position_initialized = true;
    sf::Vector2f m_draw_position;
};