#pragma once

#include <iostream>
#include <sstream>

#include <SFML/Graphics.hpp>

#include "rect.hpp"
#include "circle.hpp"
#include "box_collider.hpp"
#include "circle_collider.hpp"
#include "grid_map.hpp"
#include "game_resources_storage.hpp"

struct Debug
{
    static void toggleDeveloperMode();

    template <typename ...Args>
    static void log(Args... args);

    template <typename RenderOutput>
    static void draw(RenderOutput& render_output, Collider::Pointer collider, sf::Color color = sf::Color::Green);

    template <typename RenderOutput>
    static void draw(RenderOutput& render_output, GridMap::Pointer grid_map, sf::Color color = sf::Color::Red);

    template <typename RenderOutput>
    static void draw(RenderOutput& render_output, utl::Rect<float> const& rect, sf::Color color);

    template <typename RenderOutput>
    static void draw(RenderOutput& render_output, utl::Circle<float> const& circle, sf::Color color);

    static void drawLine(sf::Vector2f const& from, sf::Vector2f const& to, sf::Color const& color);

    static void drawCircle(sf::Vector2f const& center, float radius, sf::Color const& color);

    static void draw(std::shared_ptr<sf::RenderTexture> render_texture);
private:
    struct Line
    {
        Line() = default;
        Line(sf::Vector2f const& from, sf::Vector2f const& to, sf::Color const& color)
            : from(from)
            , to(to)
            , color(color)
        {}

        sf::Vector2f from;
        sf::Vector2f to;
        sf::Color color;
    };

    template <typename T, typename ...Args>
    static std::string parse(T value, Args... args);

    template <typename T>
    static std::string parse(T value);

    static bool isEnabled;

    static std::vector<Line> lines;
    static std::vector<std::pair<utl::Circle<float>, sf::Color>> circles;
};

template<typename ...Args>
inline void Debug::log(Args ...args)
{
#ifdef LOG
    std::string str = parse(args...);
    std::cout << str << std::endl;
#endif
}

template <typename RenderOutput>
void Debug::draw(RenderOutput& render_output, Collider::Pointer collider, sf::Color color)
{
#ifdef LOG
    if (!isEnabled)
    {
        return;
    }

    if (collider)
    {
        if (collider->getType() == Collider::Type::BOX)
        {
            auto shape = std::dynamic_pointer_cast<BoxCollider>(collider)->getShape();
            draw(render_output, shape, color);
        }
        else if (collider->getType() == Collider::Type::CIRCLE)
        {
            auto shape = std::dynamic_pointer_cast<CircleCollider>(collider)->getShape();
            draw(render_output, shape, color);
        }
    }
#endif
}

template <typename RenderOutput>
void Debug::draw(RenderOutput& render_output, GridMap::Pointer grid_map, sf::Color color)
{
#ifdef LOG
    if (!isEnabled)
    {
        return;
    }

    auto const tile_map_size = grid_map->getGridSize();
    auto const tile_size = grid_map->getCellSize();
    auto const tl = grid_map->getTopLeftPosition();
    for (size_t y = 0; y < tile_map_size.y; ++y)
    {
        for (size_t x = 0; x < tile_map_size.x; ++x)
        {
            utl::Rect<float> rect = {
                tl.x + x * tile_size,
                tl.y + y * tile_size,
                tile_size,
                tile_size
            };

            sf::Vector2i const cell(x, y);
            draw(render_output, rect, grid_map->cellContainsTag(cell, "player") ? color : sf::Color::Cyan);
        }
    }
#endif
}

template <typename RenderOutput>
void Debug::draw(RenderOutput& render_output, utl::Rect<float> const& rect, sf::Color color)
{
#ifdef LOG
    if (!isEnabled)
    {
        return;
    }

    sf::RectangleShape rect_shape;
    rect_shape.setSize({ rect.width - 2, rect.height - 2 });
    auto camera = GameResourcesStorage::instance()->getCamera();
    rect_shape.setOutlineThickness(1);
    rect_shape.setFillColor(sf::Color::Transparent);
    rect_shape.setOutlineColor(color);
    rect_shape.setPosition({ rect.left + 1, rect.top + 1 });
    render_output.draw(rect_shape);
#endif
}

template <typename RenderOutput>
void Debug::draw(RenderOutput& render_output, utl::Circle<float> const& circle, sf::Color color)
{
#ifdef LOG
    if (!isEnabled)
    {
        return;
    }

    sf::CircleShape circle_shape;
    circle_shape.setRadius(circle.radius);
    auto camera = GameResourcesStorage::instance()->getCamera();
    circle_shape.setOutlineThickness(1);
    circle_shape.setFillColor(sf::Color::Transparent);
    circle_shape.setOutlineColor(color);
    sf::Vector2f position(circle.position.x - circle.radius, circle.position.y - circle.radius);
    circle_shape.setPosition(position);
    render_output.draw(circle_shape);
#endif
}

template <typename T, typename ...Args>
std::string Debug::parse(T value, Args ...args)
{
    std::stringstream ss;
    ss << value << " " << parse(args...);
    return ss.str();
}

template <typename T>
std::string Debug::parse(T value)
{
    std::stringstream ss;
    ss << value << " ";
    return ss.str();
}
