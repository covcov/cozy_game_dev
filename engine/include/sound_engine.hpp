#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <queue>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class SoundEngine
{
public:
    SoundEngine();

    void update();
    void add(std::string const& name, std::string const& directory_name, std::string const& file_name);
    void add(std::string const& name, sf::SoundBuffer buffer);
    void play(std::string const& name, int volume, bool is_looped, float pitch = 1);
    void toggleMute();

private:
    struct SoundDescriptor
    {
        SoundDescriptor(std::shared_ptr<sf::Sound> sound, float end_time, size_t id)
            : sound(sound)
            , end_time(end_time)
            , id(id)
        {}

        std::shared_ptr<sf::Sound> sound;
        float end_time;
        size_t id;

        bool operator<(SoundDescriptor const& other) const
        {
            return end_time > other.end_time;
        }
    };

private:
    std::unordered_map<std::string, size_t> m_name_to_index_table;
    std::vector<std::shared_ptr<sf::SoundBuffer>> m_buffers;
    std::priority_queue<SoundDescriptor> m_sound_pqueue;
    std::vector<std::shared_ptr<sf::Sound>> m_loops;
    std::unordered_map<size_t, std::pair<std::shared_ptr<sf::Sound>, int>> m_sounds;
    std::unordered_map<size_t, size_t> m_added_index_to_id;
    bool m_is_muted;
    sf::Clock m_clock;
};