#pragma once

#include "effect.hpp"

class ColorFlashEffect : public Effect
{
public:
    ColorFlashEffect(float duration, sf::Color const& color);
    ColorFlashEffect(float duration, sf::Color const& color, std::shared_ptr<sf::Shader> shader);

    virtual void update(float time_delta) override;
    virtual void apply(sf::Sprite& sprite) override;
    virtual bool isFinished() const override;
    virtual Effect::Pointer clone() const override;

private:
    float m_duration;
    float m_time_elapsed;
    sf::Color m_color;

    std::shared_ptr<sf::Shader> m_shader;
};