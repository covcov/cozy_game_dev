#pragma once

#include <array>

#include "game_object.hpp"
#include "engine.hpp"
#include "grid_map.hpp"
#include "direction.hpp"
#include "line.hpp"

namespace engine
{

class RayCaster
{
public:
    static std::pair<sf::Vector2f, GameObject::Pointer> cast(
        sf::Vector2f const& from,
        sf::Vector2f const& to,
        std::unordered_set<std::string> const& collision_mask,
        std::vector<GameObject::Pointer> const& ignore_objects = {});

private:
    struct Box
    {
        Box(sf::Vector2f const& tl, float segment_width, float segment_height);
        sf::Vector2i nextDirection(utl::Line const& ray, sf::Vector2f const& from, sf::Vector2f const& to) const;
        utl::Intersection collisionPoint(utl::Line const& ray, sf::Vector2f const& from, sf::Vector2f const& to) const;
        bool isCorner(sf::Vector2f const& point) const;

        float segment_width;
        float segment_height;
        sf::Vector2f top_left_position;
        std::array<std::pair<sf::Vector2f, sf::Vector2f>, utl::DIRECTINS_N> segments;
        std::array<utl::Line, utl::DIRECTINS_N> lines;
    };

private:
    static utl::Intersection findIntersection(
        utl::Line const& ray,
        sf::Vector2f const& from,
        sf::Vector2f const& to,
        GameObject::Pointer game_object);

    static bool belongsToSegment(sf::Vector2f const& p, sf::Vector2f const& sp_1, sf::Vector2f const& sp_2);
};

} // namespace engine