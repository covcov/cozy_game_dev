#pragma once

#include <array>
#include <vector>
#include <unordered_set>
#include <string>

#include <SFML/System.hpp>

#include "matrix.hpp"
#include "direction.hpp"

class MapPreprocessor
{
public:
    struct Result
    {
        std::vector<sf::Vector2f> corners;
        std::vector<std::pair<sf::Vector2f, sf::Vector2f>> edges;
    };

    static Result preprocess(std::unordered_set<std::string> const& mask);

private:
    struct Cell
    {
        Cell()
        {
            edge_exist.fill(false);
            edge_ids.fill(0);
        }

        std::array<bool, utl::DIRECTINS_N> edge_exist;
        std::array<size_t, utl::DIRECTINS_N> edge_ids;
        bool exist = false;
    };

    struct Edge
    {
        Edge() = default;
        Edge(sf::Vector2i const& from, sf::Vector2i const& to) : from(from), to(to) {}

        sf::Vector2i from;
        sf::Vector2i to;
    };

    static void extendEdge(
        utl::Matrix<Cell>& tile_map,
        size_t x,
        size_t y,
        std::vector<Edge>& edges,
        sf::Vector2i const& np,
        sf::Vector2i const& extend_from,
        size_t ni,
        utl::DirectionType from,
        utl::DirectionType to);
};