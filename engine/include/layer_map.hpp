#pragma once

#include <vector>
#include <unordered_map>
#include <string>

class LayerMap
{
public:
    static size_t constexpr WRONG_IDX = std::numeric_limits<size_t>::max();

    LayerMap(LayerMap const&) = delete;
    void operator=(LayerMap const&) = delete;
    ~LayerMap() = default;

    static LayerMap& instance();

    size_t getLayersNumber() const { return m_index_to_layer.size(); };
    size_t getIndex(std::string const& layer) const;
    std::string const& getLayer(size_t index) const;
    void add(std::initializer_list<std::string> const& init_list);

private:
    LayerMap() = default;

    void add(std::string const& layer);
private:

    std::unordered_map<std::string, int> m_layer_to_index;
    std::vector<std::string> m_index_to_layer;
	bool m_initialized = false;
};