#pragma once

#include "game_object.hpp"

class Camera
{
public:
    using Pointer = std::shared_ptr<Camera>;

    Camera(float zone_size = 64, float scale = 0.25f)
        : m_shift({ 0.f, 0.f })
        , m_target(nullptr)
        , m_scale(scale)
        , m_camera_zone()
    {
        m_camera_zone.width = zone_size;
        m_camera_zone.height = zone_size;
    }

    void setTarget(GameObject::Pointer target);
    GameObject::Pointer getTarget() const { return m_target; }
    void setScale(float scale) { m_scale = scale; }

    float getScale() const { return m_scale; }
    void applyShift(sf::Vector2f const& shift) { m_shift = shift; }
    void update();
    sf::Vector2f getTargetPosition() const;

private:
    sf::Vector2f m_shift;
    GameObject::Pointer m_target;
    float m_scale;
    utl::Rect<float> m_camera_zone;
};