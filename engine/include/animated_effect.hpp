#pragma once

#include "game_object.hpp"

class AnimatedEffect : public GameObject
{
public:
    AnimatedEffect(std::string const& name) : GameObject(name) {}

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
};