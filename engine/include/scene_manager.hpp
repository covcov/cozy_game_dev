#pragma once

#include <string>
#include <unordered_map>
#include <memory>

#include "scene.hpp"
#include "camera.hpp"

class SceneManager : public std::enable_shared_from_this<SceneManager>
{
public:
    SceneManager(SceneManager const&) = delete;
    void operator=(SceneManager const&) = delete;

    static SceneManager& instance();

    bool registerScene(std::string const& name, Scene::Pointer scene, std::string const& next);
    bool registerScene(std::string const& name, Scene::Pointer scene);
    void reset();
    void playNext();
    void setActive(std::string const& name);
    void runMainLoop();
    Scene::Pointer getActive() const
    {
        if (m_active_index < m_scenes.size())
        {
            return m_scenes[m_active_index];
        }

        return nullptr;
    }

private:
    SceneManager() = default;

    void lateReset();
private:
    size_t m_active_index;
    size_t m_next_index;
    std::vector<std::string> m_names;
    std::vector<std::string> m_next_scene_names;
    std::vector<Scene::Pointer> m_scene_prototypes;
    std::vector<Scene::Pointer> m_scenes;
    std::unordered_map<std::string, size_t> m_name_to_index_table;

    bool m_first_update = true;
    bool m_reset = false;

    static bool is_run;

    static constexpr size_t DEFAULT_FPS = 60;
};