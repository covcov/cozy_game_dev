#pragma once

#include <string>
#include <unordered_map>

#include "animation.hpp"

class AnimationGraphFactory;

class AnimationGraph
{
public:
    bool makeTransition(std::string const& message);
    void update(float time_delta);
    Animation::Pointer getActive();
    void setActive(std::string const& name);
    void setShift(sf::Vector2f const& shift) { m_shift = shift; }
    sf::Vector2f const& getShift() const { return m_shift; }
    AnimationGraph clone() const;

private:
    friend class AnimationGraphFactory;
    void registerTransition(std::string const& from, std::string const& to, std::string const& message);
    void registerAnimation(Animation::Pointer animation, std::string const& name);

private:
    size_t m_active = 0;

    std::unordered_map<std::string, size_t> m_name_to_index;
    std::vector<std::string> m_names;
    std::vector<std::unordered_map<std::string, size_t>> m_transitions;
    std::vector<Animation::Pointer> m_animations;
    sf::Vector2f m_shift;
};