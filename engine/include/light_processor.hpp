#pragma once

#include "game_object.hpp"

class LightProcessor : public GameObject
{
public:
    LightProcessor() : GameObject("light_processor") {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;
    virtual void draw(sf::RenderTarget& render_target) override;

    void addSource(GameObject::Pointer game_object, float radius);

private:
    std::vector<sf::Vector2f> castThreeRays(
        sf::Vector2f const& origin,
        sf::Vector2f const& target,
        std::vector<std::pair<sf::Vector2f, sf::Vector2f>> const& edges) const;

    std::vector<sf::Vector2f> castToCorners(sf::Vector2f const& origin) const;

    sf::Vector2f getHit(
        sf::Vector2f const& origin,
        sf::Vector2f const& direction,
        std::vector<std::pair<sf::Vector2f, sf::Vector2f>> const& edges) const;

private:
    sf::Sprite m_sprite;
    std::vector<std::pair<GameObject::Pointer, float>> light_sources;
    std::vector<sf::Vector2f> m_corners;
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> m_edges;
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> m_visible_circle_edges;

    static float constexpr LIGHT_SOURCE_RADIUS = 64.f;
};
