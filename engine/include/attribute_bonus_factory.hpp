#pragma once

#include "attribute_bonus.hpp"

struct AttributeBonusFactory
{
    enum class Type
    {
        SPEED,
        POWER,
        CHARGE
    };

    static GameObject::Pointer create(Type type);

private:
    static std::unordered_map<Type, std::string> const TYPE_TO_ATTRIBUTE_NAME_TABLE;
    static std::unordered_map<Type, std::string> const TYPE_TO_TOOLTIP_TEXT_TABLE;
};