#pragma once

#include <unordered_map>
#include <memory>

#include <SFML/Graphics.hpp>

class FontsStorage
{
public:
    void add(std::string const& name, std::string const& path, int character_size);
    std::shared_ptr<sf::Font> getFont(std::string const& name);
    int getSize(std::string const& name);

private:
    std::unordered_map<std::string, std::shared_ptr<sf::Font>> m_fonts;
    std::unordered_map<std::string, int> m_sizes;
};