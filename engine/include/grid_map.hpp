#pragma once

#include <vector>
#include <unordered_set>

#include "game_object.hpp"
#include "matrix.hpp"

class GridMap
{
public:
    using Pointer = std::shared_ptr<GridMap>;

    GridMap(
        size_t width,
        size_t height,
        float cell_size,
        sf::Vector2f top_left_position)
        : m_grid_storage(width, height)
        , m_cell_size(cell_size)
        , m_top_left_position(top_left_position)
    {}

    void add(GameObject::Pointer game_object);
    void add(sf::Vector2i const& cell, GameObject::Pointer game_object);
    std::vector<GameObject::Pointer> getCollidedObjects(GameObject::Pointer game_object) const;
    std::vector<GameObject::Pointer> getCollidedObjects(
        GameObject::Pointer game_object,
        std::unordered_set<std::string> const& mask) const;

    bool contains(sf::Vector2i const& cell) const { return m_grid_storage.contains(cell.x, cell.y); }
    std::vector<GameObject::Pointer> const& getCellObjects(size_t x, size_t y) const { return m_grid_storage(x, y); };
    std::vector<GameObject::Pointer> const& getCellObjects(sf::Vector2i const& cell) const { return m_grid_storage(cell.x, cell.y); };
    GameObject::Pointer getCellObjectByTag(sf::Vector2i const& cell, std::string const& tag) const;
    bool cellContainsTag(sf::Vector2i const& cell, std::string const& tag) const;
    bool cellContainsAnyOfTags(sf::Vector2i const& cell, std::unordered_set<std::string> const& tags) const;
    bool cellContainsObject(sf::Vector2i const& cell, GameObject::Pointer object) const;

    sf::Vector2i getGridSize() const
    {
        return sf::Vector2i(
            static_cast<int>(m_grid_storage.getWidth()),
            static_cast<int>(m_grid_storage.getHeight()));
    }

    float getCellSize() const { return m_cell_size; }
    sf::Vector2f cellToWorldPosition(sf::Vector2i const& position) const;
    sf::Vector2i worldToCellPosition(sf::Vector2f const& position) const;
    sf::Vector2f getTopLeftPosition() const { return m_top_left_position; }
    sf::Vector2f getBottomRightPosition() const
    {
        auto const& tl = m_top_left_position;
        auto const w = m_grid_storage.getWidth() * m_cell_size;
        auto const h = m_grid_storage.getHeight() * m_cell_size;
        return m_top_left_position + sf::Vector2f(w, h);
    }

    sf::Vector2f getCellTopLeftPosition(sf::Vector2i const& position) const
    {
        return { m_cell_size * position.x, m_cell_size * position.y };
    }

    void processObjectMove(GameObject::Pointer game_object, sf::Vector2f old_position, sf::Vector2f new_position);
    void processObjectRemove(GameObject::Pointer game_object);

private:
    std::pair<sf::Vector2i, sf::Vector2i> getRegionOfInterest(utl::Rect<float> const& rect) const;
    void eraseFromRegionOfInterest(GameObject::Pointer game_object, std::pair<sf::Vector2i, sf::Vector2i> const& roi);
    void addToRegionOfInterest(GameObject::Pointer game_object, std::pair<sf::Vector2i, sf::Vector2i> const& roi);

private:
    utl::Matrix<std::vector<GameObject::Pointer>> m_grid_storage;
    sf::Vector2f m_top_left_position;
    float m_cell_size;
};