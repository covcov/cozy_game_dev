#include <array>
#include <unordered_map>

#include <SFML/System.hpp>

#include "i_input_handler.hpp"

class MouseInputHandler : public IInputHandler
{
public:
    MouseInputHandler() = default;

    virtual void update() override;
    virtual bool isPressed(std::string const& name) override;
    virtual bool isReleased(std::string const&) override;
    virtual bool isPressedInUpdate(std::string const&) override;
    virtual bool isReleasedInUpdate(std::string const&) override;
    virtual sf::Vector2f getPointerPosition(sf::RenderWindow& render_window, PointerPositionType type = PointerPositionType::WORLD) override;

    void bind(std::string const& name, sf::Mouse::Button button);

private:
    std::unordered_multimap<std::string, sf::Mouse::Button> m_button_binds;
    std::array<bool, sf::Mouse::Button::ButtonCount> m_is_button_pressed;
    std::array<bool, sf::Mouse::Button::ButtonCount> m_is_button_pressed_in_update;
    std::array<bool, sf::Mouse::Button::ButtonCount> m_is_button_released_in_update;
};