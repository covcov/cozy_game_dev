#pragma once

#include <iostream>

#include "collider.hpp"
#include "circle.hpp"

class CircleCollider : public Collider
{
public:
    utl::Circle<float> getShape() const
    {
        return { m_position + m_shift, m_radius };
    }

    virtual utl::Rect<float> getAABB() const override
    {
        utl::Rect<float> aabb{
            m_position.get().x + m_shift.x - m_radius,
            m_position.get().y + m_shift.y - m_radius,
            2 * m_radius - utl::DISTANCE_EPS,
            2 * m_radius - utl::DISTANCE_EPS };

        return aabb;
    }

    virtual Collider::Pointer clone(Position& where) const override
    {
        return std::shared_ptr<CircleCollider>(new CircleCollider(where, m_shift, m_radius));
    }

private:
    friend struct ColliderFactory;

    CircleCollider(Position& position, sf::Vector2f const& shift, float radius)
        : Collider(position, shift, Collider::Type::CIRCLE)
        , m_radius(radius)
    {}

    float const m_radius;
};