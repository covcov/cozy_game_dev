#include "i_input_handler.hpp"

#include "keyboard_input_handler.hpp"
#include "mouse_input_handler.hpp"

class KeyboardMouseInputHandler : public IInputHandler
{
public:
    KeyboardMouseInputHandler() = default;

    virtual void update() override;
    virtual bool isPressed(std::string const& name) override;
    virtual bool isReleased(std::string const&) override;
    virtual bool isPressedInUpdate(std::string const&) override;
    virtual bool isReleasedInUpdate(std::string const&) override;
    virtual sf::Vector2f getPointerPosition(sf::RenderWindow& render_window, PointerPositionType type = PointerPositionType::WORLD) override;

    void bindKeyboardKey(std::string const& name, sf::Keyboard::Key key);
    void bindMouseButton(std::string const& name, sf::Mouse::Button key);

private:
    KeyboardInputHandler keyboard_input_handler;
    MouseInputHandler mouse_input_handler;
};