#include "game_object.hpp"

class LightSource : public GameObject
{
public:
    LightSource(std::string const& name, float radius)
        : GameObject(name)
        , m_radius(radius)
    {}

    virtual void awake() override;
    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    GameObject::Pointer m_light_processor;
    float m_radius;
};