#include "rect.hpp"
#include "circle.hpp"
#include "collider.hpp"

struct CollisionManager
{
    static bool collide(Collider::Pointer collider, sf::Vector2f const& position);
    static bool collide(Collider::Pointer collider1, Collider::Pointer collider2);
    
private:
    static bool collide(utl::Circle<float> const& circle, utl::Rect<float> const& rect);

    static bool collide(utl::Rect<float> const& rect, utl::Circle<float> const& circle)
    {
        return collide(circle, rect);
    }

    template <typename T>
    static bool collide(T const& shape1, T const& shape2)
    {
        return shape1.intersects(shape2);
    }

    template <typename T>
    static bool collide(T const& shape, Collider::Pointer other);
};