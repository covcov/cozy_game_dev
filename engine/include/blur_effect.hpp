#pragma once

#include "effect.hpp"

class BlurEffect : public Effect
{
public:
    BlurEffect(int blur_size);
    BlurEffect(int blur_size, float duration);

    virtual void update(float time_delta) override;
    virtual void apply(sf::Sprite& sprite) override;
    virtual bool isFinished() const override;
    virtual Effect::Pointer clone() const override;

private:
    bool m_is_infinite;
    float m_duration;
    float m_time_elapsed = 0.f;
    int m_blur_size;
    std::shared_ptr<sf::Shader> m_shader_x;
    std::shared_ptr<sf::Shader> m_shader_y;
};