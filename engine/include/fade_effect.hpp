#pragma once

#include "effect.hpp"

class FadeEffect : public Effect
{
public:
    FadeEffect(float duration, bool is_fade_out);

    virtual void update(float time_delta) override;
    virtual void apply(sf::Sprite& sprite) override;
    virtual bool isFinished() const override;
    virtual Effect::Pointer clone() const override;

private:
    float m_transparency;
    float m_duration;
    float m_time_elapsed;
    bool m_is_fade_out;
};