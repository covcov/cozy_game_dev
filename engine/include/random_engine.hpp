#pragma once

#include <iostream>
#include <ctime>

struct RandomEngine
{
    static void setSeed(int seed = clock());
    static int randomRange(int low, int high);
    static float randomRange(float low, float high);
    static float random();
};