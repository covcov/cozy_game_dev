#pragma once

#include "game_object.hpp"
#include "camera.hpp"

class CameraShaker : public GameObject
{
public:
    CameraShaker(
        std::string const& name,
        float duration,
        sf::Vector2f const& amplitude)
        : GameObject(name)
        , m_duration(duration)
        , m_elapsed_time(0.f)
        , m_amplitude(amplitude)
    {}

    virtual void update(float time_delta) override;
private:
    float m_duration;
    float m_elapsed_time;
    sf::Vector2f m_amplitude;
};