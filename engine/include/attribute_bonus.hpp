#pragma once

#include "game_object_with_tooltip.hpp"

class AttributeBonus : public GameObjectWithTooltip
{
public:
    AttributeBonus(std::string const& attribute_name, std::string const& tooltip_text, float duration, int value);

    virtual void update(float time_delta) override;
    virtual GameObject::Pointer clone() const override;

private:
    std::string m_attribute_name;
    float m_duration;
    int m_value;
};