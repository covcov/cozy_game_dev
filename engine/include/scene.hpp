#pragma once

#include <vector>
#include <unordered_map>

#include "game_object.hpp"
#include "i_input_handler.hpp"
#include "path_search_engine.hpp"
#include "grid_map.hpp"
#include "blackboard.hpp"
#include "debug.hpp"

class Scene
{
public:
    using Pointer = std::shared_ptr<Scene>;

    void operator=(Scene const&) = delete;
    Scene(Scene const&) = delete;
    virtual ~Scene() = default;
    static Pointer create(
        size_t width,
        size_t height,
        float cell_size,
        sf::Vector2f top_left_position);

    Pointer clone();
    void update(float time_delta);
    void draw();
    void add(GameObject::Pointer game_object, std::string const& layer);
    void add(GameObject::Pointer game_object, std::string const& layer, sf::Vector2i const& cell);
    void inform(std::string const& tag, int message);
    void destroy(GameObject::Pointer game_object);
    void awakeAll();
    GameObject::Pointer findObjectById(size_t id);
    GameObject::Pointer findObjectByName(std::string const& name);
    std::vector<GameObject::Pointer> findObjectsByName(std::string const& name);
    std::vector<GameObject::Pointer> findObjectsByTag(std::string const& tag);
    GridMap::Pointer getGridMap() const { return m_grid_map; }
    PathSearchEngine::Pointer getPathSearchEngine() const { return m_path_search_engine; }
    float getTimeScale() const { return m_time_scale; }
    void setTimeScale(float time_scale) { m_time_scale = time_scale; }
    utl::Blackboard blackboard;

protected:
    friend class SceneManager;
    Scene();
    void lateAdd(GameObject::Pointer game_object, std::string const& layer);
    void lateAdd(GameObject::Pointer game_object, std::string const& layer, sf::Vector2i const& cell);
    void lateDestroy(GameObject::Pointer game_object);
    void destroyObjects();
    void addObjects();
    void destroyById(size_t id);

protected:
    std::vector<size_t> m_destroy_storage;
    std::vector<std::pair<GameObject::Pointer, std::string>> m_add_storage;
    std::unordered_map<size_t, size_t> m_id_to_index;
    std::unordered_map<size_t, size_t> m_id_to_layer;
    std::vector<GameObject::Pointer> m_game_objects;
    std::unordered_multimap<std::string, size_t> m_name_to_id;
    std::unordered_map<std::string, std::vector<int>> m_tag_to_message_table;
    GridMap::Pointer m_grid_map;
    PathSearchEngine::Pointer m_path_search_engine;
    float m_time_scale;
    bool m_is_updating = false;
};