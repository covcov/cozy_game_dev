#pragma once

#include <memory>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class Effect
{
public:
    virtual ~Effect() = default;

    using Pointer = std::shared_ptr<Effect>;

    virtual void update(float time_delta) = 0;
    virtual void apply(sf::Sprite& sprite) = 0;
    virtual bool isFinished() const = 0;
    virtual Pointer clone() const = 0;
};