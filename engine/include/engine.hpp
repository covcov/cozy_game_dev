#pragma once

#include <memory>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "i_input_handler.hpp"
#include "render_textures_storage.hpp"
#include "sound_engine.hpp"
#include "fonts_storage.hpp"

class Scene;
class GridMap;
class PathSearchEngine;
class Camera;

namespace engine
{

void init(sf::RenderWindow& render_window, std::shared_ptr<Camera> camera, IInputHandler::Pointer input_handler);
std::shared_ptr<Scene> get_active_scene();
std::shared_ptr<GridMap> get_grid_map();
std::shared_ptr<PathSearchEngine> get_path_search_engine();
sf::RenderWindow& get_render_window();
sf::RenderTexture& get_render_texture();
RenderTexturesStorage& get_render_texture_storage();
SoundEngine& get_sound_engine();
FontsStorage& get_fonts_storage();
std::shared_ptr<Camera> get_camera();
IInputHandler::Pointer get_input_handler();

}