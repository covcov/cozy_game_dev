#pragma once

#include "game_object.hpp"

class GameObjectWithTooltip : public GameObject
{
public:
    GameObjectWithTooltip(std::string const& name, std::string const& tooltip_text)
        : GameObject(name)
        , m_tooltip_text(tooltip_text)
        , m_is_pointed(false)
    {}

    // Call inside update
    void processPointer();

protected:
    std::string m_tooltip_text;
    bool m_is_pointed;
    GameObject::Pointer m_tooltip;
};