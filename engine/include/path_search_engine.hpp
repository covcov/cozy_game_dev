#pragma once

#include <array>

#include "grid_map.hpp"

class PathSearchEngine
{
public:
    using Pointer = std::shared_ptr<PathSearchEngine>;

    PathSearchEngine(GridMap::Pointer grid_map);

    std::vector<sf::Vector2i> findPath(
        sf::Vector2i const& cell_from,
        sf::Vector2i const& cell_to,
        std::unordered_set<std::string> const& collision_mask);

    std::vector<sf::Vector2i> findPathToObjectWithTag(
        sf::Vector2i const& cell_from,
        std::unordered_set<std::string> const& collision_mask,
        std::string const& tag,
        int threshold_distance = 0);

    bool existPath(sf::Vector2i const& cell_from,
        sf::Vector2i const& cell_to,
        std::unordered_set<std::string> const& collision_mask);

private:
    void clearVisited();
    std::vector<sf::Vector2i> extractPath(sf::Vector2i const& start_cell, sf::Vector2i const& target_cell);
    void getAdjacent(sf::Vector2i const& cell, std::unordered_set<std::string> const& collision_mask);

private:
    GridMap::Pointer m_grid_map;

    utl::Matrix<sf::Vector2i> m_parents;
    std::vector<sf::Vector2i> m_visited_cells;
    std::vector<sf::Vector2i> m_adjacent;

    static sf::Vector2i const NOT_VISITED;
    static std::array<sf::Vector2i, 4> const SHIFTS;
};