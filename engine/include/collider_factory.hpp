#pragma once

#include "game_object.hpp"

struct ColliderFactory
{
    static void addBoxCollider(GameObject::Pointer game_object, sf::Vector2f const& size, sf::Vector2f shift = sf::Vector2f());
    static void addCircleCollider(GameObject::Pointer game_object, float radius, sf::Vector2f shift = sf::Vector2f());
};