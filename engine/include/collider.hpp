#pragma once

#include <memory>

#include "rect.hpp"
#include "position.hpp"

class Collider
{
public:
    using Pointer = std::shared_ptr<Collider>;

    enum class Type
    {
        BOX,
        CIRCLE,
    };

    Type getType() const { return m_type; }
    virtual utl::Rect<float> getAABB() const = 0;
    virtual Collider::Pointer clone(Position& where) const = 0;

protected:
    Collider(Position& position, sf::Vector2f const& shift, Type type)
        : m_position(position)
        , m_shift(shift)
        , m_type(type) {}

    sf::Vector2f m_shift;
    Position& m_position;
    Type m_type;
};