#pragma once

#include "collider.hpp"
#include "rect.hpp"
#include "math.hpp"

class BoxCollider : public Collider
{
public:
    utl::Rect<float> getShape() const
    {
        utl::Rect<float> rect{
            m_position.get().x + m_shift.x - m_size.x / 2,
            m_position.get().y + m_shift.y - m_size.y / 2,
            m_size.x - utl::DISTANCE_EPS,
            m_size.y - utl::DISTANCE_EPS };

        return rect;
    }

    virtual utl::Rect<float> getAABB() const override { return getShape(); }
    virtual Collider::Pointer clone(Position& where) const override
    {
        return std::shared_ptr<BoxCollider>(new BoxCollider(where, m_shift, m_size));
    }

private:
    friend struct ColliderFactory;

    BoxCollider(Position& position, sf::Vector2f const& shift, sf::Vector2f const& size)
        : Collider(position, shift, Collider::Type::BOX)
        , m_size(size)
    {}

    sf::Vector2f const m_size;
};