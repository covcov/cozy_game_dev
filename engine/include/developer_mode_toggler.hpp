#include "game_object.hpp"

class DeveloperModeToggler : public GameObject
{
public:
    DeveloperModeToggler(std::string const& name) : GameObject(name) {}
    void update(float time_delta) override;
    GameObject::Pointer clone() const override;
};