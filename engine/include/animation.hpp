#pragma once

#include <memory>
#include <vector>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class Animation
{
public:
    using Pointer = std::shared_ptr<Animation>;

    Animation(sf::Texture texture, float duration, bool cycled, std::vector<sf::IntRect>&& frames);
    virtual ~Animation() = default;

    void update(float);
    void reset();
    Animation::Pointer clone() const;

    bool isFinished() const { return m_finished; }
    sf::Sprite const& getSprite() const { return m_sprite; }
    sf::Vector2f getSize() const
    {
        return { static_cast<float>(m_frames[m_index].width), static_cast<float>(m_frames[m_index].height) };
    }

private:
    float m_duration;
    bool m_cycled;

    bool m_finished;
    float m_elapsed;
    size_t m_index;

    std::vector<sf::IntRect> m_frames;
    sf::Texture m_texture;
    sf::Sprite m_sprite;
};