#pragma once

#include "game_object.hpp"

class ExperienceObject : public GameObject
{
public:
    ExperienceObject(std::string name, sf::Vector2f const& position, int amount, float duration);

    virtual void update(float time_delta) override;
    virtual void draw(sf::RenderTarget& render_target) override;

private:
    void init(sf::Vector2f const& position);

private:
    float m_duration;
    float m_elapsed_time;
    int m_amount;
    sf::Sprite m_sprite;
};