#pragma once

#include "scene.hpp"
#include "creature.hpp"

class MovementHandler
{
public:
    using Pointer = std::unique_ptr<MovementHandler>;

    MovementHandler(Scene::Pointer scene, std::shared_ptr<Creature> creature);

    bool moveTowardsGridCell(float time_delta, sf::Vector2i const& target_cell);
    bool moveTowardsPosition(float time_delta, sf::Vector2f const& target_position);
    bool moveTowardsGridCell(float time_delta, float speed, sf::Vector2i const& target_cell);
    bool moveTowardsPosition(float time_delta, float speed, sf::Vector2f const& target_position);

private:
    std::shared_ptr<Scene> m_scene;
    std::shared_ptr<Creature> m_creature;
};