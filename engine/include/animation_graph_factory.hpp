#pragma once

#include <string>

#include "animation_graph.hpp"

class AnimationGraphFactory
{
public:
    static AnimationGraph create(
        std::string const& directory_name,
        std::string const& file_name,
        sf::Vector2f shift = {});

private:
    static sf::Texture getTexture(std::string const& path);

private:
    static std::unordered_map<std::string, sf::Texture> textures;
};