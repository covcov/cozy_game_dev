#pragma once

#include <unordered_map>
#include <string>

#include "game_object.hpp"

class Creature : public GameObject
{
public:
    Creature(std::string const& name) : GameObject(name) {}

    float getAttribute(std::string const& attribute_name) const;

    template <typename T>
    T getAttributeCasted(std::string const& attribute_name) const;

    void setAttribute(std::string const& attribute_name, int value);
    bool hasAttribute(std::string const& attribute_name) const;
    void registerAttribute(std::string const& stat_name, float initial_value);

    virtual void applyDamage(float damage) {}
    virtual void kill() {}

protected:
    std::unordered_map<std::string, float> m_attributes;
};

template <typename T>
T Creature::getAttributeCasted(std::string const& attribute_name) const
{
    return static_cast<T>(getAttribute(attribute_name));
}