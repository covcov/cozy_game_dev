#pragma once

#include <string>
#include <memory>
#include <unordered_set>
#include <iostream>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "animation_graph.hpp"
#include "collider.hpp"
#include "sound_engine.hpp"
#include "effects_storage.hpp"
#include "position.hpp"
#include "engine.hpp"

class Scene;
class GridMap;

class GameObject : public std::enable_shared_from_this<GameObject>
{
public:
    static std::string const DEFAULT_TAG;
    using Pointer = std::shared_ptr<GameObject>;

    GameObject(std::string const& name);
    virtual ~GameObject() = default;

    virtual void awake() {}
    virtual void onDestroy() {}
    virtual void getInformed(int message) {}
    virtual void update(float);
    virtual void destroy();
    virtual void draw(sf::RenderTarget& render_target);
    virtual GameObject::Pointer clone() const;
    virtual std::shared_ptr<sf::Sprite> getSprite();

    size_t getId() const { return m_id; }
    std::string const& getName() const { return m_name; }
    bool destroyOnCollision();

    template <typename T>
    std::shared_ptr<T> cast();

    // Use only when you are sure the type is correct
    template <typename T>
    std::shared_ptr<T> forceCast();
protected:
    void cloneComponents(GameObject::Pointer game_object) const;
    void drawSprite(sf::RenderTarget& render_target, sf::Sprite sprite);

public:
    bool is_active = true;
    std::string tag;
    Position position;
    AnimationGraph animation_graph;
    Collider::Pointer collider;
    std::unordered_set<std::string> collision_mask;
    std::unordered_set<std::string> destroy_on_collision_mask;
    EffectsStorage effects_storage;

protected:
    size_t m_id;
    std::string m_name;
};

template <typename T>
std::shared_ptr<T> GameObject::cast()
{
    return std::dynamic_pointer_cast<T>(shared_from_this());
}

// Use only when you are sure the type is correct
template <typename T>
std::shared_ptr<T> GameObject::forceCast()
{
    return std::static_pointer_cast<T>(shared_from_this());
}