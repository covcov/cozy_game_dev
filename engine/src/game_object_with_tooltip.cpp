#include "game_object_with_tooltip.hpp"

#include "scene.hpp"
#include "text_object.hpp"
#include "game_resources_storage.hpp"

void GameObjectWithTooltip::processPointer()
{
    auto scene = engine::get_active_scene();
    if (!is_active)
    {
        return;
    }

    auto input_handler = engine::get_input_handler();
    auto pointer_position = input_handler->getPointerPosition(
        engine::get_render_window(),
        IInputHandler::PointerPositionType::WORLD);

    auto aabb = collider->getAABB();

    if (!m_is_pointed && aabb.contains(pointer_position))
    {
        m_is_pointed = true;
        TextObject::Params params;
        params.scale = 2;
        params.background_color = sf::Color::Black;
        m_tooltip = GameObject::Pointer(new TextObject("tooltip", m_tooltip_text, params));

        m_tooltip->position = input_handler->getPointerPosition(
            engine::get_render_window(),
            IInputHandler::PointerPositionType::WINDOW);

        scene->add(m_tooltip, "ui");
    }
    else if (m_is_pointed && !aabb.contains(pointer_position))
    {
        m_is_pointed = false;
        m_tooltip->destroy();
    }
}
