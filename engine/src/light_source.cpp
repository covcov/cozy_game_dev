#include "light_source.hpp"

#include "light_processor.hpp"
#include "scene.hpp"

void LightSource::awake()
{
    auto scene = engine::get_active_scene();
    m_light_processor = scene->findObjectByName("light_processor");
}

void LightSource::update(float time_delta)
{
    animation_graph.update(time_delta);
    if (m_light_processor)
    {
        m_light_processor->forceCast<LightProcessor>()->addSource(shared_from_this(), m_radius);
    }
}

GameObject::Pointer LightSource::clone() const
{
    auto instance = std::make_shared<LightSource>(m_name, m_radius);
    cloneComponents(instance);
    return instance;
}
