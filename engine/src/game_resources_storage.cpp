#include "game_resources_storage.hpp"

bool GameResourcesStorage::is_initialized = false;
std::shared_ptr<GameResourcesStorage> GameResourcesStorage::single = nullptr;

std::shared_ptr<GameResourcesStorage> GameResourcesStorage::instance()
{
    if (is_initialized)
    {
        return single;
    }

    std::cout << "ERROR: Game Resources Storage is not initialized\n";
    return nullptr;
}

void GameResourcesStorage::init(sf::RenderWindow& render_window, Camera::Pointer camera, IInputHandler::Pointer input_handler)
{
    if (!is_initialized)
    {
        single = std::shared_ptr<GameResourcesStorage>(new GameResourcesStorage(render_window, camera, input_handler));
        is_initialized = true;
    }
    else
    {
        std::cout << "ERROR: Game Resources Storage is already initialized\n";
    }
}

GameResourcesStorage::GameResourcesStorage(sf::RenderWindow& render_window, Camera::Pointer camera, IInputHandler::Pointer input_handler)
    : m_render_window(render_window)
    , m_camera(camera)
    , m_input_handler(input_handler)
    , m_sound_engine()
    , m_render_textures_storage()
{
    m_render_texture.create(m_render_window.getSize().x, m_render_window.getSize().y, render_window.getSettings());
}
