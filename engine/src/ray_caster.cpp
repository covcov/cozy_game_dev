#include "ray_caster.hpp"

#include <chrono>

#include "circle_collider.hpp"
#include "debug.hpp"

namespace engine
{

RayCaster::Box::Box(sf::Vector2f const& tl, float segment_width, float segment_height)
    : segment_width(segment_width)
    , segment_height(segment_height)
    , top_left_position(tl)
{
    size_t const top = to_index(utl::DirectionType::TOP);
    size_t const right = to_index(utl::DirectionType::RIGHT);
    size_t const bottom = to_index(utl::DirectionType::BOTTOM);
    size_t const left = to_index(utl::DirectionType::LEFT);

    segments[top] = { tl, sf::Vector2f(tl.x + segment_width, tl.y) };
    segments[right] = { sf::Vector2f(tl.x + segment_width, tl.y), sf::Vector2f(tl.x + segment_width, tl.y + segment_height) };
    segments[bottom] = { sf::Vector2f(tl.x, tl.y + segment_height), sf::Vector2f(tl.x + segment_width, tl.y + segment_height) };
    segments[left] = { tl, sf::Vector2f(tl.x, tl.y + segment_height) };

    const auto to_line = [&](size_t index) { return utl::Line(segments[index].first, segments[index].second); };
    lines[top] = to_line(top);
    lines[right] = to_line(right);
    lines[bottom] = to_line(bottom);
    lines[left] = to_line(left);
}

sf::Vector2i RayCaster::Box::nextDirection(utl::Line const& ray, sf::Vector2f const& from, sf::Vector2f const& to) const
{
    if (utl::in_range_inclusive(to.x, top_left_position.x, top_left_position.x + segment_width) &&
        utl::in_range_inclusive(to.y, top_left_position.y, top_left_position.y + segment_height))
    {
        return {};
    }

    std::vector<std::pair<sf::Vector2i, sf::Vector2f>> candidates;
    for (size_t i = 0; i < utl::DIRECTINS_N; ++i)
    {
        auto const intersection = utl::find_intersection(ray, lines[i]);
        if (intersection.valid)
        {
            if (belongsToSegment(intersection.intersectionPoint, segments[i].first, segments[i].second))
            {
                candidates.emplace_back(utl::DIRECTIONS[i], intersection.intersectionPoint);
            }
        }
    }

    auto unique_predicate = [](
        std::pair<sf::Vector2i, sf::Vector2f> const& x,
        std::pair<sf::Vector2i, sf::Vector2f> const& y)
    {
        return utl::almost_zero(utl::distance(x.second, y.second));
    };

    if (candidates.empty())
    {
        return {};
    }

    auto it = std::unique(candidates.begin(), candidates.end(), unique_predicate);
    candidates.erase(it, candidates.end());

    auto comp = [&](
        std::pair<sf::Vector2i, sf::Vector2f> const& a,
        std::pair<sf::Vector2i, sf::Vector2f> const& b)
    {
        return utl::distance(a.second, to) < utl::distance(b.second, to);
    };

    auto const candidate = *std::min_element(candidates.begin(), candidates.end(), comp);
    if (isCorner(candidate.second))
    {
        auto const direction = to - from;
        auto const dx = static_cast<int>(std::round(direction.x / std::fabs(direction.x)));
        auto const dy = static_cast<int>(std::round(direction.y / std::fabs(direction.y)));
        return { dx, dy };
    }
    else
    {
        return candidate.first;
    }
}

utl::Intersection RayCaster::Box::collisionPoint(utl::Line const& ray, sf::Vector2f const& from, sf::Vector2f const& to) const
{
    std::vector<sf::Vector2f> intersections;
    for (size_t i = 0; i < utl::DIRECTINS_N; ++i)
    {
        auto const intersection = utl::find_intersection(ray, lines[i]);
        if (intersection.valid)
        {
            if (belongsToSegment(intersection.intersectionPoint, segments[i].first, segments[i].second))
            {
                intersections.emplace_back(intersection.intersectionPoint);
            }
        }
    }

    auto comp = [&](sf::Vector2f const& a, sf::Vector2f const& b)
    {
        return utl::distance(a, from) < utl::distance(b, from);
    };

    if (!intersections.empty())
    {
        
        return { true, *std::min_element(intersections.begin(), intersections.end(), comp) };
    }

    return { false, {} };
}

bool RayCaster::Box::isCorner(sf::Vector2f const& point) const
{
    sf::Vector2f const tl(top_left_position);
    sf::Vector2f const tr(top_left_position.x + segment_width, top_left_position.y);
    sf::Vector2f const br(top_left_position.x + segment_width, top_left_position.y + segment_height);
    sf::Vector2f const bl(top_left_position.x, top_left_position.y + segment_height);

    for (auto const& p : { tl, tr, br, bl })
    {
        if (utl::distance(p, point) < utl::DISTANCE_EPS)
        {
            return true;
        }
    }

    return false;
}

std::pair<sf::Vector2f, GameObject::Pointer> RayCaster::cast(
    sf::Vector2f const& from,
    sf::Vector2f const& to,
    std::unordered_set<std::string> const& collision_mask,
    std::vector<GameObject::Pointer> const& ignore_objects)
{
    auto grid_map = engine::get_grid_map();
    if (!grid_map) return {};

    utl::Line const ray(from, to);
    auto const cell_size = grid_map->getCellSize();
    auto const grid_width = grid_map->getGridSize().x;
    auto grid_cell = grid_map->worldToCellPosition(from);
    auto get_cell_index = [&](sf::Vector2i const& grid_cell)
    {
        return grid_cell.y * grid_width + grid_cell.x;
    };

    std::unordered_set<size_t> ids;
    std::unordered_set<size_t> visited;
    for (auto const game_object : ignore_objects)
    {
        ids.insert(game_object->getId());
    }

    std::vector<GameObject::Pointer> game_objects;

    auto const target_cell = grid_map->worldToCellPosition(to);
    while (true)
    {
        if (!grid_map->contains(grid_cell))
        {
            break;
        }

        size_t const cell_index = get_cell_index(grid_cell);
        if (visited.find(cell_index) != visited.end())
        {
            break;
        }

        visited.insert(cell_index);

        for (auto const& game_object : grid_map->getCellObjects(grid_cell))
        {
            if (ids.find(game_object->getId()) == ids.end() &&
                collision_mask.find(game_object->tag) != collision_mask.end())
            {
                game_objects.push_back(game_object);
                ids.insert(game_object->getId());
            }
        }

        Box const box(grid_map->getCellTopLeftPosition(grid_cell), cell_size, cell_size);
        auto const direction = box.nextDirection(ray, from, to);

        if (direction.x == 0 && direction.y == 0)
        {
            break;
        }

        grid_cell += direction;
    }

    std::vector<std::pair<sf::Vector2f, GameObject::Pointer>> hits;
    for (auto game_object : game_objects)
    {
        auto const intersection = findIntersection(ray, from, to, game_object);
        if (intersection.valid)
        {
            hits.emplace_back(intersection.intersectionPoint, game_object);
        }
    }

    if (hits.empty())
    {
        return { {}, nullptr };
    }

    auto comp = [&](
        std::pair<sf::Vector2f, GameObject::Pointer> const& a,
        std::pair<sf::Vector2f, GameObject::Pointer> const& b)
    {
        return utl::distance(from, a.first) < utl::distance(from, b.first);
    };

    return *std::min_element(hits.begin(), hits.end(), comp);
}

utl::Intersection RayCaster::findIntersection(
    utl::Line const& ray,
    sf::Vector2f const& from,
    sf::Vector2f const& to,
    GameObject::Pointer game_object)
{
    utl::Intersection const WRONG_INTERSECTION = { false, {} };

    if (game_object->collider->getType() == Collider::Type::BOX)
    {
        auto const& aabb = game_object->collider->getAABB();
        Box const box(aabb.getTopLeft(), aabb.width, aabb.height);
        return box.collisionPoint(ray, from, to);
    }
    else if (game_object->collider->getType() == Collider::Type::CIRCLE)
    {
        auto const circle_collider = std::static_pointer_cast<CircleCollider>(game_object->collider);
        auto const x0 = circle_collider->getShape().position.x;
        auto const y0 = circle_collider->getShape().position.y;
        auto const r = circle_collider->getShape().radius;

        sf::Vector2f p1;
        sf::Vector2f p2;
        if (std::fabs(ray.a) > utl::DISTANCE_EPS)
        {
            auto const e = -ray.b / ray.a;
            auto const f = -ray.c / ray.a;
            auto const g = x0 - f;
            auto const a = 1.f + e * e;
            auto const b = -2.f * (g * e + y0);
            auto const c = g * g + y0 * y0 - r * r;
            auto const d = b * b - 4.f * a * c;
            if (d >= 0.f)
            {
                auto const left = -b / 2.f / a;
                auto const right = std::sqrt(d) / 2.f / a;
                auto const y1 = left - right;
                auto const y2 = left + right;
                auto const x1 = e * y1 + f;
                auto const x2 = e * y2 + f;
                p1 = sf::Vector2f(x1, y1);
                p2 = sf::Vector2f(x2, y2);
            }
            else
            {
                return WRONG_INTERSECTION;
            }
        }
        else
        {
            auto const d = r * r - utl::sqr(ray.c / ray.b + y0);
            if (d >= 0.f)
            {
                auto const y1 = -ray.c / ray.b;
                auto const y2 = y1;
                auto const x1 = x0 + std::sqrt(d);
                auto const x2 = x0 - std::sqrt(d);
                p1 = sf::Vector2f(x1, y1);
                p2 = sf::Vector2f(x2, y2);
            }
            else
            {
                return WRONG_INTERSECTION;
            }
        }

        if (utl::distance(from, p1) >= utl::distance(from, p2))
        {
            std::swap(p1, p2);
        }

        for (auto p : { p1, p2 })
        {
            if (belongsToSegment(p, from, to))
            {
                return { true, p };
            }
        }
    }

    return WRONG_INTERSECTION;
}

bool RayCaster::belongsToSegment(sf::Vector2f const& p, sf::Vector2f const& sp_1, sf::Vector2f const& sp_2)
{
    auto const d_1 = utl::distance(sp_1, p);
    auto const d_2 = utl::distance(sp_2, p);
    auto const d = utl::distance(sp_1, sp_2);
    auto const delta = d_1 + d_2 - d;
    return std::fabs(delta) < utl::DISTANCE_EPS;
}

}
