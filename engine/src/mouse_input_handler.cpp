#include "mouse_input_handler.hpp"

#include <iostream>

void MouseInputHandler::update()
{
    for (auto const& p : m_button_binds)
    {
        sf::Mouse::Button const& button = p.second;

        m_is_button_pressed_in_update[button] = false;
        m_is_button_released_in_update[button] = false;

        if (!m_is_button_pressed[button] && sf::Mouse::isButtonPressed(button))
        {
            m_is_button_pressed[button] = true;
            m_is_button_pressed_in_update[button] = true;
        }
        else if (m_is_button_pressed[button] && !sf::Mouse::isButtonPressed(button))
        {
            m_is_button_pressed[button] = false;
            m_is_button_released_in_update[button] = true;
        }
    }
}

bool MouseInputHandler::isPressed(std::string const& name)
{
    auto range = m_button_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = sf::Mouse::isButtonPressed(it->second);
        result |= r;
    }

    return result;
}

bool MouseInputHandler::isReleased(std::string const& name)
{
    return !isPressed(name);
}

bool MouseInputHandler::isPressedInUpdate(std::string const& name)
{
    auto range = m_button_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = m_is_button_pressed_in_update[it->second];
        m_is_button_pressed_in_update[it->second] = false;
        result |= r;
    }

    return result;
}

bool MouseInputHandler::isReleasedInUpdate(std::string const& name)
{
    auto range = m_button_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = m_is_button_released_in_update[it->second];
        m_is_button_released_in_update[it->second] = false;
        result |= r;
    }

    return result;
}

sf::Vector2f MouseInputHandler::getPointerPosition(sf::RenderWindow& render_window, PointerPositionType type)
{
    if (type == PointerPositionType::WORLD)
    {
        return render_window.mapPixelToCoords(sf::Mouse::getPosition(render_window));
    }
    else
    {
        return static_cast<sf::Vector2f>(sf::Mouse::getPosition(render_window));
    }
}

void MouseInputHandler::bind(std::string const& name, sf::Mouse::Button button)
{
    auto it = m_button_binds.find(name);
    if (it == m_button_binds.end())
    {
        m_button_binds.insert(std::make_pair(name, button));
        m_is_button_pressed[button] = false;
    }
}
