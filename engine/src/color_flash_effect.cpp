#include "color_flash_effect.hpp"

#include "game_resources_storage.hpp"

namespace
{

std::shared_ptr<sf::Shader> create_flash_shader(sf::Glsl::Vec4 const& color)
{
    std::string shader_code =
        "uniform sampler2D texture;"
        "uniform vec4 flash_color;"
        "void main()"
        "{"
        "    vec4 pixel_color = texture2D(texture, gl_TexCoord[0].xy);"
        "    float percent = flash_color.a;"
        "    vec4 color_difference = vec4(0, 0, 0, 1);"
        "    if (pixel_color.r != 0.f && pixel_color.g != 0.f && pixel_color.b != 0.f)"
        "    {"
        "        color_difference.r = flash_color.r - pixel_color.r;"
        "        color_difference.g = flash_color.g - pixel_color.g;"
        "        color_difference.b = flash_color.b - pixel_color.b;"
        "        pixel_color.r = pixel_color.r + color_difference.r * percent;"
        "        pixel_color.g = pixel_color.g + color_difference.g * percent;"
        "        pixel_color.b = pixel_color.b + color_difference.b * percent;"
        "    }"
        "    gl_FragColor = pixel_color;"
        "}";

    std::shared_ptr<sf::Shader> shader(new sf::Shader());
    shader->loadFromMemory(shader_code, sf::Shader::Fragment);
    shader->setUniform("flash_color", color);
    return shader;
}

}

ColorFlashEffect::ColorFlashEffect(float duration, sf::Color const& color)
    : m_duration(duration)
    , m_time_elapsed(0)
    , m_color(color)
{
    sf::Glsl::Vec4 shader_color(color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
    m_shader = create_flash_shader(shader_color);
}

ColorFlashEffect::ColorFlashEffect(float duration, sf::Color const& color, std::shared_ptr<sf::Shader> shader)
    : m_duration(duration)
    , m_time_elapsed(0)
    , m_color(color)
    , m_shader(shader)
{}

void ColorFlashEffect::update(float time_delta)
{
    m_time_elapsed += time_delta;
}

void ColorFlashEffect::apply(sf::Sprite& sprite)
{
    const auto texture_size = static_cast<sf::Vector2i>(sprite.getTexture()->getSize());
    auto render_texture = engine::get_render_texture_storage().get(texture_size);
    render_texture->clear(sf::Color::Transparent);
    render_texture->draw(sprite, m_shader.get());
    render_texture->display();
    sprite = sf::Sprite(render_texture->getTexture());
}

bool ColorFlashEffect::isFinished() const
{
    if (m_duration < 0.f)
    {
        return false;
    }

    return m_time_elapsed >= m_duration;
}

Effect::Pointer ColorFlashEffect::clone() const
{
    return std::make_shared<ColorFlashEffect>(m_duration, m_color, m_shader);
}
