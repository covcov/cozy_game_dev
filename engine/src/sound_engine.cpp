#include "sound_engine.hpp"
#include "debug.hpp"

SoundEngine::SoundEngine()
    : m_is_muted(false)
{
    m_clock.restart();
}

void SoundEngine::update()
{
    while (!m_sound_pqueue.empty())
    {
        auto const& sound_descriptor = m_sound_pqueue.top();
        const size_t id = sound_descriptor.id;
        if (sound_descriptor.sound->getStatus() == sf::Sound::Playing)
        {
            break;
        }
        else
        {
            m_sound_pqueue.pop();
            m_sounds.erase(id);
        }
    }

    if (m_sound_pqueue.empty())
    {
        m_clock.restart();
    }

    m_added_index_to_id.clear();
}

void SoundEngine::add(std::string const& name, std::string const& directory_name, std::string const& file_name)
{
    sf::SoundBuffer sound_buffer;
    sound_buffer.loadFromFile(directory_name + file_name);
    add(name, sound_buffer);
}

void SoundEngine::add(std::string const& name, sf::SoundBuffer buffer)
{
    m_name_to_index_table[name] = m_buffers.size();
    m_buffers.emplace_back(new sf::SoundBuffer(buffer));
}

void SoundEngine::play(
    std::string const& name,
    int volume,
    bool is_looped,
    float pitch)
{
    static size_t id = 0;
    auto it = m_name_to_index_table.find(name);
    if (it != m_name_to_index_table.end())
    {
        size_t index = it->second;
        auto it = m_added_index_to_id.find(index);
        if (it == m_added_index_to_id.end())
        {
            m_added_index_to_id.insert(std::make_pair(index, id));
            std::shared_ptr<sf::Sound> sound(new sf::Sound(*m_buffers[index]));

            if (is_looped)
            {
                m_loops.push_back(sound);
                m_loops.back()->setLoop(true);
            }
            else
            {
                float const time = m_clock.getElapsedTime().asSeconds();
                float const end_time = time + m_buffers[index]->getDuration().asSeconds() / pitch;
                m_sound_pqueue.emplace(sound, end_time, id);
            }

            m_sounds[id++] = std::make_pair(sound, volume);
            sound->setVolume(m_is_muted ? 0 : volume);
            sound->setPitch(pitch);
            sound->play();
        }
        else
        {
            int const updatedVolume = std::max(volume, m_sounds[it->second].second);
            m_sounds[it->second].first->setVolume(m_is_muted ? 0 : updatedVolume);
            m_sounds[it->second].second = updatedVolume;
        }
    }
}

void SoundEngine::toggleMute()
{
    for (auto& it : m_sounds)
    {
        std::shared_ptr<sf::Sound> sound;
        int volume;
        std::tie(sound, volume) = it.second;
        sound->setVolume(m_is_muted ? volume : 0);
    }

    m_is_muted = !m_is_muted;
}
