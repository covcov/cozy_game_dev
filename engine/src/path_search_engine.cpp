#include "path_search_engine.hpp"

#include <queue>

#include "debug.hpp"

sf::Vector2i const PathSearchEngine::NOT_VISITED = { -1, -1 };

std::array<sf::Vector2i, 4> const PathSearchEngine::SHIFTS = { {
    { -1,  0  },
    {  0, -1  },
    {  1,  0  },
    {  0,  1  } } };

PathSearchEngine::PathSearchEngine(GridMap::Pointer grid_map)
    : m_grid_map(grid_map)
    , m_parents(grid_map->getGridSize().x, grid_map->getGridSize().y)
{
    auto const& grid_size = grid_map->getGridSize();
    for (size_t y = 0; y < grid_size.y; ++y)
    {
        for (size_t x = 0; x < grid_size.x; ++x)
        {
            m_parents(x, y) = NOT_VISITED;
        }
    }
}

std::vector<sf::Vector2i> PathSearchEngine::findPath(
    sf::Vector2i const& start_cell,
    sf::Vector2i const& target_cell,
    std::unordered_set<std::string> const& collision_mask)
{
    std::queue<sf::Vector2i> cells_queue;
    cells_queue.push(start_cell);
    m_parents(start_cell.x, start_cell.y) = start_cell;
    m_visited_cells.push_back(start_cell);

    bool is_found = false;
    while (!cells_queue.empty())
    {
        auto cell = cells_queue.front();
        cells_queue.pop();

        if (cell == target_cell)
        {
            is_found = true;
            break;
        }

        getAdjacent(cell, collision_mask);
        for (auto const& adjacent : m_adjacent)
        {
            if (m_parents(adjacent.x, adjacent.y) == NOT_VISITED)
            {
                cells_queue.push(adjacent);
                m_parents(adjacent.x, adjacent.y) = cell;
                m_visited_cells.push_back(adjacent);
            }
        }
    }

    auto path = is_found ? extractPath(start_cell, target_cell) : std::vector<sf::Vector2i>();
    clearVisited();
    return path;
}

std::vector<sf::Vector2i> PathSearchEngine::findPathToObjectWithTag(
    sf::Vector2i const& start_cell,
    std::unordered_set<std::string> const& collision_mask,
    std::string const& tag,
    int threshold_distance)
{
    std::queue<sf::Vector2i> cells_queue;
    cells_queue.push(start_cell);
    m_parents(start_cell.x, start_cell.y) = start_cell;
    m_visited_cells.push_back(start_cell);

    bool is_found = false;
    sf::Vector2i target_cell;
    while (!cells_queue.empty())
    {
        auto cell = cells_queue.front();
        cells_queue.pop();

        if (m_grid_map->cellContainsTag(cell, tag))
        {
            target_cell = cell;
            is_found = true;
            break;
        }

        getAdjacent(cell, collision_mask);
        for (auto const& adjacent : m_adjacent)
        {
            if (m_parents(adjacent.x, adjacent.y) == NOT_VISITED &&
                (utl::norm_l1(cell, start_cell) < threshold_distance || threshold_distance == 0))
            {
                cells_queue.push(adjacent);
                m_parents(adjacent.x, adjacent.y) = cell;
                m_visited_cells.push_back(adjacent);
            }
        }
    }

    auto path = is_found ? extractPath(start_cell, target_cell) : std::vector<sf::Vector2i>();
    clearVisited();
    return path;
}

bool PathSearchEngine::existPath(
    sf::Vector2i const& cell_from,
    sf::Vector2i const& cell_to,
    std::unordered_set<std::string> const& collision_mask)
{
    auto const path = findPath(cell_from, cell_to, collision_mask);
    return !path.empty();
}

void PathSearchEngine::clearVisited()
{
    for (auto const& cell : m_visited_cells)
    {
        m_parents(cell.x, cell.y) = NOT_VISITED;
    }

    m_visited_cells.clear();
}

std::vector<sf::Vector2i> PathSearchEngine::extractPath(sf::Vector2i const& start_cell, sf::Vector2i const& target_cell)
{
    std::vector<sf::Vector2i> path;
    for (auto cell = target_cell; cell != start_cell;)
    {
        path.push_back(cell);
        cell = m_parents(cell.x, cell.y);
    }

    return path;
}

void PathSearchEngine::getAdjacent(sf::Vector2i const& cell, std::unordered_set<std::string> const& collision_mask)
{
    m_adjacent.clear();
    for (auto const& shift : SHIFTS)
    {
        auto const adjacent_cell = cell + shift;
        if (m_grid_map->contains(adjacent_cell) &&
            !m_grid_map->cellContainsAnyOfTags(adjacent_cell, collision_mask))
        {
            m_adjacent.push_back(adjacent_cell);
        }
    }
}