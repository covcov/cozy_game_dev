#include "ui_object.hpp"

#include "scene_manager.hpp"
#include "game_resources_storage.hpp"

void UIObject::update(float time_delta)
{
    auto const target_position = engine::get_camera()->getTargetPosition();
    position = target_position;
    animation_graph.update(time_delta);
}

GameObject::Pointer UIObject::clone() const
{
    GameObject::Pointer instance = std::make_shared<UIObject>(m_name);
    cloneComponents(instance);
    return instance;
}