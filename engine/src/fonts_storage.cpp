#include "fonts_storage.hpp"

void FontsStorage::add(std::string const& name, std::string const& path, int character_size)
{
    std::shared_ptr<sf::Font> font(new sf::Font());
    font->loadFromFile(path);
    m_fonts[name] = font;
    m_sizes[name] = character_size;
}

std::shared_ptr<sf::Font> FontsStorage::getFont(std::string const& name)
{
    auto it = m_fonts.find(name);
    if (it != m_fonts.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}

int FontsStorage::getSize(std::string const& name)
{
    auto it = m_sizes.find(name);
    if (it != m_sizes.end())
    {
        return it->second;
    }
    else
    {
        return 0;
    }
}
