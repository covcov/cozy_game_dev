#include "scene.hpp"

#include "layer_map.hpp"
#include "debug.hpp"
#include "scene_manager.hpp"
#include "game_resources_storage.hpp"

Scene::Pointer Scene::create(
    size_t width,
    size_t height,
    float cell_size,
    sf::Vector2f top_left_position)
{
    auto scene = Pointer(new Scene());
    scene->m_grid_map = std::make_shared<GridMap>(width, height, cell_size, top_left_position);
    scene->m_path_search_engine = std::make_shared<PathSearchEngine>(scene->m_grid_map);
    return scene;
}

Scene::Pointer Scene::clone()
{
    Scene::Pointer scene_cloned = create(
        m_grid_map->getGridSize().x,
        m_grid_map->getGridSize().y,
        m_grid_map->getCellSize(),
        m_grid_map->getTopLeftPosition());

    for (auto game_object : m_game_objects)
    {
        auto const layer_idx = m_id_to_layer[game_object->getId()];
        auto game_object_cloned = game_object->clone();
        game_object_cloned->position = game_object->position;
        scene_cloned->add(game_object_cloned, LayerMap::instance().getLayer(layer_idx));
    }

    return scene_cloned;
}

Scene::Scene() : m_time_scale(1.f)
{}

void Scene::lateAdd(GameObject::Pointer game_object, std::string const& layer)
{
    if (!game_object)
    {
        return;
    }

    m_add_storage.emplace_back(game_object, layer);
}

void Scene::lateAdd(GameObject::Pointer game_object, std::string const& layer, sf::Vector2i const& cell)
{
    if (!game_object)
    {
        return;
    }

    if (m_grid_map->contains(cell))
    {
        game_object->position = m_grid_map->cellToWorldPosition(cell);
        lateAdd(game_object, layer);
    }
}

void Scene::update(float time_delta)
{
    m_is_updating = true;
    time_delta *= m_time_scale;
    for (auto& game_object : m_game_objects)
    {
        auto it = m_tag_to_message_table.find(game_object->tag);
        if (it != m_tag_to_message_table.end())
        {
            auto const& messages = it->second;
            for (auto const& message : messages)
            {
                if (game_object->is_active)
                {
                    game_object->getInformed(message);
                }
            }
        }
    }

    m_tag_to_message_table.clear();
    for (auto& game_object : m_game_objects)
    {
        if (game_object->is_active && !game_object->destroyOnCollision())
        {
            game_object->update(time_delta);
            game_object->effects_storage.update(time_delta);
        }
    }

    m_is_updating = false;
    destroyObjects();
    addObjects();
}

void Scene::draw()
{
    auto& render_texture = engine::get_render_texture();
    auto& render_window = engine::get_render_window();

    size_t const layers_n = LayerMap::instance().getLayersNumber();
    if (layers_n == 0)
    {
        return;
    }

    std::vector<std::vector<GameObject::Pointer>> layers(layers_n);
    for (auto& game_object : m_game_objects)
    {
        auto const id = game_object->getId();
        auto const& layer_index = m_id_to_layer[id];
        if (game_object->is_active)
        {
            layers[layer_index].push_back(game_object);
        }
    }

    auto const comp = [](GameObject::Pointer a, GameObject::Pointer b) { return a->position.get().y < b->position.get().y; };
    std::sort(layers.front().begin(), layers.front().end(), comp);
    for (auto& game_object : layers.front())
    {
        game_object->draw(render_texture);
    }

    render_texture.display();
    sf::Sprite sprite(render_texture.getTexture());
    render_window.draw(sprite);

    for (size_t i = 1; i < layers_n; ++i)
    {
        if (!layers[i].empty())
        {
            std::sort(layers[i].begin(), layers[i].end(), comp);
            auto const view = render_window.getView();
            auto const default_view = render_window.getDefaultView();
            auto const& layer_name = LayerMap::instance().getLayer(i);
            if (layer_name == "ui")
            {
                render_window.setView(default_view);
            }

            for (auto& game_object : layers[i])
            {
                game_object->draw(render_window);
            }

            if (layer_name == "ui")
            {
                render_window.setView(view);
            }
        }
    }

    auto debug_texture = engine::get_render_texture_storage().get(static_cast<sf::Vector2i>(render_window.getSize()));
    Debug::draw(debug_texture);
    debug_texture->display();
    sf::Sprite debug_sprite(debug_texture->getTexture());
    render_window.draw(debug_sprite);

    engine::get_render_texture_storage().reset();
}

void Scene::inform(std::string const& tag, int message)
{
    m_tag_to_message_table[tag].push_back(message);
}

GameObject::Pointer Scene::findObjectById(size_t id)
{
    auto it = m_id_to_index.find(id);
    if (it != m_id_to_index.end())
    {
        auto const& index = it->second;
        return m_game_objects[index];
    }

    return nullptr;
}

std::vector<GameObject::Pointer> Scene::findObjectsByName(std::string const& name)
{
    auto range = m_name_to_id.equal_range(name);
    std::vector<GameObject::Pointer> objects;
    for (auto it = range.first; it != range.second; ++it)
    {
        objects.push_back(findObjectById(it->second));
    }

    return objects;
}

GameObject::Pointer Scene::findObjectByName(std::string const& name)
{
    auto range = m_name_to_id.equal_range(name);
    if (range.first != m_name_to_id.end())
    {
        return findObjectById(range.first->second);
    }

    return nullptr;
}

std::vector<GameObject::Pointer> Scene::findObjectsByTag(std::string const& tag)
{
    std::vector<GameObject::Pointer> objects;
    for (auto& game_object : m_game_objects)
    {
        if (game_object->tag == tag)
        {
            objects.push_back(game_object);
        }
    }

    return objects;
}

void Scene::lateDestroy(GameObject::Pointer game_object)
{
    if (!game_object)
    {
        return;
    }

    m_destroy_storage.push_back(game_object->getId());
}

void Scene::destroyObjects()
{
    for (auto id : m_destroy_storage)
    {
        destroyById(id);
    }

    m_destroy_storage.clear();
}

void Scene::addObjects()
{
    for (auto& p : m_add_storage)
    {
        add(p.first, p.second);
        p.first->awake();
    }

    m_add_storage.clear();
}

void Scene::add(GameObject::Pointer game_object, std::string const& layer, sf::Vector2i const& cell)
{
    if (!game_object)
    {
        return;
    }

    game_object->position = m_grid_map->cellToWorldPosition(cell);
    add(game_object, layer);
}

void Scene::awakeAll()
{
    m_is_updating = true;
    for (auto& game_object : m_game_objects)
    {
        game_object->awake();
    }

    m_is_updating = false;
}

void Scene::add(GameObject::Pointer game_object, std::string const& layer)
{
    if (!game_object)
    {
        return;
    }

    if (m_is_updating)
    {
        lateAdd(game_object, layer);
    }
    else
    {
        auto layer_index = LayerMap::instance().getIndex(layer);
        if (layer_index != LayerMap::WRONG_IDX)
        {
            auto it = m_id_to_index.find(game_object->getId());
            if (it == m_id_to_index.end())
            {
                size_t idx = m_game_objects.size();
                m_game_objects.push_back(game_object);
                m_id_to_layer[game_object->getId()] = layer_index;
                m_id_to_index[game_object->getId()] = idx;
                m_name_to_id.insert(std::make_pair(game_object->getName(), game_object->getId()));
            }

            m_grid_map->add(game_object);
            game_object->position.setTarget(game_object);
        }
    }
}

void Scene::destroy(GameObject::Pointer game_object)
{
    if (!game_object)
    {
        return;
    }
    if (m_is_updating)
    {
        lateDestroy(game_object);
    }
    else
    {
        destroyById(game_object->getId());
    }
}

void Scene::destroyById(size_t id)
{
    auto it = m_id_to_index.find(id);
    if (it == m_id_to_index.end())
    {
        return;
    }

    auto const index = it->second;

    auto game_object = findObjectById(id);
    auto range = m_name_to_id.equal_range(game_object->getName());

    for (auto it = range.first; it != range.second; ++it)
    {
        if (it->second == id)
        {
            m_name_to_id.erase(it);
            break;
        }
    }

    m_game_objects[index]->is_active = false;
}