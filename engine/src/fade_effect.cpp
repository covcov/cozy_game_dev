#include "fade_effect.hpp"

#include "math.hpp"

FadeEffect::FadeEffect(float duration, bool is_fade_out)
    : m_duration(duration)
    , m_time_elapsed(0)
    , m_transparency(0)
    , m_is_fade_out(is_fade_out)
{}

void FadeEffect::update(float time_delta)
{
    m_time_elapsed = std::min(m_time_elapsed + time_delta, m_duration);
    m_transparency = m_is_fade_out ? (m_duration - m_time_elapsed) / m_duration : m_time_elapsed / m_duration;
}

void FadeEffect::apply(sf::Sprite& sprite)
{
    sprite.setColor({ 255, 255, 255, static_cast<sf::Uint8>(255.f * m_transparency) });
}

bool FadeEffect::isFinished() const
{
    return std::fabs(m_time_elapsed - m_duration) < std::numeric_limits<float>::epsilon() * 10;
}

Effect::Pointer FadeEffect::clone() const
{
    return Effect::Pointer(new FadeEffect(m_duration, m_is_fade_out));
}
