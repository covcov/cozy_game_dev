#include "animation.hpp"
#include "debug.hpp"

Animation::Animation(sf::Texture texture, float duration, bool cycled, std::vector<sf::IntRect>&& frames)
    : m_duration(duration)
    , m_cycled(cycled)
    , m_frames(std::move(frames))
    , m_finished(false)
    , m_elapsed(0.f)
    , m_index(0)
    , m_texture(texture)
{
    m_sprite.setTexture(m_texture);
    m_sprite.setTextureRect(m_frames.front());
}

void Animation::update(float delta)
{
    m_elapsed += delta;
    if (m_elapsed > m_duration)
    {
        m_finished = ++m_index == m_frames.size();
        if (m_finished)
        {
            m_index = m_cycled ? 0 : m_frames.size() - 1;
        }

        m_sprite.setTextureRect(m_frames[m_index]);
        m_elapsed = 0.f;
    }
}

void Animation::reset()
{
    m_finished = false;
    m_elapsed = 0.f;
    m_index = 0;
    m_sprite.setTextureRect(m_frames.front());
}

Animation::Pointer Animation::clone() const
{
    std::vector<sf::IntRect> frames = m_frames;
    return std::make_shared<Animation>(m_texture, m_duration, m_cycled, std::move(frames));
}
