#include "attribute_modifier.hpp"
#include "creature.hpp"

void AttributeModifier::awake()
{
    std::shared_ptr<Creature> object = std::dynamic_pointer_cast<Creature>(m_target);
    if (object)
    {
        object->setAttribute(m_attribute_name, object->getAttribute(m_attribute_name) + m_value);
    }
}

void AttributeModifier::update(float time_delta)
{
    if (m_duration > 0.f)
    {
        m_elapsed_time += time_delta;
        std::shared_ptr<Creature> object = std::dynamic_pointer_cast<Creature>(m_target);
        if (object)
        {
            if (m_elapsed_time > m_duration)
            {
                object->setAttribute(m_attribute_name, object->getAttribute(m_attribute_name) - m_value);
                destroy();
            }
        }
    }
}