#include "game_object.hpp"
#include "scene.hpp"
#include "scene_manager.hpp"
#include "grid_map.hpp"
#include "debug.hpp"

std::string const GameObject::DEFAULT_TAG = "default";

GameObject::GameObject(std::string const& name)
    : m_name(name)
    , tag(DEFAULT_TAG)
    , is_active(true)
    , position{}
{
    static size_t id = 0;
    m_id = id++;
}

void GameObject::update(float time_delta)
{
    auto animation = animation_graph.getActive();
    if (animation)
    {
        animation->update(time_delta);
    }
}

void GameObject::destroy()
{
    onDestroy();

    auto scene = engine::get_active_scene();
    scene->destroy(shared_from_this());
}

bool GameObject::destroyOnCollision()
{
    auto grid_map = engine::get_grid_map();
    const auto collided_objects = grid_map->getCollidedObjects(shared_from_this(), destroy_on_collision_mask);
    if (!collided_objects.empty())
    {
        destroy();
        return true;
    }

    return false;
}

GameObject::Pointer GameObject::clone() const
{
    GameObject::Pointer instance = std::make_shared<GameObject>(m_name);
    cloneComponents(instance);
    return instance;
}

 void GameObject::draw(sf::RenderTarget& render_target)
 {
    auto const& shift = animation_graph.getShift();
    auto animation = animation_graph.getActive();
    if (animation)
    {
        sf::Sprite sprite = animation->getSprite();
        sf::Vector2f const size = animation->getSize();
        effects_storage.apply(sprite);
        sprite.setPosition(position + shift - 0.5f * size);
        render_target.draw(sprite);
    }

    if (collider)
    {
        Debug::draw(render_target, collider);
    }
 }

std::shared_ptr<sf::Sprite> GameObject::getSprite()
{
    auto animation = animation_graph.getActive();
    if (animation)
    {
        return std::make_shared<sf::Sprite>(animation_graph.getActive()->getSprite());
    }

    return nullptr;
}

void GameObject::cloneComponents(GameObject::Pointer game_object) const
{
    game_object->tag = tag;
    game_object->position = position;
    game_object->animation_graph = animation_graph.clone();
    game_object->collision_mask = collision_mask;
    game_object->destroy_on_collision_mask = destroy_on_collision_mask;

    if (collider)
    {
        game_object->collider = collider->clone(game_object->position);
    }
}

void GameObject::drawSprite(sf::RenderTarget& render_target, sf::Sprite sprite)
{
    auto const texture = sprite.getTexture();
    if (texture)
    {
        auto const size = static_cast<sf::Vector2f>(texture->getSize());
        effects_storage.apply(sprite);
        sprite.setPosition(position - 0.5f * size);
        render_target.draw(sprite);
    }

    if (collider)
    {
        Debug::draw(render_target, collider);
    }
}
