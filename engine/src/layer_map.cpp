#include "debug.hpp"
#include "layer_map.hpp"

LayerMap& LayerMap::instance()
{
    static LayerMap layer_map;
    return layer_map;
}

size_t LayerMap::getIndex(std::string const& layer) const
{
    auto it = m_layer_to_index.find(layer);
    if (it != m_layer_to_index.end())
    {
        return it->second;
    }

    return WRONG_IDX;
}

std::string const& LayerMap::getLayer(size_t index) const
{
    return m_index_to_layer[index];
}

void LayerMap::add(std::initializer_list<std::string> const& init_list)
{
    if (!m_initialized)
    {
        add("background");
        for (auto const& layer : init_list)
        {
            add(layer);
        }

        add("foreground");
        add("ui");
        add("posteffects");

        m_initialized = true;
    }
    else
    {
        Debug::log("Layer map has been initialized once!");
    }
}

void LayerMap::add(std::string const& layer)
{
    auto it = m_layer_to_index.find(layer);
    if (it == m_layer_to_index.end())
    {
        int idx = static_cast<int>(m_index_to_layer.size());
        m_index_to_layer.push_back(layer);
        m_layer_to_index[layer] = idx;
    }
}