#include "position.hpp"
#include "engine.hpp"
#include "scene.hpp"

Position const& Position::operator=(sf::Vector2f const& position)
{
    applyChange(position);
    return *this;
}

Position::Position(sf::Vector2f const& position)
{
    applyChange(position);
}

void Position::setTarget(std::shared_ptr<GameObject> game_object)
{
    m_game_object = game_object;
}

Position& Position::operator+=(sf::Vector2f const& value)
{
    applyChange(m_position + value);
    return *this;
}

Position& Position::operator-=(sf::Vector2f const& value)
{
    applyChange(m_position - value);
    return *this;
}

Position& Position::operator*=(float value)
{
    applyChange(m_position * value);
    return *this;
}

Position& Position::operator/=(float value)
{
    applyChange(m_position / value);
    return *this;
}

sf::Vector2f Position::operator+(sf::Vector2f const& value)
{
    return m_position + value;
}

sf::Vector2f Position::operator-(sf::Vector2f const& value)
{
    return m_position - value;
}

sf::Vector2f Position::operator*(float value)
{
    return m_position * value;
}

sf::Vector2f Position::operator/(float value)
{
    return m_position / value;
}

void Position::applyChange(sf::Vector2f const& position)
{
    auto old_position = m_position;
    m_position = position;

    auto obj = m_game_object.lock();
    if (obj)
    {
        auto grid_map = engine::get_grid_map();
        if (grid_map && old_position != m_position)
        {
            grid_map->processObjectMove(obj, old_position, m_position);
        }
    }
}
