#include "keyboard_mouse_input_handler.hpp"

#include <iostream>

void KeyboardMouseInputHandler::update()
{
    keyboard_input_handler.update();
    mouse_input_handler.update();
}

bool KeyboardMouseInputHandler::isPressed(std::string const& name)
{
    bool is_keyboard_pressed = keyboard_input_handler.isPressed(name);
    bool is_mouse_pressed = mouse_input_handler.isPressed(name);
    return is_keyboard_pressed || is_mouse_pressed;
}

bool KeyboardMouseInputHandler::isReleased(std::string const& name)
{
    bool is_keyboard_released = keyboard_input_handler.isReleased(name);
    bool is_mouse_released = mouse_input_handler.isReleased(name);
    return is_keyboard_released || is_mouse_released;
}

bool KeyboardMouseInputHandler::isPressedInUpdate(std::string const& name)
{
    bool is_keyboard_pressed = keyboard_input_handler.isPressedInUpdate(name);
    bool is_mouse_pressed = mouse_input_handler.isPressedInUpdate(name);
    return is_keyboard_pressed || is_mouse_pressed;
}

bool KeyboardMouseInputHandler::isReleasedInUpdate(std::string const& name)
{
    bool is_keyboard_released = keyboard_input_handler.isReleasedInUpdate(name);
    bool is_mouse_released = mouse_input_handler.isReleasedInUpdate(name);
    return is_keyboard_released || is_mouse_released;
}

sf::Vector2f KeyboardMouseInputHandler::getPointerPosition(sf::RenderWindow& render_window, PointerPositionType type)
{
    return mouse_input_handler.getPointerPosition(render_window, type);
}

void KeyboardMouseInputHandler::bindKeyboardKey(std::string const& name, sf::Keyboard::Key key)
{
    keyboard_input_handler.bind(name, key);
}

void KeyboardMouseInputHandler::bindMouseButton(std::string const& name, sf::Mouse::Button button)
{
    mouse_input_handler.bind(name, button);
}
