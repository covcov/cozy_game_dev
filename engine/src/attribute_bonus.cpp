#include "attribute_bonus.hpp"
#include "scene.hpp"
#include "attribute_modifier.hpp"
#include "creature.hpp"
#include "blink_effect.hpp"
#include "game_resources_storage.hpp"

AttributeBonus::AttributeBonus(std::string const& attribute_name, std::string const& tooltip_text, float duration, int value)
    : GameObjectWithTooltip(attribute_name + "_bonus", tooltip_text)
    , m_attribute_name(attribute_name)
    , m_duration(duration)
    , m_value(value)
{
    auto effect = Effect::Pointer(new BlinkEffect(1.f, 0.25f, 1.f));
    effects_storage.add("blink", effect);
}

void AttributeBonus::update(float time_delta)
{
    animation_graph.update(time_delta);
    auto scene = engine::get_active_scene();
    auto grid_map = scene->getGridMap();
    auto const& cell = grid_map->worldToCellPosition(position);

    if (!grid_map->cellContainsTag(cell, "brick"))
    {
        processPointer();
    }

    auto const& cell_objects = grid_map->getCellObjects(cell);
    for (auto object : cell_objects)
    {
        if (object->cast<Creature>() &&
            grid_map->worldToCellPosition(object->position) == cell)
        {
            auto attribute_modifier = std::make_shared<AttributeModifier>(m_attribute_name, m_duration, m_value, object);
            scene->add(attribute_modifier, "foreground");
            engine::get_sound_engine().play("BONUS", 100, false);

            if (m_tooltip)
            {
                m_tooltip->destroy();
            }

            destroy();
            break;
        }
    }
}

GameObject::Pointer AttributeBonus::clone() const
{
    GameObject::Pointer instance = std::make_shared<AttributeBonus>(m_attribute_name, m_tooltip_text, m_duration, m_value);
    cloneComponents(instance);
    return instance;
}
