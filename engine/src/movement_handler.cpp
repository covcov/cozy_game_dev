#include "movement_handler.hpp"
#include "math.hpp"

MovementHandler::MovementHandler(Scene::Pointer scene, std::shared_ptr<Creature> creature)
    : m_scene(scene)
    , m_creature(creature)
{}

bool MovementHandler::moveTowardsGridCell(float time_delta, float speed, sf::Vector2i const& target_cell)
{
    auto grid_map = m_scene->getGridMap();
    auto const target_position = grid_map->cellToWorldPosition(target_cell);
    return moveTowardsPosition(time_delta, speed, target_position);
}

bool MovementHandler::moveTowardsPosition(float time_delta, float speed, sf::Vector2f const& target_position)
{
    auto grid_map = m_scene->getGridMap();

    if (utl::almost_zero(time_delta))
    {
        return false;
    }

    bool moved = true;
    auto const old_position = m_creature->position;
    auto const old_distance = utl::distance(target_position, static_cast<sf::Vector2f>(old_position));
    if (old_distance > std::numeric_limits<float>::epsilon())
    {
        auto const direction = (target_position - m_creature->position) / old_distance;
        m_creature->position += direction * speed * time_delta;
        auto const new_distance = utl::distance(target_position, static_cast<sf::Vector2f>(m_creature->position));
        if (new_distance >= old_distance)
        {
            m_creature->position = target_position;
        }
        else
        {
            moved = false;
        }
    }

    if (old_position != m_creature->position)
    {
        grid_map->processObjectMove(m_creature, old_position, m_creature->position);
    }

    return moved;
}

bool MovementHandler::moveTowardsGridCell(float time_delta, sf::Vector2i const& target_cell)
{
    return moveTowardsGridCell(time_delta, m_creature->getAttributeCasted<float>("speed"), target_cell);
}

bool MovementHandler::moveTowardsPosition(float time_delta, sf::Vector2f const& target_position)
{
    return moveTowardsPosition(time_delta, m_creature->getAttributeCasted<float>("speed"), target_position);
}
