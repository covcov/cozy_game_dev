#include "text_object.hpp"

#include "fonts_storage.hpp"
#include "game_resources_storage.hpp"

TextObject::TextObject(std::string const& name, std::string const& text, TextObject::Params params)
    : GameObject(name)
    , m_text(text)
    , m_params(params)
{
    init();
}

void TextObject::draw(sf::RenderTarget& render_target)
{
    if (!m_draw_position_initialized) return;

    sf::Vector2f p = position;
    if (m_target)
    {
        p = m_draw_position;
    }


    auto const texture = m_sprite.getTexture();
    if (texture)
    {
        auto const size = static_cast<sf::Vector2f>(texture->getSize());
        effects_storage.apply(m_sprite);
        m_sprite.setPosition(p - 0.5f * size);
        render_target.draw(m_sprite);
    }
}

void TextObject::update(float)
{
    auto& render_window = engine::get_render_window();
    if (m_target)
    {
        m_draw_position = static_cast<sf::Vector2f>(render_window.mapCoordsToPixel(utl::round(m_target->position.get())));
        m_draw_position_initialized = true;
    }

}

void TextObject::init()
{
    auto font = engine::get_fonts_storage().getFont(m_params.font_type);
    sf::Text text;
    text.setLineSpacing(2);
    text.setFont(*font);
    text.setCharacterSize(m_params.scale * engine::get_fonts_storage().getSize(m_params.font_type));
    text.setColor(m_params.text_color);
    text.setOutlineThickness(m_params.outline_thickness);
    text.setOutlineColor(m_params.outline_color);
    text.setString(m_text);
    auto size = text.getLocalBounds();
    text.setPosition(sf::Vector2f(m_params.padding, m_params.padding));
    sf::Vector2i texture_size(size.width + 2 * m_params.padding, size.height + 2 * m_params.padding);
    auto render_texture = engine::get_render_texture_storage().get(texture_size);
    render_texture->clear(m_params.background_color);
    render_texture->draw(text);
    render_texture->display();
    m_sprite = sf::Sprite(render_texture->getTexture());
}
