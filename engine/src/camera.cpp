#include "camera.hpp"

#include "debug.hpp"

void Camera::setTarget(GameObject::Pointer target)
{
    m_target = target;
    m_camera_zone.top = m_target->position.get().x - m_camera_zone.width / 2;
    m_camera_zone.left = m_target->position.get().y - m_camera_zone.height / 2;
}

void Camera::update()
{
    auto& render_window = engine::get_render_window();
    auto view_size = render_window.getSize();

    if (m_target)
    {
        sf::Vector2f delta{};
        float const top = m_camera_zone.top;
        float const left = m_camera_zone.left;
        float const bottom = m_camera_zone.top + m_camera_zone.height;
        float const right = m_camera_zone.left + m_camera_zone.width;

        if (!utl::in_range_inclusive(m_target->position.get().y, top, bottom))
        {
            delta.y = m_target->position.get().y >= bottom ? m_target->position.get().y - bottom : m_target->position.get().y - top;
        }
        if (!utl::in_range_inclusive(m_target->position.get().x, left, right))
        {
            delta.x = m_target->position.get().x >= right ? m_target->position.get().x - right : m_target->position.get().x - left;
        }

        m_camera_zone = m_camera_zone.shift(delta);
        sf::Vector2f const c = 0.5f * (m_camera_zone.getTopLeft() + m_camera_zone.getBottomRight());

        sf::View view(c + m_shift, static_cast<sf::Vector2f>(view_size));
        view.zoom(m_scale);
        render_window.setView(view);
    }
}

sf::Vector2f Camera::getTargetPosition() const
{
    if (m_target)
    {
        sf::Vector2f const c = 0.5f * (m_camera_zone.getTopLeft() + m_camera_zone.getBottomRight());
        return c + m_shift;
    }

    return{};
}