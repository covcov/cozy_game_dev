#include "render_textures_storage.hpp"
#include <iostream>

std::shared_ptr<sf::RenderTexture> RenderTexturesStorage::get(sf::Vector2i const& size)
{
    auto it = m_render_textures_storage.find(size);
    if (it != m_render_textures_storage.end())
    {
        auto& render_textures = it->second;
        for (auto& render_texture : render_textures)
        {
            if (render_texture.first)
            {
                render_texture.first = false;
                return render_texture.second;
            }
        }

        add(render_textures, size);
        return render_textures.back().second;
    }
    else
    {
        it = m_render_textures_storage.insert(std::make_pair(size, RenderTexturesT())).first;
        auto& render_textures = it->second;
        add(render_textures, size);
        return render_textures.back().second;
    }

}

void RenderTexturesStorage::reset()
{
    for (auto& p : m_render_textures_storage)
    {
        for (auto& render_texture : p.second)
        {
            render_texture.first = true;
        }
    }
}

void RenderTexturesStorage::add(RenderTexturesT& render_textures, sf::Vector2i const& size)
{
    render_textures.emplace_back(false, std::make_shared<sf::RenderTexture>());
    render_textures.back().second->create(size.x, size.y);
}
