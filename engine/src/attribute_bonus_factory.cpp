#include "attribute_bonus_factory.hpp"

#include "attribute_modifier.hpp"

std::unordered_map<AttributeBonusFactory::Type, std::string> const AttributeBonusFactory::TYPE_TO_ATTRIBUTE_NAME_TABLE =
{
    { AttributeBonusFactory::Type::SPEED, "speed" },
    { AttributeBonusFactory::Type::POWER, "power" },
    { AttributeBonusFactory::Type::CHARGE, "charge" },
};

std::unordered_map<AttributeBonusFactory::Type, std::string> const AttributeBonusFactory::TYPE_TO_TOOLTIP_TEXT_TABLE =
{
    { AttributeBonusFactory::Type::SPEED, "Speed Bonus:\n\nIncreases character's speed" },
    { AttributeBonusFactory::Type::POWER, "Power Bonus:\n\nIncreases explosion power" },
    { AttributeBonusFactory::Type::CHARGE, "Charge Bonus:\n\nAllows to plant one more bomb" },
};

GameObject::Pointer AttributeBonusFactory::create(Type type)
{
    auto const& stat_name = TYPE_TO_ATTRIBUTE_NAME_TABLE.at(type);
    auto const& tooltip_text = TYPE_TO_TOOLTIP_TEXT_TABLE.at(type);
    switch (type)
    {
    case AttributeBonusFactory::Type::SPEED:
        return std::make_shared<AttributeBonus>(stat_name, tooltip_text, 50, 5.f);
    case AttributeBonusFactory::Type::POWER:
        return std::make_shared<AttributeBonus>(stat_name, tooltip_text, 1, -1.f);
    case AttributeBonusFactory::Type::CHARGE:
        return std::make_shared<AttributeBonus>(stat_name, tooltip_text, 1, -1.f);
    }

    return nullptr;
}