#include "blink_effect.hpp"

#include "math.hpp"

BlinkEffect::BlinkEffect(float time_period, float transparency_min, float transparency_max)
    : m_time_period(time_period)
    , m_transparency(1.f)
    , m_time_elapsed(0.f)
    , m_transparency_min(transparency_min)
    , m_transparency_max(transparency_max)
{}

void BlinkEffect::update(float time_delta)
{
    m_time_elapsed = utl::fmod(m_time_elapsed + time_delta, m_time_period);
    float const k = (1.f + std::cos(m_time_elapsed / m_time_period * 2.f * utl::PI)) * 0.5f;
    m_transparency = k * (m_transparency_max - m_transparency_min) + m_transparency_min;
}

void BlinkEffect::apply(sf::Sprite& sprite)
{
    sprite.setColor({ 255, 255, 255, static_cast<sf::Uint8>(255.f * m_transparency) });
}

bool BlinkEffect::isFinished() const
{
    return false;
}

Effect::Pointer BlinkEffect::clone() const
{
    return Effect::Pointer(new BlinkEffect(m_time_period, m_transparency_min, m_transparency_max));
}
