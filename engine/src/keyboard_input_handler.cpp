#include "keyboard_input_handler.hpp"

void KeyboardInputHandler::update()
{
    for (auto const& p : m_key_binds)
    {
        sf::Keyboard::Key const& key = p.second;

        m_is_key_pressed_in_update[key] = false;
        m_is_key_released_in_update[key] = false;

        if (!m_is_key_pressed[key] && sf::Keyboard::isKeyPressed(key))
        {
            m_is_key_pressed[key] = true;
            m_is_key_pressed_in_update[key] = true;
        }
        else if (m_is_key_pressed[key] && !sf::Keyboard::isKeyPressed(key))
        {
            m_is_key_pressed[key] = false;
            m_is_key_released_in_update[key] = true;
        }
    }
}

bool KeyboardInputHandler::isPressed(std::string const& name)
{
    auto range = m_key_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = sf::Keyboard::isKeyPressed(it->second);
        result |= r;
    }

    return result;
}

bool KeyboardInputHandler::isReleased(std::string const& name)
{
    return !isPressed(name);
}

bool KeyboardInputHandler::isPressedInUpdate(std::string const& name)
{
    auto range = m_key_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = m_is_key_pressed_in_update[it->second];
        m_is_key_pressed_in_update[it->second] = false;
        result |= r;
    }

    return result;
}

bool KeyboardInputHandler::isReleasedInUpdate(std::string const& name)
{
    auto range = m_key_binds.equal_range(name);
    bool result = false;

    auto it = range.first;
    for (it = range.first; it != range.second; ++it)
    {
        bool r = m_is_key_released_in_update[it->second];
        m_is_key_released_in_update[it->second] = false;
        result |= r;
    }

    return result;
}

void KeyboardInputHandler::bind(std::string const& name, sf::Keyboard::Key key)
{
    m_key_binds.insert(std::make_pair(name, key));
}
