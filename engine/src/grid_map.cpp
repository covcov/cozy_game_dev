#include "grid_map.hpp"

#include <iostream>
#include <unordered_set>

#include "algorithm_extra.hpp"
#include "collision_manager.hpp"
#include "debug.hpp"

void GridMap::add(GameObject::Pointer game_object)
{
    if (game_object->collider)
    {
        auto const roi = getRegionOfInterest(game_object->collider->getAABB());
        addToRegionOfInterest(game_object, roi);
    }
}

void GridMap::add(sf::Vector2i const& cell, GameObject::Pointer game_object)
{
    sf::Vector2f world_position = cellToWorldPosition(cell);
    game_object->position = world_position;
    add(game_object);
}

std::vector<GameObject::Pointer> GridMap::getCollidedObjects(GameObject::Pointer game_object) const
{
    return getCollidedObjects(game_object, game_object->collision_mask);
}

std::vector<GameObject::Pointer> GridMap::getCollidedObjects(
    GameObject::Pointer game_object,
    std::unordered_set<std::string> const& mask) const
{
    std::unordered_set<int> unique_ids;
    std::vector<GameObject::Pointer> collided;
    auto const collider = game_object->collider;
    if (collider)
    {
        auto const roi = getRegionOfInterest(collider->getAABB());
        for (int x = roi.first.x; x <= roi.second.x; ++x)
        {
            for (int y = roi.first.y; y <= roi.second.y; ++y)
            {
                if (m_grid_storage.contains(x, y))
                {
                    for (auto obj : m_grid_storage(x, y))
                    {
                        if (game_object != obj &&
                            CollisionManager::collide(game_object->collider, obj->collider) &&
                            unique_ids.find(obj->getId()) == unique_ids.end() &&
                            mask.find(obj->tag) != mask.end())
                        {
                            collided.push_back(obj);
                            unique_ids.insert(obj->getId());
                        }
                    }
                }
            }
        }
    }

    return collided;
}

GameObject::Pointer GridMap::getCellObjectByTag(sf::Vector2i const& cell, std::string const& tag) const
{
    auto const& cell_objects = getCellObjects(cell);
    for (auto obj : cell_objects)
    {
        if (obj->tag == tag)
        {
            return obj;
        }
    }

    return nullptr;
}

bool GridMap::cellContainsTag(sf::Vector2i const& cell, std::string const& tag) const
{
    auto const& cell_objects = getCellObjects(cell);
    return utl::contains_linear(cell_objects, [&](GameObject::Pointer obj) { return obj->tag == tag; });
}

bool GridMap::cellContainsAnyOfTags(sf::Vector2i const& cell, std::unordered_set<std::string> const& tags) const
{
    for (auto const& cell_object : m_grid_storage(cell.x, cell.y))
    {
        auto it = tags.find(cell_object->tag);
        if (it != tags.end())
        {
            return true;
        }
    }

    return false;
}

bool GridMap::cellContainsObject(sf::Vector2i const& cell, GameObject::Pointer game_object) const
{
    auto const& cell_objects = getCellObjects(cell);
    auto const same_id = [game_object](GameObject::Pointer obj) { return obj->getId() == game_object->getId(); };
    return utl::contains_linear(cell_objects, same_id);
}

sf::Vector2f GridMap::cellToWorldPosition(sf::Vector2i const& position) const
{
    sf::Vector2f const& tl = m_top_left_position;
    return {
        tl.x + (position.x + 0.5f) * m_cell_size,
        tl.y + (position.y + 0.5f) * m_cell_size };
}

sf::Vector2i GridMap::worldToCellPosition(sf::Vector2f const& position) const
{
    sf::Vector2f const d = position - m_top_left_position;
    return {
        static_cast<int>(std::floor(d.x / m_cell_size)),
        static_cast<int>(std::floor(d.y / m_cell_size)) };
}

void GridMap::processObjectMove(GameObject::Pointer game_object, sf::Vector2f old_position, sf::Vector2f new_position)
{
    if (game_object->collider)
    {
        auto const aabb = game_object->collider->getAABB();

        auto const old_aabb = aabb.shift(old_position - game_object->position);
        auto const old_roi = getRegionOfInterest(old_aabb);
        eraseFromRegionOfInterest(game_object, old_roi);

        auto const new_aabb = aabb.shift(new_position - game_object->position);
        auto const new_roi = getRegionOfInterest(new_aabb);
        addToRegionOfInterest(game_object, new_roi);
    }
}

void GridMap::processObjectRemove(GameObject::Pointer game_object)
{
    if (game_object->collider)
    {
        auto const roi = getRegionOfInterest(game_object->collider->getAABB());
        eraseFromRegionOfInterest(game_object, roi);
    }
}

std::pair<sf::Vector2i, sf::Vector2i> GridMap::getRegionOfInterest(utl::Rect<float> const& rect) const
{
    sf::Vector2i const tl = worldToCellPosition(rect.getTopLeft());
    sf::Vector2i const br = worldToCellPosition(rect.getBottomRight());
    return { tl, br };
}

void GridMap::eraseFromRegionOfInterest(GameObject::Pointer game_object, std::pair<sf::Vector2i, sf::Vector2i> const& roi)
{
    auto const same_id = [game_object](GameObject::Pointer obj) { return obj->getId() == game_object->getId(); };
    for (int x = roi.first.x; x <= roi.second.x; ++x)
    {
        for (int y = roi.first.y; y <= roi.second.y; ++y)
        {
            if (m_grid_storage.contains(x, y))
            {
                utl::inplace::erase_if(m_grid_storage(x, y), same_id);
            }
        }
    }
}

void GridMap::addToRegionOfInterest(GameObject::Pointer game_object, std::pair<sf::Vector2i, sf::Vector2i> const& roi)
{
    for (int x = roi.first.x; x <= roi.second.x; ++x)
    {
        for (int y = roi.first.y; y <= roi.second.y; ++y)
        {
            if (m_grid_storage.contains(x, y))
            {
                m_grid_storage(x, y).push_back(game_object);
            }
        }
    }
}
