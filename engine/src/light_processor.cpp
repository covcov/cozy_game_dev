#include "light_processor.hpp"
#include "scene.hpp"
#include "blur_effect.hpp"
#include "ray_caster.hpp"
#include "random_engine.hpp"
#include "algorithm_extra.hpp"
#include "math.hpp"
#include "follower.hpp"
#include "collider_factory.hpp"
#include "map_preprocessor.hpp"

void LightProcessor::awake()
{
    auto camera = engine::get_camera();
    effects_storage.add("blur", std::make_shared<BlurEffect>(15));
    auto result = MapPreprocessor::preprocess({ "wall" });
    m_corners = std::move(result.corners);
    m_edges = std::move(result.edges);

    size_t constexpr N = 36;
    std::vector<sf::Vector2f> vertices;
    sf::Vector2f direction(LIGHT_SOURCE_RADIUS, 0);
    for (float angle = 0.f; angle < 2 * utl::PI; angle += 2 * utl::PI / N)
    {
        vertices.push_back(utl::rotate({}, direction, angle));
    }

    for (size_t i = 0; i < N; ++i)
    {
        m_visible_circle_edges.emplace_back(vertices[i], vertices[(i + 1) % N]);
    }
}

void LightProcessor::update(float time_delta)
{
    auto& render_texture_storage = engine::get_render_texture_storage();
    auto& render_window = engine::get_render_window();
    auto const center_pixel = static_cast<sf::Vector2i>(render_window.getSize()) / 2;
    auto const center = render_window.mapPixelToCoords(center_pixel);
    position = center;

    int constexpr PIXEL_PADDING = 16;
    sf::Vector2i const PIXEL_PADDING_2D = { PIXEL_PADDING, PIXEL_PADDING };
    auto const tl = render_window.mapPixelToCoords(-PIXEL_PADDING_2D);
    auto const br = render_window.mapPixelToCoords(static_cast<sf::Vector2i>(render_window.getSize()) + PIXEL_PADDING_2D);
    auto texture = render_texture_storage.get(static_cast<sf::Vector2i>(br - tl));
    texture->clear(sf::Color(0, 0, 0, 255));

    for (auto light_source : light_sources)
    {
        auto const& source = light_source.first;
        std::vector<sf::Vector2f> all_hits = castToCorners(source->position);

        for (size_t i = 0; i < all_hits.size(); ++i)
        {
            sf::ConvexShape transparent_shape;
            transparent_shape.setPointCount(3);
            transparent_shape.setPoint(0, source->position - tl);
            transparent_shape.setPoint(1, all_hits[i] - tl);
            transparent_shape.setPoint(2, all_hits[(i + 1) % all_hits.size()] - tl);
            transparent_shape.setFillColor(sf::Color(255, 255, 255, 0));
            texture->draw(transparent_shape, sf::BlendMultiply);
        }
    }

    texture->display();
    m_sprite = sf::Sprite(texture->getTexture());
    light_sources.clear();
}

GameObject::Pointer LightProcessor::clone() const
{
    auto instance = std::make_shared<LightProcessor>();
    cloneComponents(instance);
    return instance;
}

void LightProcessor::draw(sf::RenderTarget& render_target)
{
    drawSprite(render_target, m_sprite);
}

void LightProcessor::addSource(GameObject::Pointer game_object, float radius)
{
    radius = std::min(LIGHT_SOURCE_RADIUS, radius);
    light_sources.emplace_back(game_object, radius);
}

std::vector<sf::Vector2f> LightProcessor::castThreeRays(
    sf::Vector2f const& origin,
    sf::Vector2f const& target,
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> const& edges) const
{
    auto const direction = target - origin;
    float const DELTA = utl::deg_to_rad(0.5f);
    auto const direction_plus = utl::rotate({}, direction, DELTA);
    auto const direction_minus = utl::rotate({}, direction, -DELTA);

    std::vector<sf::Vector2f> hits;
    hits.push_back(getHit(origin, direction, edges));
    hits.push_back(getHit(origin, direction_plus, edges));
    hits.push_back(getHit(origin, direction_minus, edges));

    return hits;
}

std::vector<sf::Vector2f> LightProcessor::castToCorners(sf::Vector2f const& origin) const
{
    auto edges = m_edges;
    for (auto const& circle_edge : m_visible_circle_edges)
    {
        edges.emplace_back(circle_edge.first + origin, circle_edge.second + origin);
    }

    std::vector<sf::Vector2f> all_hits;
    for (auto const& corner : m_corners)
    {
        auto const hits = castThreeRays(origin, corner, edges);
        all_hits.insert(all_hits.end(), hits.begin(), hits.end());
    }

    for (auto const& circle_edge : m_visible_circle_edges)
    {
        auto const hits = castThreeRays(origin, circle_edge.first + origin, edges);
        all_hits.insert(all_hits.end(), hits.begin(), hits.end());
    }

    auto comp = [&](sf::Vector2f const& a, sf::Vector2f const& b)
    {
        return utl::pseudo_angle(a - origin) < utl::pseudo_angle(b - origin);
    };

    std::sort(all_hits.begin(), all_hits.end(), comp);

    return all_hits;
}

sf::Vector2f LightProcessor::getHit(
    sf::Vector2f const& origin,
    sf::Vector2f const& direction,
    std::vector<std::pair<sf::Vector2f, sf::Vector2f>> const& edges) const
{
    sf::Vector2f const direction_norm = direction / utl::norm(direction);
    float min_distance = std::numeric_limits<float>::infinity();
    sf::Vector2f hit;
    for (auto const& edge : edges)
    {
        utl::Line const edge_line(edge.first, edge.second);
        utl::Line const ray_line(origin, origin + direction);
        auto const intersection = utl::find_intersection(edge_line, ray_line);
        if (intersection.valid)
        {
            bool const belongs = utl::belongs_to_segment(
                intersection.intersectionPoint,
                edge.first,
                edge.second,
                utl::DISTANCE_EPS);

            if (belongs)
            {
                sf::Vector2f const current_direction = intersection.intersectionPoint - origin;
                sf::Vector2f const current_direction_norm = current_direction / utl::norm(current_direction);
                bool const same_ray = utl::almost_zero(utl::norm(current_direction_norm - direction_norm), utl::DISTANCE_EPS);
                if (same_ray)
                {
                    auto const distance = utl::distance(intersection.intersectionPoint, origin);
                    if (distance < min_distance)
                    {
                        min_distance = distance;
                        hit = intersection.intersectionPoint;
                    }
                }
            }
        }
    }

    return hit;
}
