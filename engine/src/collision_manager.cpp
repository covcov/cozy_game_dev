#include "collision_manager.hpp"
#include "box_collider.hpp"
#include "circle_collider.hpp"

bool CollisionManager::collide(Collider::Pointer collider, sf::Vector2f const& position)
{
    if (collider)
    {
        auto type = collider->getType();
        switch (type)
        {
        case Collider::Type::BOX:
        {
            auto box = std::dynamic_pointer_cast<BoxCollider>(collider)->getShape();
            return box.contains(position);
        }
        case Collider::Type::CIRCLE:
        {
            auto circle = std::dynamic_pointer_cast<CircleCollider>(collider)->getShape();
            return circle.contains(position);
        }
        }
    }
    return false;
}

bool CollisionManager::collide(Collider::Pointer collider1, Collider::Pointer collider2)
{
    if (collider1 && collider2)
    {
        auto type = collider1->getType();
        switch (type)
        {
        case Collider::Type::BOX:
        {
            auto box = std::dynamic_pointer_cast<BoxCollider>(collider1)->getShape();
            return collide(box, collider2);
        }
        case Collider::Type::CIRCLE:
        {
            auto circle = std::dynamic_pointer_cast<CircleCollider>(collider1)->getShape();
            return collide(circle, collider2);
        }
        }
    }

    return false;
}

template <typename T>
bool CollisionManager::collide(T const& shape, Collider::Pointer other)
{
    auto other_type = other->getType();
    switch (other_type)
    {
    case Collider::Type::BOX:
    {
        auto box = std::dynamic_pointer_cast<BoxCollider>(other)->getShape();
        return collide(shape, box);
    }
    case Collider::Type::CIRCLE:
    {
        auto circle = std::dynamic_pointer_cast<CircleCollider>(other)->getShape();
        return collide(shape, circle);
    }
    }

    return false;
}

bool CollisionManager::collide(utl::Circle<float> const& circle, utl::Rect<float> const& rect)
{
    float const cx = circle.position.x;
    float const cy = circle.position.y;
    float const rx = rect.left + rect.width / 2;
    float const ry = rect.top + rect.height / 2;
    float const w = rect.width;
    float const h = rect.height;

    float const dx = std::fabs(cx - rx);
    float const dy = std::fabs(cy - ry);

    if (dx > w / 2 + circle.radius) return false;
    if (dy > h / 2 + circle.radius) return false;

    if (dx <= w / 2) return true;
    if (dy <= h / 2) return true;

    float const corner_distance_squared = utl::sqr(dx - w / 2) + utl::sqr(dy - h / 2);
    return corner_distance_squared <= utl::sqr(circle.radius);
}