#include "health_bar.hpp"

#include "scene.hpp"
#include "game_resources_storage.hpp"

HealthBar::HealthBar(std::string const& name, std::shared_ptr<Creature> creature, sf::Vector2i const& size, sf::Vector2f shift)
    : GameObject(name)
    , m_creature(creature)
    , m_size(size)
    , m_shift(shift)
{
    position = m_creature->position + m_shift;
}

void HealthBar::update(float time_delta)
{
    auto position_old = position;
    position = m_creature->position + m_shift;
    auto grid_map = engine::get_grid_map();
    grid_map->processObjectMove(shared_from_this(), position_old, position);

    if (m_creature->getAttribute("health") == 0)
    {
        destroy();
    }

    auto health = m_creature->getAttribute("health");
    auto max_health = m_creature->getAttribute("health_max");
    sf::RectangleShape rectangle_shape;
    rectangle_shape.setSize(static_cast<sf::Vector2f>(m_size));
    rectangle_shape.setFillColor(sf::Color::Red);

    auto render_texture = engine::get_render_texture_storage().get(m_size);
    render_texture->clear(sf::Color::Black);
    render_texture->draw(rectangle_shape);
    float width = m_size.x * health / max_health;
    rectangle_shape.setSize({ width, static_cast<float>(m_size.y) });
    rectangle_shape.setFillColor(sf::Color::Green);
    render_texture->draw(rectangle_shape);
    render_texture->display();
    m_sprite = sf::Sprite(render_texture->getTexture());
}

void HealthBar::draw(sf::RenderTarget& render_target)
{
    drawSprite(render_target, m_sprite);
}