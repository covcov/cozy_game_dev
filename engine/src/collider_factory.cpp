#include "collider_factory.hpp"
#include "box_collider.hpp"
#include "circle_collider.hpp"
#include "debug.hpp"

void ColliderFactory::addBoxCollider(GameObject::Pointer game_object, sf::Vector2f const& size, sf::Vector2f shift)
{
    game_object->collider = Collider::Pointer(new BoxCollider(game_object->position, shift, size));
}

void ColliderFactory::addCircleCollider(GameObject::Pointer game_object, float radius, sf::Vector2f shift)
{
    game_object->collider = Collider::Pointer(new CircleCollider(game_object->position, shift, radius));
}