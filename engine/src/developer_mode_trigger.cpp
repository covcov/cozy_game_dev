#include "developer_mode_toggler.hpp"
#include "engine.hpp"
#include "debug.hpp"

void DeveloperModeToggler::update(float time_delta)
{
    auto input_handler = engine::get_input_handler();
    if (input_handler->isPressedInUpdate("DEVELOPER_MODE"))
    {
        Debug::toggleDeveloperMode();
    }
}

GameObject::Pointer DeveloperModeToggler::clone() const
{
    return std::make_shared<DeveloperModeToggler>(m_name);
}