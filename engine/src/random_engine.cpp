#include "random_engine.hpp"

void RandomEngine::setSeed(int seed)
{
    srand(seed);
}

int RandomEngine::randomRange(int low, int high)
{
    if (low > high)
    {
        std::swap(low, high);
    }

    int bias = low < 0 ? -low : 0;
    low += bias;
    high += bias;
    int value = rand() % (1 + high - low) + low;
    return value - bias;
}

float RandomEngine::randomRange(float low, float high)
{
    if (low > high)
    {
        std::swap(low, high);
    }

    float const random_value = random();
    return low + (high - low) * random_value;
}

float RandomEngine::random()
{
    int const LARGE_VALUE = 10001;
    int const random_value = randomRange(0, LARGE_VALUE);
    return static_cast<float>(random_value) / LARGE_VALUE;
}
