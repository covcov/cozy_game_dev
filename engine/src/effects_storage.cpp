#include "effects_storage.hpp"

#include "algorithm_extra.hpp"

void EffectsStorage::update(float time_delta)
{
    for (auto it : m_effects)
    {
        it.second->update(time_delta);
    }

    for (auto it = std::begin(m_effects); it != std::end(m_effects);)
    {
        if (it->second->isFinished())
        {
            it = m_effects.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

void EffectsStorage::apply(sf::Sprite& sprite) const
{
    for (auto p : m_effects)
    {
        p.second->apply(sprite);
    }
}

void EffectsStorage::add(std::string const& name, Effect::Pointer effect)
{
    m_effects[name] = effect;
}

void EffectsStorage::remove(std::string const& name)
{
    auto it = m_effects.find(name);
    if (it != m_effects.end())
    {
        m_effects.erase(it);
    }
}

bool EffectsStorage::contains(std::string const& name)
{
    return m_effects.find(name) != m_effects.end();
}
