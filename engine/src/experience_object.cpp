#include "experience_object.hpp"

#include <iostream>

#include "fonts_storage.hpp"
#include "game_resources_storage.hpp"
#include "fade_effect.hpp"

ExperienceObject::ExperienceObject(std::string name, sf::Vector2f const& position, int amount, float duration)
    : GameObject(name)
    , m_duration(duration)
    , m_elapsed_time(0)
    , m_amount(amount)
{
    init(position);
}

void ExperienceObject::update(float time_delta)
{
    m_elapsed_time += time_delta;
    if (m_elapsed_time > m_duration)
    {
        destroy();
    }
}

void ExperienceObject::draw(sf::RenderTarget& render_target)
{
    return drawSprite(render_target, m_sprite);
}

void ExperienceObject::init(sf::Vector2f const& position)
{
    auto font = engine::get_fonts_storage().getFont("GRAVITY_BOLD");
    sf::Text text;
    text.setFont(*font);
    text.setCharacterSize(engine::get_fonts_storage().getSize("GRAVITY_BOLD"));
    text.setColor(sf::Color::Yellow);
    text.setOutlineThickness(1);
    text.setOutlineColor(sf::Color::Black);
    auto const str = std::to_string(m_amount) + " XP";
    text.setString(str);
    auto size = text.getLocalBounds();
    auto render_texture = engine::get_render_texture_storage().get(sf::Vector2i(size.width, size.height));
    render_texture->clear(sf::Color::Transparent);
    render_texture->draw(text);
    render_texture->display();
    m_sprite.setTexture(render_texture->getTexture());

    auto effect = Effect::Pointer(new FadeEffect(m_duration, true));
    effects_storage.add("fade", effect);
    this->position = position;
}
