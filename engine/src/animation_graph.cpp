#include "animation_graph.hpp"

bool AnimationGraph::makeTransition(std::string const& message)
{
    size_t const active = m_active;
    auto it = m_transitions[m_active].find(message);
    if (it != m_transitions[m_active].end())
    {
        m_active = it->second;
        if (active != m_active)
        {
            m_animations[m_active]->reset();
        }

        return true;
    }

    return false;
}

void AnimationGraph::update(float time_delta)
{
    auto animation = getActive();
    if (animation)
    {
        animation->update(time_delta);
    }
}

Animation::Pointer AnimationGraph::getActive()
{
    if (m_animations.empty())
    {
        return nullptr;
    }

    return m_animations[m_active];
}

void AnimationGraph::setActive(std::string const& name)
{
    auto it = m_name_to_index.find(name);
    if (it != m_name_to_index.end())
    {
        m_active = it->second;
    }
}

AnimationGraph AnimationGraph::clone() const
{
    AnimationGraph animation_graph;
    animation_graph.setShift(m_shift);
    for (size_t i = 0; i < m_names.size(); ++i)
    {
        animation_graph.registerAnimation(m_animations[i]->clone(), m_names[i]);
    }

    for (int i = 0; i < m_names.size(); ++i)
    {
        auto const& from = m_names[i];
        for (auto const& transition : m_transitions[i])
        {
            std::string const& message = transition.first;
            std::string const& to = m_names[transition.second];
            animation_graph.registerTransition(from, to, message);
        }
    }

    return animation_graph;
}

void AnimationGraph::registerTransition(std::string const& from, std::string const& to, std::string const& message)
{
    auto from_it = m_name_to_index.find(from);
    auto to_it = m_name_to_index.find(to);

    if (from_it != m_name_to_index.end() && to_it != m_name_to_index.end())
    {
        auto from_idx = from_it->second;
        auto to_idx = to_it->second;
        auto it = m_transitions[from_idx].find(message);
        if (it == m_transitions[from_idx].end())
        {
            m_transitions[from_idx].insert(std::make_pair(message, to_idx));
        }
    }
}

void AnimationGraph::registerAnimation(Animation::Pointer animation, std::string const& name)
{
    auto it = m_name_to_index.find(name);
    if (it == m_name_to_index.end())
    {
        m_animations.push_back(animation);
        m_name_to_index.insert(std::make_pair(name, m_animations.size() - 1));
        m_transitions.emplace_back();
        m_names.push_back(name);
    }
}
