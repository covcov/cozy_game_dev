#include "map_preprocessor.hpp"

#include "engine.hpp"
#include "grid_map.hpp"

MapPreprocessor::Result MapPreprocessor::preprocess(std::unordered_set<std::string> const& mask)
{
    Result result;
    auto grid_map = engine::get_grid_map();
    if (grid_map)
    {
        auto const size = grid_map->getGridSize();
        utl::Matrix<Cell> tile_map(size.x, size.y);
        std::vector<Edge> edges;

        for (size_t y = 0; y < size.y; ++y)
        {
            for (size_t x = 0; x < size.x; ++x)
            {
                if (grid_map->cellContainsAnyOfTags(sf::Vector2i(x, y), mask))
                {
                    tile_map(x, y).exist = true;
                }
            }
        }

        for (size_t y = 0; y < size.y; ++y)
        {
            for (size_t x = 0; x < size.x; ++x)
            {
                if (tile_map(x, y).exist)
                {
                    auto const li = utl::to_index(utl::DirectionType::LEFT);
                    auto const ti = utl::to_index(utl::DirectionType::TOP);
                    auto const ri = utl::to_index(utl::DirectionType::RIGHT);
                    auto const bi = utl::to_index(utl::DirectionType::BOTTOM);

                    auto const lp = sf::Vector2i(x, y) + utl::get_direction(utl::DirectionType::LEFT);
                    auto const tp = sf::Vector2i(x, y) + utl::get_direction(utl::DirectionType::TOP);
                    auto const rp = sf::Vector2i(x, y) + utl::get_direction(utl::DirectionType::RIGHT);
                    auto const bp = sf::Vector2i(x, y) + utl::get_direction(utl::DirectionType::BOTTOM);

                    extendEdge(tile_map, x, y, edges, lp, tp, li, utl::DirectionType::NONE, utl::DirectionType::BOTTOM);
                    extendEdge(tile_map, x, y, edges, rp, tp, ri, utl::DirectionType::RIGHT, utl::DirectionType::BOTTOM);
                    extendEdge(tile_map, x, y, edges, tp, lp, ti, utl::DirectionType::NONE, utl::DirectionType::RIGHT);
                    extendEdge(tile_map, x, y, edges, bp, lp, bi, utl::DirectionType::BOTTOM, utl::DirectionType::RIGHT);
                }
            }
        }

        auto const sz = grid_map->getCellSize();
        std::vector<size_t> used(size.x * size.y);
        for (auto const& edge : edges)
        {
            sf::Vector2f v = static_cast<sf::Vector2f>(edge.from) * sz;
            sf::Vector2f u = static_cast<sf::Vector2f>(edge.to) * sz;
            result.edges.emplace_back(v, u);
            used[edge.from.y * size.x + edge.from.x] = 1;
            used[edge.to.y * size.x + edge.to.x] = 1;
        }

        for (size_t i = 0; i < used.size(); ++i)
        {
            if (used[i])
            {
                size_t x = i % size.x;
                size_t y = i / size.x;
                result.corners.emplace_back(x * sz, y * sz);
            }
        }
    }

    return result;
}

void MapPreprocessor::extendEdge(
    utl::Matrix<Cell>& tile_map,
    size_t x,
    size_t y,
    std::vector<Edge>& edges,
    sf::Vector2i const& np,
    sf::Vector2i const& extend_from,
    size_t ni,
    utl::DirectionType from,
    utl::DirectionType to)
{
    if (!tile_map.contains(np.x, np.y) || !tile_map(np.x, np.y).exist)
    {
        if (tile_map.contains(extend_from.x, extend_from.y) &&
            tile_map(extend_from.x, extend_from.y).edge_exist[ni])
        {
            auto const id = tile_map(extend_from.x, extend_from.y).edge_ids[ni];
            edges[id].to += utl::get_direction(to);
            tile_map(x, y).edge_ids[ni] = id;
            tile_map(x, y).edge_exist[ni] = true;
        }
        else
        {
            size_t edge_id = edges.size();
            auto const v = sf::Vector2i(x, y) + utl::get_direction(from);
            edges.emplace_back(v, v + utl::get_direction(to));
            tile_map(x, y).edge_exist[ni] = true;
            tile_map(x, y).edge_ids[ni] = edge_id;
        }
    }
}
