#include "scene_manager.hpp"

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "debug.hpp"
#include "i_enemy.hpp"
#include "game_resources_storage.hpp"

bool SceneManager::is_run = false;

SceneManager& SceneManager::instance()
{
    static SceneManager scene_manager;
    return scene_manager;
}

bool SceneManager::registerScene(std::string const& name, Scene::Pointer scene, std::string const& next)
{
    if (registerScene(name, scene))
    {
        m_next_scene_names.back() = next;
        return true;
    }

    return false;
}

bool SceneManager::registerScene(std::string const& name, Scene::Pointer scene)
{
    auto it = m_name_to_index_table.find(name);
    if (it == m_name_to_index_table.end())
    {
        m_name_to_index_table.insert(std::make_pair(name, m_names.size()));
        m_names.push_back(name);
        m_scene_prototypes.push_back(scene);
        m_scenes.push_back(scene->clone());
        m_next_scene_names.push_back("");
        return true;
    }

    return false;
}

void SceneManager::reset()
{
    m_reset = true;
}

void SceneManager::playNext()
{
    auto const& next = m_next_scene_names[m_active_index];
    auto it = m_name_to_index_table.find(next);
    if (it != m_name_to_index_table.end())
    {
        m_active_index = it->second;
        m_scenes[m_active_index] = m_scene_prototypes[m_active_index]->clone();
        m_scenes[m_active_index]->awakeAll();
        m_first_update = true;
    }
}

void SceneManager::setActive(std::string const& name)
{
    auto it = m_name_to_index_table.find(name);
    if (it != m_name_to_index_table.end())
    {
        auto const& next_scene_name = m_next_scene_names[it->second];
        m_next_index = m_active_index;
        if (!next_scene_name.empty())
        {
            auto it_next = m_name_to_index_table.find(next_scene_name);
            if (it_next != m_name_to_index_table.end())
            {
                m_next_index = it_next->second;
            }
        }

        m_active_index = it->second;
        m_scenes[m_active_index]->awakeAll();
        m_first_update = true;
    }
}

void SceneManager::runMainLoop()
{
    if (!is_run)
    {
        is_run = true;
        auto& render_window = engine::get_render_window();

        sf::Event event;
        sf::Clock clock;
        bool isFocused = true;

        while (true)
        {
            render_window.pollEvent(event);

            auto input_handler = engine::get_input_handler();
            if (event.type == sf::Event::GainedFocus)
            {
                isFocused = true;
            }
            else if (event.type == sf::Event::LostFocus)
            {
                isFocused = false;
            }

            sf::Time time = clock.restart();
            float const time_delta = time.asSeconds();

            if (isFocused)
            {
                if (input_handler->isPressed("EXIT"))
                {
                    render_window.close();
                    break;
                }

                input_handler->update();

                engine::get_sound_engine().update();

                m_scenes[m_active_index]->update(time_delta);
                engine::get_camera()->update();
                lateReset();

                if (!m_first_update)
                {
                    engine::get_render_texture().clear(sf::Color::Black);
                    render_window.clear();
                    m_scenes[m_active_index]->draw();
                    render_window.display();
                }

                m_first_update = false;
            }
        }
    }
}

void SceneManager::lateReset()
{
    if (m_reset)
    {
        m_scenes[m_active_index] = m_scene_prototypes[m_active_index]->clone();
        m_scenes[m_active_index]->awakeAll();
        m_first_update = true;
        m_reset = false;
    }
}