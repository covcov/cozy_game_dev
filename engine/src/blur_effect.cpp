#include "blur_effect.hpp"

#include "engine.hpp"

namespace
{

enum class BlurDirectionType
{
    HORIZONTAL,
    VERTICAL
};

std::shared_ptr<sf::Shader> create_blur_shader(int blur_size, BlurDirectionType direction)
{
    std::string shader_code =
        "uniform sampler2D texture;"
        "uniform float invertTextureSize;"
        "uniform int blurSize;"
        "uniform vec2 direction;"
        "void main()"
        "{"
        "    vec4 color = vec4(0);"
        "    vec4 tmpColor;"
        "    float sumColors = 0;"
        "    float sumAlphas = 0;"
        "    for (int i = -blurSize; i <= blurSize; ++i)"
        "    {"
        "        tmpColor = texture2D("
        "            texture,"
        "            vec2("
        "                gl_TexCoord[0].s + (direction.x * i * invertTextureSize),"
        "                gl_TexCoord[0].t + (direction.y * i * invertTextureSize)));"
        "        color.rgb += tmpColor.rgb * tmpColor.a;"
        "        color.a += tmpColor.a;"
        "        sumColors += tmpColor.a;"
        "        sumAlphas += 1;"
        "    }"
        "    color.rgb /= sumColors;"
        "    color.a /= sumAlphas;"
        "    gl_FragColor = vec4(color.r, color.g, color.b, color.a);"
        "}";

    std::shared_ptr<sf::Shader> shader(new sf::Shader());
    shader->loadFromMemory(shader_code, sf::Shader::Fragment);
    shader->setUniform("blurSize", blur_size);
    shader->setUniform("direction", direction == BlurDirectionType::HORIZONTAL ? sf::Vector2f(1, 0) : sf::Vector2f(0, 1));
    return shader;
}

}

BlurEffect::BlurEffect(int blur_size)
    : m_is_infinite(true)
    , m_duration(0.f)
    , m_blur_size(blur_size)
{
    m_shader_x = create_blur_shader(m_blur_size, BlurDirectionType::HORIZONTAL);
    m_shader_y = create_blur_shader(m_blur_size, BlurDirectionType::VERTICAL);
}

BlurEffect::BlurEffect(int blur_size, float duration)
    : m_is_infinite(false)
    , m_duration(duration)
    , m_blur_size(blur_size)
{
    m_shader_x = create_blur_shader(m_blur_size, BlurDirectionType::HORIZONTAL);
    m_shader_y = create_blur_shader(m_blur_size, BlurDirectionType::VERTICAL);
}

void BlurEffect::update(float time_delta)
{
    m_time_elapsed += time_delta;
}

void BlurEffect::apply(sf::Sprite& sprite)
{
    const auto texture_size = static_cast<sf::Vector2i>(sprite.getTexture()->getSize());
    auto render_texture_x = engine::get_render_texture_storage().get(texture_size);
    render_texture_x->clear(sf::Color::Transparent);
    m_shader_x->setUniform("texture", *sprite.getTexture());
    m_shader_x->setUniform("invertTextureSize", 1.0f / sprite.getTexture()->getSize().x);
    render_texture_x->draw(sprite, m_shader_x.get());
    render_texture_x->display();

    m_shader_y->setUniform("texture", render_texture_x->getTexture());
    m_shader_y->setUniform("invertTextureSize", 1.0f / render_texture_x->getTexture().getSize().y);
    auto render_texture_y = engine::get_render_texture_storage().get(texture_size);
    render_texture_y->clear(sf::Color::Transparent);
    render_texture_y->draw(sf::Sprite(render_texture_x->getTexture()), m_shader_y.get());
    render_texture_y->display();
    sprite = sf::Sprite(render_texture_y->getTexture());
}

bool BlurEffect::isFinished() const
{
    if (!m_is_infinite)
    {
        return m_time_elapsed >= m_duration;
    }

    return false;
}

Effect::Pointer BlurEffect::clone() const
{
    if (m_is_infinite)
    {
        return std::make_shared<BlurEffect>(m_blur_size);
    }
    else
    {
        return std::make_shared<BlurEffect>(m_blur_size, m_duration);
    }
}
