#include "creature.hpp"

void Creature::registerAttribute(std::string const& stat_name, float initial_value)
{
    auto it = m_attributes.find(stat_name);
    if (it == m_attributes.end())
    {
        m_attributes.insert(std::make_pair(stat_name, initial_value));
    }
}

float Creature::getAttribute(std::string const& stat_name) const
{
    auto it = m_attributes.find(stat_name);
    if (it != m_attributes.end())
    {
        return it->second;
    }

    return 0;
}

void Creature::setAttribute(std::string const& stat_name, int value)
{
    auto it = m_attributes.find(stat_name);
    if (it != m_attributes.end())
    {
        it->second = value;
    }
}

bool Creature::hasAttribute(std::string const& stat_name) const
{
    return m_attributes.find(stat_name) != m_attributes.end();
}
