#include "animated_effect.hpp"

void AnimatedEffect::update(float time_delta)
{
    auto animation = animation_graph.getActive();
    if (animation)
    {
        animation->update(time_delta);
        if (animation->isFinished())
        {
            destroy();
        }
    }
}

GameObject::Pointer AnimatedEffect::clone() const
{
    auto instance = std::make_shared<AnimatedEffect>(m_name);
    cloneComponents(instance);
    return instance;
}
