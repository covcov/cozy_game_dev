#include "debug.hpp"

bool Debug::isEnabled = false;
std::vector<Debug::Line> Debug::lines = {};
std::vector<std::pair<utl::Circle<float>, sf::Color>> Debug::circles = {};

void Debug::toggleDeveloperMode()
{
#ifdef LOG
    isEnabled = !isEnabled;
    log("Developer mode is", isEnabled ? "enabled!" : "disabled!");
#endif
}

void Debug::drawLine(sf::Vector2f const& from, sf::Vector2f const& to, sf::Color const& color)
{
    if (!isEnabled)
    {
        return;
    }

    lines.emplace_back(from, to, color);
}

void Debug::drawCircle(sf::Vector2f const& center, float radius, sf::Color const& color)
{
    if (!isEnabled)
    {
        return;
    }

    circles.push_back({ utl::Circle<float>(center, radius), color });
}

void Debug::draw(std::shared_ptr<sf::RenderTexture> render_texture)
{
    render_texture->clear(sf::Color::Transparent);
    for (auto const& line : lines)
    {
        sf::VertexArray lines(sf::PrimitiveType::Lines, 2);
        lines[0].position = line.from;
        lines[0].color = line.color;
        lines[1].position = line.to;
        lines[1].color = line.color;
        render_texture->draw(lines);
    }

    for (auto const& p : circles)
    {
        sf::CircleShape circle_shape;
        circle_shape.setRadius(p.first.radius);
        circle_shape.setOutlineThickness(1);
        circle_shape.setFillColor(sf::Color::Transparent);
        circle_shape.setOutlineColor(p.second);
        sf::Vector2f position(p.first.position.x - p.first.radius, p.first.position.y - p.first.radius);
        circle_shape.setPosition(position);
        render_texture->draw(circle_shape);
    }

    lines.clear();
    circles.clear();
}
