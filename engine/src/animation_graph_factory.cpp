#include "animation_graph_factory.hpp"
#include "debug.hpp"

#include <fstream>
#include <iostream>

std::unordered_map<std::string, sf::Texture> AnimationGraphFactory::textures = {};

AnimationGraph AnimationGraphFactory::create(
    std::string const& directory_name,
    std::string const& file_name,
    sf::Vector2f shift)
{
    AnimationGraph graph;
    graph.setShift(shift);
    std::ifstream fstr(directory_name + file_name);

    std::string tag;
    std::string sprite_file;
    fstr >> tag >> sprite_file;

    sf::Texture texture = getTexture(directory_name + sprite_file);

    int states_num;
    fstr >> tag >> states_num;

    std::vector<std::string> states;
    for (int i = 0; i < states_num; ++i)
    {
        std::string name;
        float duration;
        int frames_num;
        int cycled;
        std::vector<sf::IntRect> frames;

        fstr >> name;
        fstr >> tag >> duration;
        fstr >> tag >> cycled;
        fstr >> tag >> frames_num;
        for (int j = 0; j < frames_num; ++j)
        {
            sf::IntRect rect;
            fstr >> rect.left >> rect.top >> rect.width >> rect.height;
            frames.push_back(rect);
        }

        Animation::Pointer animation = std::make_shared<Animation>(texture, duration, static_cast<bool>(cycled), std::move(frames));
        graph.registerAnimation(animation, name);
        states.push_back(name);
    }

    int transitions_num;
    fstr >> tag >> transitions_num;
    for (int i = 0; i < transitions_num; ++i)
    {
        std::string from_state;
        std::string to_state;
        std::string message;

        fstr >> from_state >> message >> to_state;

        if (from_state == "ANY_STATE")
        {
            for (auto const& state : states)
            {
                graph.registerTransition(state, to_state, message);
            }
        }
        else
        {
            graph.registerTransition(from_state, to_state, message);
        }
    }

    return graph;
}

sf::Texture AnimationGraphFactory::getTexture(std::string const& path)
{
    auto it = textures.find(path);
    if (it == textures.end())
    {
        sf::Texture texture;
        texture.loadFromFile(path);
        return texture;
    }
    else
    {
        return it->second;
    }
    
}
