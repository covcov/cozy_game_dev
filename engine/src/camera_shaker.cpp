#include "camera_shaker.hpp"
#include "game_resources_storage.hpp"
#include "random_engine.hpp"

void CameraShaker::update(float time_delta)
{
    m_elapsed_time += time_delta;
    if (m_elapsed_time >= m_duration)
    {
        destroy();
        engine::get_camera()->applyShift({ 0.f, 0.f });
    }
    else if (time_delta > std::numeric_limits<float>::epsilon() * 10)
    {
        float const dumping = (m_duration - m_elapsed_time) / m_duration;
        float const scale_horizontal = dumping * RandomEngine::random();
        float const scale_vertical = dumping * RandomEngine::random();
        sf::Vector2f const shift(m_amplitude.x * scale_horizontal, m_amplitude.y * scale_vertical);
        engine::get_camera()->applyShift(shift);
    }
}