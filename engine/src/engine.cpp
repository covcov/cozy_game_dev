#include "engine.hpp"

#include "game_resources_storage.hpp"
#include "scene_manager.hpp"

namespace engine
{

void init(sf::RenderWindow& render_window, Camera::Pointer camera, IInputHandler::Pointer input_handler)
{
    GameResourcesStorage::init(render_window, camera, input_handler);
}

Scene::Pointer get_active_scene()
{
    return SceneManager::instance().getActive();
}

std::shared_ptr<GridMap> get_grid_map()
{
    auto scene = get_active_scene();
    if (scene)
    {
        return scene->getGridMap();
    }

    return nullptr;
}

std::shared_ptr<PathSearchEngine> get_path_search_engine()
{
    auto scene = get_active_scene();
    if (scene)
    {
        return scene->getPathSearchEngine();
    }

    return nullptr;
}

sf::RenderWindow& get_render_window()
{
    return GameResourcesStorage::instance()->getRenderWindow();
};

sf::RenderTexture& get_render_texture()
{
    return GameResourcesStorage::instance()->getRenderTexture();
};

RenderTexturesStorage& get_render_texture_storage()
{
    return GameResourcesStorage::instance()->getRenderTexturesStorage();
}

SoundEngine& get_sound_engine()
{
    return GameResourcesStorage::instance()->getSoundEngine();
}

FontsStorage& get_fonts_storage()
{
    return GameResourcesStorage::instance()->getFontsStorage();
}

Camera::Pointer get_camera()
{
    return GameResourcesStorage::instance()->getCamera();
}

IInputHandler::Pointer get_input_handler()
{
    return GameResourcesStorage::instance()->getInputHandler();
}

}