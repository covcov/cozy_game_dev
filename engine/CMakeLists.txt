cmake_minimum_required (VERSION 2.6)

project (engine)
set (BIN_NAME engine)

file (GLOB_RECURSE SRCS
    src/*.cpp
    include/*.hpp
    include/*.h
)

include_directories (
    ${CMAKE_CURRENT_SOURCE_DIR}/include/
    ${SFML_INCLUDE_PATH}/
)

add_library (${BIN_NAME} STATIC ${SRCS})
set_target_properties(${BIN_NAME} PROPERTIES DEBUG_POSTFIX _d)
target_link_libraries(${BIN_NAME} ${SFML_LIBS})