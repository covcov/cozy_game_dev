from PIL import Image, ImageDraw, ImageFilter

CIRCLE_RADIUS = 6
WIDTH = 2048
HEIGHT = 2048
SIZE = (WIDTH, HEIGHT)
X, Y = WIDTH // 2, HEIGHT // 2
BLUR_KERNEL = 2

imageAlpha = Image.new('L', SIZE, 255)
draw = ImageDraw.Draw(imageAlpha)
draw.ellipse((X - CIRCLE_RADIUS, X - CIRCLE_RADIUS, Y + CIRCLE_RADIUS, Y + CIRCLE_RADIUS), fill = 0)
imageAlpha = imageAlpha.filter(ImageFilter.GaussianBlur(BLUR_KERNEL))
image = Image.new('L', SIZE, 0)
image.putalpha(imageAlpha)
image.save(R"D:\game_dev\cozy_game_dev\resources\light_zone_3.png")